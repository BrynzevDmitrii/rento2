// value формируется из sortBy и SortDirection. Используется дальше для преобразования и отправки нужных данных на сервер
const optionsSort = [
  { value: 'priceAsc', label: 'Сначала дешевле' }, // sortBy = 'price, SortDirection = 'asc'
  { value: 'priceDesc', label: 'Сначала дороже' }, // sortBy = 'price, SortDirection = 'desc'
  { value: 'areaDesc', label: 'Площадь больше' }, // sortBy = 'area, SortDirection = 'desc'
  { value: 'areaAsc', label: 'Площадь меньше' }, // sortBy = 'area, SortDirection = 'asc'
  { value: 'createdAtDesc', label: 'По умолчанию' }, // sortBy = 'createdAt, SortDirection = 'desc'
]

const defaultTimeFrom = 0

const defaultOptionsSort = optionsSort[4] ?? { value: '', label: '' }

export { optionsSort, defaultTimeFrom, defaultOptionsSort }
