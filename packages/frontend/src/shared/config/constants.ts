const pathPages = {
  long: 'long',
  short: 'short',
  owners: 'owners',
}

// TODO: добавить актуальные значения контактов
const messengers = {
  home: {
    whatsApp: 'https://wa.me/message/URV33CMJOYT5H1',
    telegram: '',
    vk: '',
    phone: '+74993213185',
  },
  long: {
    whatsApp: 'https://wa.me/message/URV33CMJOYT5H1',
    telegram: '',
    vk: '',
    phone: '+7 499 321 31 85',
  },
  short: {
    whatsApp: 'https://wa.me/message/URV33CMJOYT5H1',
    telegram: '',
    vk: '',
    phone: '+74993213185',
  },
  owners: {
    whatsApp: 'https://wa.me/message/ZFREJ6VNESOCF1',
    telegram: '',
    vk: '',
    phone: '+74993213185',
  },
}

export { pathPages, messengers }
