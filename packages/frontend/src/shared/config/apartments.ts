/**
 * Дефолтное значение offset для запроса списка квартир
 */
const defaultOffset = 0

/**
 * Дефолтное значение limit для запроса списка квартир
 */
const defaultLimit = 30

export { defaultOffset, defaultLimit }
