import {
  Accommodation,
  Apartment,
  Banner,
  MetroStation,
  Photo,
  Service,
  SleepingPlace,
  Review,
} from 'rento-common/models'

import { IRequestFilterLong, IRequestFilterShort } from './typesRequest'

type TType = 'long' | 'short' | 'all'

const enum TypeApartments {
  long = 'long',
  short = 'short',
}

interface IApartmentItem extends Apartment {
  pricePerMonth: string
  securityDepositLong: string
  id: string
  metroAvailabilityByFoot: number
  metroAvailabilityByVehicle: number
  metroStations: IMetroStation[]
  name: string
  roomsNum: string
  storey: number
  totalStoreys: number
  area: string
  type: TType
  isActive: boolean
  isPopular: boolean
  isRentoChoose: boolean
  latinName: string
  description: string
  bnovoId: string
  inparsId: string
  price: string
  discount: string
  commission: string
  utilityBills: string
  securityDepositShort: string
  kitchenArea: string
  distanceFromCenter: number
  admArea: string
  district: string
  sellingPoint: string
  geoCoordinateX: string
  geoCoordinateY: string
  subwayLine: string
  repairs: string
  purity: string
  location: string
  priceQuality: string
  totalRating: string
  checkInStart: string
  checkInEnd: string
  checkOutEnd: string
  smokingAllowed: boolean
  partyingAllowed: boolean
  childrenAllowed: boolean
  petsAllowed: boolean
  maxAdults: number
  maxChildren: number
  createdAt: string
  updatedAt: string
  sleepingPlaces: ISleepingPlaces[]
  accommodations: IAccommodations[]
  services: IServices[]
  banners: IBanners[]
  photos: IPhotos[]
  reviews: IReviews[]
}

interface IReviews extends Review {
  id: string
  apartmentId: string
  author: string
  avatar: string
  repairs: string
  purity: string
  location: string
  priceQuality: string
  comment: string
  isApproved: boolean
  createdAt: string
  updatedAt: string
}

interface ISleepingPlaces extends SleepingPlace {
  id: string
  name: string
  type: string
  size: number
  number: number
  createdAt: string
  updatedAt: string
}

interface IAccommodations extends Accommodation {
  id: string
  name: string
  nameEn: string
  createdAt: string
  updatedAt: string
}

interface IServices extends Service {
  id: string
  type: TType
  name: string
  createdAt: string
  updatedAt: string
}

interface IMetroStation extends MetroStation {
  id: number
  name: string
  isCentral: boolean
  metroLineId: string
  lat: string
  lon: string
  createdAt: string
  updatedAt: string
  metroLine: {
    name: string
    color: string
    id: string
  }
}

interface IBanners extends Banner {
  id: string
  name: string
  createdAt: string
  updatedAt: string
}

interface IPhotos extends Photo {
  id: string
  apartment_id: string
  link: string
  path: string
  created_at: string
  updated_at: string
}

interface IMeta {
  result: string
  pagination: {
    perPage: number
    currentPage: number
    hasMorePages: boolean
    total: number
  }
}

interface ICollections {
  popular: IApartmentItem[]
  rentoChoose: IApartmentItem[]
  inCityCenter: IApartmentItem[]
  newlyAdded: IApartmentItem[]
  nearTheSubway: IApartmentItem[]
}

interface IApartmentsOneResponse {
  meta: IMeta
  data: IApartmentItem
  collections: Pick<ICollections, 'popular' | 'rentoChoose'>
}

interface IApartmentsListResponse {
  meta: IMeta
  data: IApartmentItem[]
  collections: ICollections
}

interface IMetroStationsListResponse {
  meta: IMeta
  data: IMetroStation[]
}

interface IApartmentsListParams {
  apartmentRentType: string
  limit?: number
  offset?: number
  fields?: IRequestFilterLong | IRequestFilterShort
}

export type {
  IApartmentItem,
  IApartmentsOneResponse,
  IApartmentsListResponse,
  IMetroStationsListResponse,
  IMetroStation,
  ISleepingPlaces,
  IApartmentsListParams,
  ICollections,
}

export { TypeApartments }
