const APARTMENTS_LIST = '/api/v1/apartments/list'
const APARTMENTS_ONE = '/api/v1/apartments/show'
const METRO_STATIONS_LIST = '/api/v1/metro-stations/list'

export { APARTMENTS_LIST, APARTMENTS_ONE, METRO_STATIONS_LIST }
