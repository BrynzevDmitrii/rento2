import { AxiosResponse } from 'axios'

import { defaultLimit, defaultOffset } from '@shared/config'

import axios from './axios'
import {
  APARTMENTS_LIST,
  APARTMENTS_ONE,
  METRO_STATIONS_LIST,
} from './endpoints'
import {
  IApartmentsListParams,
  IApartmentsListResponse,
  IApartmentsOneResponse,
  IMetroStationsListResponse,
} from './typesApi'

const getApartmentsList = async ({
  apartmentRentType,
  limit = defaultLimit,
  offset = defaultOffset,
  fields,
}: IApartmentsListParams): Promise<AxiosResponse<IApartmentsListResponse>> => {
  return await axios.post(APARTMENTS_LIST, {
    apartmentRentType,
    limit,
    offset,
    fields,
  })
}

const getApartmentsOne = async (
  latinName: string,
  fields?: string
): Promise<AxiosResponse<IApartmentsOneResponse>> =>
  await axios.get(`${APARTMENTS_ONE}/${latinName}`, {
    params: {
      fields,
    },
  })

const getMetroStationsList = async (): Promise<
  AxiosResponse<IMetroStationsListResponse>
> => await axios.get(METRO_STATIONS_LIST)

export { getApartmentsList, getApartmentsOne, getMetroStationsList }
