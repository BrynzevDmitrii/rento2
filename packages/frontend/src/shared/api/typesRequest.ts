import { IApartmentItem } from './typesApi'

type RoomsNumber = 'studio' | '1' | '2' | '3'
type FloorType = 'notFirst' | 'notLast' | 'last'
type SortBy = 'price' | 'area' | 'createdAt'
type SortDirection = 'acs' | 'desc'
type Appliances =
  | 'refrigerator'
  | 'tv'
  | 'washingMachine'
  | 'airConditioner'
  | 'dishWasher'
type AvailablityBy = 'vehicle' | 'foot'
type ApartmentFurniture =
  | 'withFurniture'
  | 'withoutFurniture'
  | 'goodRepairs'
  | 'balcony'
type BuildingType = 'stalinEra' | 'newlyBuilt' | 'inTheCenter'
type PricePerNight = '<4000' | '4000-4500' | '4500-5000' | '>5000'
type OptionsShort =
  | 'childrenAllowed'
  | 'petsAllowed'
  | 'balcony'
  | 'parking'
  | 'moreThanAMonth'

type FilterItem =
  | RoomsNumber
  | FloorType
  | Appliances
  | AvailablityBy
  | ApartmentFurniture
  | BuildingType
  | PricePerNight
  | OptionsShort
  | SortBy
  | SortDirection

interface IParametersListApartments {
  type: string
  offset?: number
  limit?: number
  fields?: IApartmentItem
}

interface IRequestFilterLong {
  roomsNumber?: RoomsNumber[] | undefined
  areChildrenAllowed?: boolean | undefined
  arePetsAllowed?: boolean | undefined
  priceFrom?: number | undefined
  priceTo?: number | undefined
  areaFrom?: number | undefined
  areaTo?: number | undefined
  floorFrom?: number | undefined
  floorTo?: number | undefined
  floorType?: FloorType[] | undefined
  sortBy: SortBy
  sortDirection: SortDirection
  appliances?: Appliances[] | undefined
  availablityBy?: AvailablityBy | undefined
  availabilityTimeFrom?: number | undefined
  availabilityTimeTo?: number | undefined
  apartmentFurniture?: ApartmentFurniture[] | undefined
  buildingType?: BuildingType[] | undefined
  metroStationIds?: number[] | undefined
}

interface IRequestFilterShort {
  // apartmentRentType: 'short'
  roomsNumber: RoomsNumber[]
  pricePerNight: PricePerNight[]
  dateIn?: Date // Пока необязательное поле. Сделать обязательным после календаря
  dateOut?: Date // Пока необязательное поле. Сделать обязательным после календаря
  areaFrom: number
  areaTo: number
  ofAdults: number
  ofInfants: number
  ofChilds: number
  sortBy?: SortBy
  sortDirection?: SortDirection
  options: OptionsShort[]
  appliances: Appliances[]
  availablityBy: AvailablityBy
  availabilityTimeFrom: number
  availabilityTimeTo: number
  metroStationIds: number[]
}

export type {
  IRequestFilterLong,
  RoomsNumber,
  FloorType,
  SortBy,
  SortDirection,
  Appliances,
  AvailablityBy,
  ApartmentFurniture,
  BuildingType,
  IRequestFilterShort,
  PricePerNight,
  OptionsShort,
  IParametersListApartments,
  FilterItem,
}
