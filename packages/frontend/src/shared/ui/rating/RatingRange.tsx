import { DetailedHTMLProps, HTMLAttributes } from 'react'
import classNames from 'classnames'

import styles from './RatingRange.module.scss'

interface IRatingProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  rating: number
  label: string
  classWrapper?: string
}

export const RatingRange = ({
  rating,
  label,
  classWrapper,
  className,
  ...props
}: IRatingProps): JSX.Element => {
  const getRatingPercentage = (rating: number, maxRating: number): number => {
    return (rating * 100) / maxRating
  }

  return (
    <div className={className} {...props}>
      <p className={styles.label}>{label}</p>
      <div className={classNames(styles.wrapper, classWrapper)}>
        <div className={styles.range}>
          <div
            className={styles.rating}
            style={{ width: `${getRatingPercentage(rating, 5)}%` }}
          />
        </div>
        <span className={styles.text}>{rating}</span>
      </div>
    </div>
  )
}
