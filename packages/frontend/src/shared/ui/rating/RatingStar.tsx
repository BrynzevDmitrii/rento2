import {
  useEffect,
  useState,
  KeyboardEvent,
  DetailedHTMLProps,
  HTMLAttributes,
} from 'react'
import classNames from 'classnames'

import { IconStar } from '@shared/ui'

import styles from './RatingStar.module.scss'

interface IRatingProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  isEditable?: boolean
  rating: number
  setRating?: (rating: number) => void
  classList?: string
}

export const RatingStar = ({
  isEditable = false,
  rating,
  setRating,
  classList,
  className,
  ...props
}: IRatingProps): JSX.Element => {
  const [ratingArray, setRatingArray] = useState<JSX.Element[]>(
    // Нужен для формирования начального массива JSX.Element
    // eslint-disable-next-line react/jsx-no-useless-fragment
    new Array(5).fill(<></>)
  )

  useEffect(() => {
    constructRating(rating)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rating])

  const constructRating = (currentRating: number): void => {
    const updatedArray = ratingArray.map(
      (_el: JSX.Element, idx: number): JSX.Element => {
        return (
          <span
            key={idx}
            className={classNames({
              [styles.filled]: idx < currentRating,
              [styles.editable]: isEditable,
            })}
            onClick={() => onClick(idx + 1)}
            onMouseEnter={() => changeDisplay(idx + 1)}
            onMouseLeave={() => changeDisplay(rating)}
          >
            <IconStar
              className={classNames(styles.icon, {
                [styles.icon_margin]: idx !== ratingArray.length - 1,
              })}
              tabIndex={isEditable ? 0 : -1}
              onKeyDown={(e: KeyboardEvent<SVGElement>) =>
                isEditable && handleSpace(idx + 1, e)
              }
            />
          </span>
        )
      }
    )

    setRatingArray(updatedArray)
  }

  const changeDisplay = (idx: number): void => {
    if (!isEditable) return

    constructRating(idx)
  }

  const onClick = (idx: number): void => {
    if (!isEditable || setRating == null) return

    setRating(idx)
  }

  const handleSpace = (idx: number, e: KeyboardEvent<SVGElement>): void => {
    if (e.code !== 'Space' || setRating == null) return

    setRating(idx)
  }

  return (
    <div className={classNames(styles.wrapper, className)} {...props}>
      <ul className={classNames(styles.list, classList)}>
        {ratingArray.map((el, idx) => (
          <li key={idx} className={styles.item}>
            {el}
          </li>
        ))}
      </ul>
      <span className={styles.text}>{rating}</span>
    </div>
  )
}
