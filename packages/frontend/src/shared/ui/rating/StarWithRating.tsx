import { DetailedHTMLProps, HTMLAttributes } from 'react'
import classNames from 'classnames'

import { IconStar } from '@shared/ui'

import styles from './StarWithRating.module.scss'

interface IRatingProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  rating: number
  isBackground?: boolean
}

export const StarWithRating = ({
  rating,
  isBackground,
  className,
  ...props
}: IRatingProps): JSX.Element => {
  return (
    <div
      className={classNames(styles.wrapper, className, {
        [styles.wrapper_background]: isBackground,
      })}
      {...props}
    >
      <IconStar className={styles.icon} />
      <span className={styles.text}>{rating}</span>
    </div>
  )
}
