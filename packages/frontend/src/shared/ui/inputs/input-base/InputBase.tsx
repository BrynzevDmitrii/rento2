import { ComponentPropsWithoutRef, ForwardedRef, forwardRef } from 'react'
import classNames from 'classnames'

import styles from './InputBase.module.scss'

const enum full {
  filled = 'filled',
  stroke = 'stroke',
}

interface IInputProps extends ComponentPropsWithoutRef<'input'> {
  full?: keyof typeof full
  isValid?: boolean
  label: string
  errorText?: string
}

/**
 * Компонент для формирования простых полей различных типов (text, email, tel и др.). Валидируем и накладываем маску на уровне родителя.
 * @param param0
 * @returns
 */
export const InputBase = forwardRef(
  (
    {
      full = 'filled',
      isValid = true,
      label,
      errorText = 'Нужно заполнить это поле',
      className,
      ...props
    }: IInputProps,
    ref: ForwardedRef<HTMLInputElement>
  ) => {
    return (
      <>
        <div className={styles.label}>{label}</div>
        <div className={styles.input__wrapper}>
          <input
            ref={ref}
            className={classNames(styles.input, className, {
              [styles['input-filled']]: full === 'filled',
              [styles['input-stroke']]: full === 'stroke',
              [styles['error-input']]: !isValid,
            })}
            {...props}
          />
        </div>
        {!isValid && <p className={styles['error-text']}>{errorText}</p>}
      </>
    )
  }
)

InputBase.displayName = 'InputBase'
