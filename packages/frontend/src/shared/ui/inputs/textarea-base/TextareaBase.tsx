import { ComponentPropsWithoutRef, ForwardedRef, forwardRef } from 'react'
import classNames from 'classnames'

import styles from './TextareaBase.module.scss'

const enum full {
  filled = 'filled',
  stroke = 'stroke',
}

interface ITextareaProps extends ComponentPropsWithoutRef<'textarea'> {
  full?: keyof typeof full
  isValid?: boolean
  label?: string
  errorText?: string
}

/**
 * Компонент для формирования поля textarea. Валидируем на уровне родителя.
 * @param param0
 * @returns
 */
export const TextareaBase = forwardRef(
  (
    {
      full = 'filled',
      isValid = true,
      label,
      errorText = 'Нужно заполнить это поле',
      className,
      ...props
    }: ITextareaProps,
    ref: ForwardedRef<HTMLTextAreaElement>
  ) => {
    return (
      <>
        {label != null && <div className={styles.label}>{label}</div>}
        <div className={styles.textarea__wrapper}>
          <textarea
            ref={ref}
            className={classNames(styles.textarea, className, {
              [styles['textarea-filled']]: full === 'filled',
              [styles['textarea-stroke']]: full === 'stroke',
              [styles['error-textarea']]: !isValid,
            })}
            {...props}
          />
        </div>
        {!isValid && <p className={styles['error-text']}>{errorText}</p>}
      </>
    )
  }
)

TextareaBase.displayName = 'TextareaBase'
