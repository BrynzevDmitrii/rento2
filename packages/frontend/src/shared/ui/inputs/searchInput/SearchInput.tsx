import { ChangeEvent, FC } from 'react'
import classNames from 'classnames'

import { IconClose, IconSearchNormal } from '@shared/ui/icons'

import styles from './SearchInput.module.scss'

interface ISearchInput {
  value: string
  onChange: (value: string) => void
  placeholder?: string
  classProps?: string
  name?: string
}

export const SearchInput: FC<ISearchInput> = ({
  value,
  onChange,
  classProps,
  placeholder,
  name = '',
}) => {
  const handleChange = ({ target }: ChangeEvent<HTMLInputElement>): void =>
    onChange(target.value)

  return (
    <div className={classNames(styles.input, classProps)}>
      <label className={styles.input__label}>
        {value === '' && (
          <IconSearchNormal className={styles['input__search-icon']} />
        )}
        <input
          autoComplete="off"
          className={styles.input__filed}
          name={name}
          placeholder={placeholder}
          type="text"
          value={value}
          onChange={handleChange}
        />
        {value !== '' && (
          <button
            className={styles['input__close']}
            onClick={() => onChange('')}
          >
            <IconClose className={styles['input__close-icon']} />
          </button>
        )}
      </label>
    </div>
  )
}
