import classNames from 'classnames'
import {
  ComponentPropsWithoutRef,
  ElementType,
  ReactNode,
  RefObject,
  useMemo,
} from 'react'

import styles from './ButtonIcon.module.scss'

// Корректное определение типов происходит на основе приходящего appearance
const enum appearance {
  arrowNavigation = 'arrowNavigation',
  buttonIconMain = 'buttonIconMain',
  buttonAction = 'buttonAction',
  buttonCount = 'buttonCount',
}

const enum full {
  filled = 'filled',
  stroke = 'stroke',
}
interface IButtonBase<T extends keyof typeof appearance> {
  children?: ReactNode
  size: '56' | '48' | '40' | '32' | '24'
  full?: keyof typeof full
  refProps?: RefObject<HTMLButtonElement>
  appearance: T
  round?: boolean
}

interface IButtonArrowNavigation {
  grade: 'iris' | 'neutral'
  round?: never
}

interface IButtonIconMain {
  grade: 'primary' | 'secondary' | 'neutral'
  round?: boolean
}

interface IButtonAction {
  grade: 'iris'
  size: '32'
  round?: never
}

interface IButtonCount {
  full: 'stroke'
  grade: 'iris'
  size: '24'
  round?: never
}

// Условная типизация на основе appearance
type TButtonOption<T> = appearance.arrowNavigation extends T
  ? IButtonArrowNavigation
  : appearance.buttonAction extends T
  ? IButtonAction
  : appearance.buttonCount extends T
  ? IButtonCount
  : IButtonIconMain

export type TButtonIcon<T extends keyof typeof appearance> = IButtonBase<T> &
  TButtonOption<T> &
  ComponentPropsWithoutRef<'button'> &
  ComponentPropsWithoutRef<'a'>

/**
 * Данный компонент служит для формирования кнопок управления слайдерами, кнопок с иконками, кнопок-действий (для закрывания/открывания элементов, добавления в избранное и др). подобных. Цвет передаваемых иконок может меняться за счет fill и stroke. Если цвет иконки меняется за счет stroke, то нужно передать full = stroke, иначе full = filled.
 * @param param0
 * @returns
 */
export function ButtonIcon<T extends keyof typeof appearance>({
  children,
  size,
  className,
  grade,
  full = 'filled',
  appearance,
  round = false,
  href,
  refProps,
  ...props
}: TButtonIcon<T>): JSX.Element {
  const getClassesArrowNavigation = useMemo(
    () => ({
      // @ts-expect-error Правильные типы обеспечиваются через входные параметры
      [styles[`grade-${grade}-full-${full}`]]: appearance === 'arrowNavigation',
    }),
    [grade, full, appearance]
  )

  const getClassesButtonIconMain = useMemo(
    () => ({
      [styles.main]: appearance === 'buttonIconMain',
      [styles['main-round']]: round,
      // @ts-expect-error Правильные типы обеспечиваются через входные параметры
      [styles[`main-grade-${grade}-full-${full}`]]:
        appearance === 'buttonIconMain',
    }),
    [appearance, round, grade, full]
  )

  const getClassesButtonAction = useMemo(
    () => ({
      // Правильные типы обеспечиваются через входные параметры
      [styles[`action-full-${full}`]]: appearance === 'buttonAction',
    }),
    [full, appearance]
  )

  const getClassesButtonCount = useMemo(
    () => ({
      // Правильные типы обеспечиваются через входные параметры
      [styles[`count-full-${full as full.stroke}`]]:
        appearance === 'buttonCount',
    }),
    [full, appearance]
  )

  const Component = href != null ? 'a' : ('button' as ElementType)

  return (
    <Component
      ref={refProps}
      className={classNames(
        className,
        styles['button-icon'],
        styles[`size-${size}`],
        {
          ...getClassesButtonIconMain,
          ...getClassesArrowNavigation,
          ...getClassesButtonAction,
          ...getClassesButtonCount,
        }
      )}
      href={href}
      {...props}
    >
      {children}
    </Component>
  )
}
