import classNames from 'classnames'
import {
  ReactNode,
  ComponentPropsWithoutRef,
  ElementType,
  ForwardedRef,
  forwardRef,
} from 'react'

import styles from './ButtonGeneral.module.scss'

// Корректное определение типов происходит на основе приходящего full
const enum full {
  filled = 'filled',
  outlined = 'outlined',
  text = 'text',
}

interface IButtonBase<T extends keyof typeof full> {
  round?: boolean
  font: 'l' | 'm' | 's' | 'xs'
  height: '56' | '48' | '44' | '40' | '32'
  children: ReactNode
  full: T
}

interface IButtonFilled {
  grade?: 'primary' | 'secondary' | 'neutral'
}

interface IButtonOutlined {
  grade?: 'primary' | 'secondary' | 'neutral'
}

interface IButtonText {
  font?: 'm' | 's' | 'xs'
  grade: 'neutral' | 'accent' | 'iris'
}

// Условная типизация на основе full
type TButtonOption<T> = full.filled extends T
  ? IButtonFilled
  : full.outlined extends T
  ? IButtonOutlined
  : IButtonText

export type TButton<T extends keyof typeof full> = IButtonBase<T> &
  TButtonOption<T> &
  ComponentPropsWithoutRef<'button'> &
  ComponentPropsWithoutRef<'a'>

/**
 * Данный компонент служит для формирования большинства кнопок (текстовых, с иконками). Ширину кнопки можно указать через className (можно передать любые классы)
 * При использовании кнопки в качестве ссылки, которая служит для перехода по страницам сайта, нужно обернуть кнопку в Link (Next.js). Link управляет кнопкой через ref.
 * @param param0
 * @returns
 */
export const ButtonGeneral = forwardRef(
  <T extends keyof typeof full>(
    {
      round = false,
      grade = 'primary',
      full,
      font,
      height,
      className,
      href,
      children,
      ...props
    }: TButton<T>,
    ref: ForwardedRef<HTMLAnchorElement | HTMLButtonElement>
  ) => {
    const Component = href != null ? 'a' : ('button' as ElementType)

    return (
      <Component
        ref={ref}
        className={classNames(
          className,
          styles['button'],
          // @ts-expect-error Правильные типы обеспечиваются через входные параметры
          styles[`grade-${grade}-full-${full}`],
          styles[`height-${height}`],
          {
            [styles.round]: round,
            [styles[`font-${font}`]]: full !== 'text',
            // @ts-expect-error Правильные типы обеспечиваются через входные параметры
            [styles[`font-text-${font}`]]: full === 'text',
          }
        )}
        href={href}
        {...props}
      >
        {children}
      </Component>
    )
  }
) as (<T extends keyof typeof full>(
  prop: TButton<T> & {
    ref?: ForwardedRef<HTMLAnchorElement | HTMLButtonElement>
  }
) => JSX.Element) & { displayName?: string }

ButtonGeneral.displayName = 'ButtonGeneral'
