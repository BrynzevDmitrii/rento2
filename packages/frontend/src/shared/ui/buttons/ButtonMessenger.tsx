import classNames from 'classnames'
import { AnchorHTMLAttributes, DetailedHTMLProps, FC, ReactNode } from 'react'

import styles from './ButtonMessenger.module.scss'

interface IPropsButtonMessenger
  extends DetailedHTMLProps<
    AnchorHTMLAttributes<HTMLAnchorElement>,
    HTMLAnchorElement
  > {
  path: string
  children: ReactNode
}

export const ButtonMessenger: FC<IPropsButtonMessenger> = ({
  path,
  children,
  className,
  ...props
}) => {
  return (
    <a
      className={classNames(styles.link, className)}
      href={path}
      rel="noopener noreferrer"
      target="_blank"
      {...props}
    >
      {children}
    </a>
  )
}
