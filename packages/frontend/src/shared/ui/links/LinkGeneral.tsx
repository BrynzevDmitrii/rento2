import classNames from 'classnames'
import { FC } from 'react'
import Link, { LinkProps } from 'next/link'
import styles from './LinkGeneral.module.scss'

export interface ILink {
  classProps?: string | undefined
  target?: string
}

export const LinkGeneral: FC<LinkProps & ILink> = ({
  href,
  classProps,
  children,
  target,
}) => (
  <Link href={href}>
    <a className={classNames(styles['link'], classProps)} target={target}>
      {children}
    </a>
  </Link>
)
