import { FC } from 'react'
import classNames from 'classnames'
import { Swiper, SwiperSlide } from 'swiper/react'

import styles from './ChipBox.module.scss'

interface IChip {
  key: string | number
  title: string
}
interface IChipBox {
  chips: IChip[]
  value: IChip[]
  classProps?: string
  classChip?: string
  onChange: (value: IChip[]) => void
  marginRight?: number
  isSwipeable?: boolean
  slidesPerView?: number | 'auto'
  isRadio?: boolean
}

export const ChipBox: FC<IChipBox> = ({
  value,
  onChange,
  chips,
  classProps,
  classChip,
  isSwipeable = false,
  marginRight = 0,
  slidesPerView = 'auto',
  isRadio = false,
}) => {
  const handleSelect = (chip: IChip) => () => {
    if (isContainsValue(chip)) {
      onChange(value.filter((chipValue) => chipValue.key !== chip.key))
    } else onChange([...value, chip])
  }

  // для типа isRadio тоже массив, т.к. с массивом проще работать и для множественного выбора здесь массив
  const handleSelectRadio = (chip: IChip) => () => {
    if (isContainsValue(chip)) {
      onChange([])
    } else onChange([chip])
  }

  const isContainsValue = (chip: IChip): boolean => {
    return value.some((item) => item.key === chip.key)
  }

  if (isSwipeable) {
    return (
      <Swiper
        className={classNames(classProps)}
        slidesPerView={slidesPerView}
        spaceBetween={marginRight}
      >
        {chips.map((chip) => (
          <SwiperSlide key={chip.key} className={styles.box__slide}>
            <button
              className={classNames(styles.box__chip, classChip, {
                [styles['box__chip-active']]: isContainsValue(chip),
              })}
              type="button"
              onClick={isRadio ? handleSelectRadio(chip) : handleSelect(chip)}
            >
              {chip.title}
            </button>
          </SwiperSlide>
        ))}
      </Swiper>
    )
  }
  return (
    <div className={classNames(styles.box, classProps)}>
      {chips.map((chip) => (
        <button
          key={chip.key}
          className={classNames(styles.box__chip, classChip, {
            [styles['box__chip-active']]: isContainsValue(chip),
          })}
          type="button"
          onClick={isRadio ? handleSelectRadio(chip) : handleSelect(chip)}
        >
          {chip.title}
        </button>
      ))}
    </div>
  )
}
