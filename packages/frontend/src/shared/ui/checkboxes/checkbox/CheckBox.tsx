import { ChangeEvent, FC, ReactNode } from 'react'
import classNames from 'classnames'

import styles from './CheckBox.module.scss'

interface ICheckBox {
  value: boolean
  onChange?: (value: boolean) => void
  label?: string | ReactNode
  classBox?: string
  classLabel?: string
  classWrapper?: string
  isValid?: boolean
}

export const CheckBox: FC<ICheckBox> = ({
  value,
  onChange,
  classBox,
  classLabel,
  classWrapper,
  label,
  isValid = true,
}) => {
  const handleChange = (e: ChangeEvent<HTMLInputElement>): void =>
    onChange?.(e.target.checked)

  return (
    <div className={classWrapper}>
      {!isValid && !value && (
        <span className={styles.checkbox__required}>
          Необходимо ваше согласие
        </span>
      )}
      <label className={classNames(styles.checkbox__label, classLabel)}>
        <input
          checked={value}
          className={classNames(styles.checkbox__box, classBox, {
            [styles['checkbox__box-required']]: !isValid && !value,
          })}
          type="checkbox"
          onChange={handleChange}
        />
        {label}
      </label>
    </div>
  )
}
