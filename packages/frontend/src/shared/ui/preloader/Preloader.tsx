import classNames from 'classnames'
import { useEffect } from 'react'
import FocusLock from 'react-focus-lock'

import styles from './Preloader.module.scss'

export function Preloader(): JSX.Element {
  useEffect(() => {
    document.body.classList.add('modal-open')

    return () => {
      document.body.classList.remove('modal-open')
    }
  }, [])

  return (
    <FocusLock>
      <div className={styles.wrapper} tabIndex={1}>
        <div className={styles.preloader}>
          <div className={styles.preloader__inner} />
          <div className={styles.preloader__img} />
        </div>
      </div>
      <div
        className={classNames(styles.backdrop, styles['backdrop--visible'])}
      />
    </FocusLock>
  )
}
