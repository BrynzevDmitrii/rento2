import { SVGProps } from 'react'

// TODO: Убрать classProps, в компонентах использовать className
export interface IIconClassProps extends SVGProps<SVGSVGElement> {
  classProps?: string
}

export interface IIconClassStroke extends IIconClassProps {
  figure?: 'square' | 'circle'
}

export interface IIconClassColor extends IIconClassProps {
  isColor?: boolean
}
