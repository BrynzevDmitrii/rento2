import { IIconClassProps } from '../types/iconTypes'
import classNames from 'classnames'
import styles from '../IconBaseStyles.module.scss'

export const IconDocumentUpload = ({
  className,
  ...props
}: IIconClassProps): JSX.Element => {
  return (
    <svg
      className={classNames(styles['icon'], className)}
      fill="none"
      height="16"
      viewBox="0 0 17 16"
      width="17"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M6.49984 11.3335V7.3335L5.1665 8.66683"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.5 7.3335L7.83333 8.66683"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.1668 6.66683V10.0002C15.1668 13.3335 13.8335 14.6668 10.5002 14.6668H6.50016C3.16683 14.6668 1.8335 13.3335 1.8335 10.0002V6.00016C1.8335 2.66683 3.16683 1.3335 6.50016 1.3335H9.8335"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.1668 6.66683H12.5002C10.5002 6.66683 9.8335 6.00016 9.8335 4.00016V1.3335L15.1668 6.66683Z"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
