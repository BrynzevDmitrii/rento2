import { IIconClassProps } from '../types/iconTypes'
import classNames from 'classnames'
import styles from '../IconBaseStyles.module.scss'

export const IconMenu = ({
  className,
  ...props
}: IIconClassProps): JSX.Element => (
  <svg
    className={classNames(styles['icon'], className)}
    height="24"
    viewBox="0 0 24 24"
    width="24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path d="M3 7H21" strokeLinecap="round" />
    <path d="M3 12H21" strokeLinecap="round" />
    <path d="M3 17H21" strokeLinecap="round" />
  </svg>
)
