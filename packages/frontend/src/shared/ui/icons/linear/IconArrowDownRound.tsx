import { IIconClassProps } from '../types/iconTypes'
import classNames from 'classnames'
import styles from '../IconBaseStyles.module.scss'

export const IconArrowDownRound = ({
  className,
  ...props
}: IIconClassProps): JSX.Element => {
  return (
    <svg
      className={classNames(styles['icon'], className)}
      fill="none"
      height="21"
      viewBox="0 0 20 21"
      width="20"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path d="M12.3873 20.074C12.5898 20.2618 12.9061 20.2499 13.0939 20.0475L16.1542 16.7482C16.3419 16.5458 16.3301 16.2294 16.1276 16.0416C15.9251 15.8538 15.6088 15.8657 15.421 16.0682L12.7008 19.0008L9.76813 16.2806C9.56567 16.0928 9.24931 16.1047 9.06152 16.3072C8.87373 16.5096 8.88562 16.826 9.08807 17.0138L12.3873 20.074ZM1.80533 9.32657C5.63738 9.65733 8.11958 10.278 9.70551 11.7422C11.275 13.1913 12.0715 15.5706 12.2277 19.7262L13.227 19.6886C13.0684 15.4677 12.2586 12.7384 10.3839 11.0075C8.52559 9.2918 5.72568 8.66124 1.89133 8.33028L1.80533 9.32657Z" />
    </svg>
  )
}
