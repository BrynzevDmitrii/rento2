import { IIconClassProps } from '../types/iconTypes'
import classNames from 'classnames'
import styles from '../IconBaseStyles.module.scss'

export const IconTrushSquare = ({
  className,
  ...props
}: IIconClassProps): JSX.Element => {
  return (
    <svg
      className={classNames(styles['icon'], className)}
      fill="none"
      height="16"
      viewBox="0 0 17 16"
      width="17"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M13.1668 4C11.4425 4 9.70243 4 7.97022 4C6.94197 4 5.91372 4 4.89338 4H3.8335"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M6.5 4L6.62227 3.11795C6.70961 2.48205 6.77947 2 7.74891 2H9.25109C10.2205 2 10.2904 2.50256 10.3777 3.11795L10.5 3.98975"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12.5 4L12.1173 11.8292C12.0551 13.051 12.0017 14 10.3732 14H6.62681C4.99833 14 4.94493 13.051 4.88264 11.8292L4.5 4"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M7.1665 6V12M9.83317 6V12" strokeLinecap="round" />
    </svg>
  )
}
