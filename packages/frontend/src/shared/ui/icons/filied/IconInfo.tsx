import { IIconClassProps } from '../types/iconTypes'
import classNames from 'classnames'
import styles from '../IconBaseStyles.module.scss'

export const IconInfo = ({
  className,
  ...props
}: IIconClassProps): JSX.Element => {
  return (
    <svg
      className={classNames(styles['icon-filied'], className)}
      fill="none"
      height="17"
      viewBox="0 0 16 17"
      width="16"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <circle cx="8" cy="8.5" r="7" />
      <path d="M7.00024 7.83321C7.00024 7.28099 7.4479 6.83333 8.00012 6.83333C8.55234 6.83333 9 7.28099 9 7.83321V8.5L9 11.8333V12.5C9 13.0523 8.55228 13.5 8 13.5C7.44771 13.5 7 13.0523 7 12.5V11.8333L7.00024 8.5L7.00024 7.83321Z" />
      <path d="M7.00024 4.49988C7.00024 3.94766 7.4479 3.5 8.00012 3.5C8.55234 3.5 9 3.94766 9 4.49988L9 5.16681C9 5.71903 8.55234 6.16669 8.00012 6.16669C7.4479 6.16669 7.00024 5.71903 7.00024 5.16681L7.00024 4.49988Z" />
    </svg>
  )
}
