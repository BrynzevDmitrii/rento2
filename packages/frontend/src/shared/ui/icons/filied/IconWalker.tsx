import { IIconClassProps } from '../types/iconTypes'
import classNames from 'classnames'
import styles from '../IconBaseStyles.module.scss'

export function IconWalker({
  className,
  ...props
}: IIconClassProps): JSX.Element {
  return (
    <svg
      className={classNames(styles['icon-filied'], className)}
      height="16"
      viewBox="0 0 16 16"
      width="16"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        clipRule="evenodd"
        d="M10.0002 2.66638C10.0002 3.40278 9.40327 3.99975 8.66687 3.99975C7.93047 3.99975 7.3335 3.40278 7.3335 2.66638C7.3335 1.92998 7.93047 1.33301 8.66687 1.33301C9.40327 1.33301 10.0002 1.92998 10.0002 2.66638ZM11.2712 6.51453L12.667 7.33345L12.0003 8.66682L10.5072 7.83617L9.66692 7.00011L9.36725 8.58786L10.0219 10.2007L10.3333 14.0003H8.66657L8.33322 11.6669L7.6668 10.0002L6.76972 11.8362L4.66645 14.0003L3.33307 13.0003L5.3334 10.6669L5.95986 8.38142L6.34808 6.48978L4.00015 8.33353L3.10938 7.15916L7.00012 4L9.77764 4.82807L11.2712 6.51453Z"
        fillRule="evenodd"
      />
    </svg>
  )
}
