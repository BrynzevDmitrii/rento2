import Image from 'next/image'
import classNames from 'classnames'

import { IconAvatarDefault } from '@shared/ui'

import styles from './Avatar.module.scss'

interface IAvatar {
  src?: string
  name?: string
  classProps?: string
}

export const Avatar = ({
  src = '',
  name = '',
  classProps,
}: IAvatar): JSX.Element => {
  const isDefault = name === '' && src === ''
  const isLetterName = src === '' && name !== ''
  const isImage = src !== ''
  const color = name !== '' ? ((name.codePointAt(0) as number) % 4) + 1 : '1'
  return (
    <div
      className={classNames(
        styles.avatar,
        styles[`avatar-${color}`],
        { [styles['avatar-default']]: isDefault },
        classProps
      )}
    >
      {isDefault && (
        <IconAvatarDefault
          className={classNames(styles['avatar__icon-defalut'])}
        />
      )}

      {isLetterName && (
        <span className={styles.avatar__empty}>{name[0]?.toUpperCase()}</span>
      )}

      {isImage && <Image layout="fill" objectFit="cover" src={src} />}
    </div>
  )
}
