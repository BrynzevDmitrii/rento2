import Link from 'next/link'
import { DetailedHTMLProps, FC, HTMLAttributes } from 'react'
import classNames from 'classnames'

import { IconArrowLeft } from '../icons'

import styles from './Breadcrumbs.module.scss'

interface IProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  backPath: {
    path: string
    label: string
  }
  currentPathLabel: string
}

export const Breadcrumbs: FC<IProps> = ({
  backPath,
  currentPathLabel,
  className,
  ...props
}) => {
  return (
    <div className={classNames(styles.breadcrumbs, className)} {...props}>
      <Link href={backPath.path}>
        <a className={styles.breadcrumbs_link}>
          <IconArrowLeft className={styles.breadcrumbs__icon} />
          <span className={styles.label}>{backPath.label}</span>
        </a>
      </Link>
      <span className={styles.slash}>/</span>
      <span className={styles.breadcrumbs__current}>{currentPathLabel}</span>
    </div>
  )
}
