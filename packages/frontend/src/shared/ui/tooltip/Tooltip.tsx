import { ReactNode, useEffect, useRef, useState } from 'react'
import classNames from 'classnames'

import styles from './Tooltip.module.scss'

const enum size {
  xs = 'xs',
  l = 'l',
}

interface ITooltipBase<T extends keyof typeof size> {
  children: ReactNode
  label: string
  size: T
}

interface ITooltipSmall {
  position:
    | 'left-top'
    | 'top-center'
    | 'right-top'
    | 'right'
    | 'bottom-right'
    | 'bottom-center'
    | 'bottom-left'
    | 'left'
}

interface ITooltipLarge {
  position: 'top-center'
}

type TTooltipOption<T> = size.xs extends T ? ITooltipSmall : ITooltipLarge

export type TTooltip<T extends keyof typeof size> = ITooltipBase<T> &
  TTooltipOption<T>

const margin = 10
const headerHeight = 60

export const Tooltip = <T extends keyof typeof size>({
  children,
  label,
  position,
  size,
}: TTooltip<T>): JSX.Element => {
  const targetRef = useRef<HTMLDivElement>(null)
  const tooltipRef = useRef<HTMLDivElement>(null)
  const arrowRef = useRef<HTMLDivElement>(null)

  const [realPosition, setRealPosition] = useState(position)

  const [isVisible, setIsVisible] = useState(false)

  useEffect(() => {
    const onClick = (e: MouseEvent): void => {
      if (tooltipRef.current === null || targetRef.current === null) return
      const node = e.target as Node

      targetRef.current.contains(node) && setIsVisible(!isVisible)

      !tooltipRef.current.contains(node) &&
        !targetRef.current.contains(node) &&
        setIsVisible(false)
    }
    document.addEventListener('click', onClick)

    return () => document.removeEventListener('click', onClick)
  }, [isVisible])

  useEffect(() => {
    const onScroll = (): void => setIsVisible(false)

    document.addEventListener('scroll', onScroll)

    return () => document.removeEventListener('scroll', onScroll)
  }, [position, realPosition])

  useEffect(() => {
    if (
      tooltipRef.current !== null &&
      targetRef.current !== null &&
      arrowRef.current !== null
    ) {
      const tooltipRect = tooltipRef.current.getBoundingClientRect()
      const targetRect = targetRef.current.getBoundingClientRect()
      const arrowRect = arrowRef.current.getBoundingClientRect()

      const isEnoughSpaceAround = {
        top:
          tooltipRect.height + margin < window.innerHeight - targetRect.bottom,
        bottom: tooltipRect.height + margin < targetRect.top - headerHeight,
        left: tooltipRect.width + margin < targetRect.left,
        right:
          tooltipRect.width + margin <
          document.body.clientWidth - targetRect.right,
      }

      switch (realPosition) {
        case 'top-center': {
          if (position !== realPosition && isEnoughSpaceAround.bottom)
            setRealPosition(position)
          if (!isEnoughSpaceAround.top) setRealPosition('bottom-center')

          tooltipRef.current.style.top = `${targetRect.height + margin}px`
          tooltipRef.current.style.left = `${
            targetRect.width / 2 - tooltipRect.width / 2
          }px`
          arrowRef.current.style.top = `-${arrowRect.height}px`
          arrowRef.current.style.left = `${
            tooltipRect.width / 2 - arrowRect.width / 2
          }px`
          break
        }
        case 'right-top': {
          if (position !== realPosition && isEnoughSpaceAround.bottom)
            setRealPosition(position)
          if (!isEnoughSpaceAround.top) setRealPosition('bottom-right')

          tooltipRef.current.style.top = `${targetRect.height + margin}px`
          tooltipRef.current.style.left = `${
            targetRect.width - tooltipRect.width
          }px`
          arrowRef.current.style.left = `${
            tooltipRect.width - arrowRect.width
          }px`
          arrowRef.current.style.top = '0'
          break
        }
        case 'right': {
          if (position !== realPosition && isEnoughSpaceAround.left)
            setRealPosition(position)
          if (!isEnoughSpaceAround.right) setRealPosition('left')

          tooltipRef.current.style.top = `${
            targetRect.height / 2 - tooltipRect.height / 2
          }px`
          tooltipRef.current.style.left = `${targetRect.width + margin}px`
          arrowRef.current.style.top = `${
            tooltipRect.height / 2 - arrowRect.height / 2
          }px`
          arrowRef.current.style.left = `-${arrowRect.width}px`
          break
        }
        case 'bottom-right': {
          if (position !== realPosition && isEnoughSpaceAround.top)
            setRealPosition(position)
          if (!isEnoughSpaceAround.bottom) setRealPosition('right-top')

          tooltipRef.current.style.top = `-${tooltipRect.height + margin}px`
          tooltipRef.current.style.left = `${
            targetRect.width - tooltipRect.width
          }px`
          arrowRef.current.style.left = `${
            tooltipRect.width - arrowRect.width
          }px`
          arrowRef.current.style.top = `${
            tooltipRect.height - arrowRect.height
          }px`
          break
        }
        case 'bottom-center': {
          if (position !== realPosition && isEnoughSpaceAround.top)
            setRealPosition(position)
          if (!isEnoughSpaceAround.bottom) setRealPosition('top-center')

          tooltipRef.current.style.top = `-${tooltipRect.height + margin}px`
          tooltipRef.current.style.left = `${
            targetRect.width / 2 - tooltipRect.width / 2
          }px`
          arrowRef.current.style.top = `${tooltipRect.height}px`
          arrowRef.current.style.left = `${
            tooltipRect.width / 2 - arrowRect.width / 2
          }px`
          break
        }
        case 'bottom-left': {
          if (position !== realPosition && isEnoughSpaceAround.top)
            setRealPosition(position)
          if (!isEnoughSpaceAround.bottom) setRealPosition('left-top')

          tooltipRef.current.style.top = `-${tooltipRect.height + margin}px`
          arrowRef.current.style.top = `${
            tooltipRect.height - arrowRect.height
          }px`
          break
        }
        case 'left': {
          if (position !== realPosition && isEnoughSpaceAround.right)
            setRealPosition(position)
          if (!isEnoughSpaceAround.left) setRealPosition('right')

          tooltipRef.current.style.left = `-${tooltipRect.width + margin}px`
          tooltipRef.current.style.top = `${
            targetRect.height / 2 - tooltipRect.height / 2
          }px`
          arrowRef.current.style.top = `${
            tooltipRect.height / 2 - arrowRect.height / 2
          }px`
          arrowRef.current.style.left = `${tooltipRect.width}px`
          break
        }
        case 'left-top': {
          if (position !== realPosition && isEnoughSpaceAround.bottom)
            setRealPosition(position)
          if (!isEnoughSpaceAround.top) setRealPosition('bottom-left')

          tooltipRef.current.style.top = `${targetRect.height + margin}px`
          arrowRef.current.style.top = '0'
          break
        }
      }
    }
  }, [isVisible, position, realPosition])

  return (
    <div className={styles.wrapper}>
      <div ref={targetRef} className={styles.target}>
        {children}
      </div>

      <div
        ref={tooltipRef}
        className={classNames(styles.tooltip, {
          [styles['tooltip-hidden']]: !isVisible,
          [styles['tooltip-large']]: size === 'l',
        })}
      >
        <div
          ref={arrowRef}
          className={classNames(
            styles.tooltip__arrow,
            styles[`tooltip__arrow-${realPosition}`],
            { [styles['tooltip__arrow-large']]: size === 'l' }
          )}
        />
        <div className={styles.tooltip__label}>{label}</div>
      </div>
    </div>
  )
}
