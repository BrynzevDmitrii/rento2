import { FC } from 'react'
import ReactSelect, {
  components,
  DropdownIndicatorProps,
  Props as SelectProps,
} from 'react-select'
import classNames from 'classnames'

import { IconArrowDown, IconArrowUp } from '@shared/ui'
import styles from './Select.module.scss'

interface IProps extends SelectProps {
  isStroke?: boolean
  className?: string
}

const DropdownIndicator: FC<DropdownIndicatorProps> = (props) => {
  return (
    <components.DropdownIndicator {...props}>
      {props.selectProps.menuIsOpen ? <IconArrowUp /> : <IconArrowDown />}
    </components.DropdownIndicator>
  )
}

export const Select: FC<IProps> = ({
  isStroke = false,
  className,
  ...props
}) => {
  return (
    <ReactSelect
      className={classNames(styles['styled-select'], className, {
        [styles.stroke]: isStroke,
        [styles['without-stroke']]: !isStroke,
      })}
      classNamePrefix="styled-select"
      components={{ DropdownIndicator }}
      {...props}
    />
  )
}
