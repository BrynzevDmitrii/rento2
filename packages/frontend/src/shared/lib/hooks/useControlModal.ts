import { ElementType, useState } from 'react'

interface IRegistry<K> {
  getComponent: (id: string) => K
  addComponent: (id: string, component: K) => void
}

interface IResponse<K, P> {
  Component: K
  handleClick: (nameModal: P) => void
  nameOpenModal?: P
  resetModal: () => void
}

export const useControlModal = <P extends string, K = ElementType>(
  registry: IRegistry<K>,
  initNameComponent: P
): IResponse<K, P> => {
  const [nameOpenModal, setNameOpenModal] = useState<P>(initNameComponent)

  const handleClick = (nameModal: P): void => {
    setNameOpenModal(nameModal)
  }

  const resetModal = (): void => {
    setNameOpenModal(initNameComponent)
  }

  const Component = registry.getComponent(nameOpenModal)

  return { Component, handleClick, nameOpenModal, resetModal }
}
