import { useEffect, useState } from 'react'

export const useFullPath = (): string => {
  const [path, setPath] = useState('')

  useEffect(() => {
    if (typeof window !== 'undefined' && window.location !== undefined) {
      setPath(window.location.href)
    }
  }, [])

  return path
}
