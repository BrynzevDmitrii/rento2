import { useState, useEffect, useRef, useCallback } from 'react'

interface IUseSticky {
  isHeaderMode: boolean
  showHeader: boolean
}
export const useSticky = (offsetTop: number): IUseSticky => {
  const [isHeaderMode, setIsHeaderMode] = useState(false)
  const [showHeader, setShowHeader] = useState(false)
  const lastScrollTop = useRef(0)

  const toggleMode = useCallback(() => {
    const currentScrollTop = window.scrollY

    if (
      lastScrollTop.current <= currentScrollTop &&
      currentScrollTop > offsetTop
    ) {
      setIsHeaderMode(true)
      setShowHeader(true)
    } else if (
      lastScrollTop.current > currentScrollTop &&
      currentScrollTop < offsetTop
    ) {
      setIsHeaderMode(false)
      setShowHeader(false)
    } else {
      setShowHeader(false)
    }

    lastScrollTop.current = currentScrollTop
  }, [offsetTop])

  useEffect(() => {
    window.addEventListener('scroll', toggleMode)
    return () => {
      window.removeEventListener('scroll', toggleMode)
    }
  }, [toggleMode])

  return { isHeaderMode, showHeader }
}
