import {
  Dispatch,
  RefObject,
  SetStateAction,
  useEffect,
  useRef,
  useState,
} from 'react'
import { useMeasure } from 'react-use'

interface IResult {
  isVisibleText: boolean
  setIsVisibleText: Dispatch<SetStateAction<boolean>>
  isVisibleButton: boolean
  refText: RefObject<HTMLParagraphElement>
}

/**
 * Хук для управления скрытием текста. Определяет показывать/скрывать кнопку управления текста.
 * @param lineText Количество отображаемых строк текста
 * @returns {IResult} Объект:
 * - ``isVisibleText`` - видимость текста,
 * - ``setIsVisibleText`` - функция для изменения видимости текста,
 * - ``isVisibleButton`` - видимость кнопки, по которой текст меняется. Нужно чтобы показывать кнопку, если количество строк текста больше lineText,
 * - ``refText`` - реф для навешивания на элемент с текстом
 */
export const useMoreText = (lineText: number): IResult => {
  const [isVisibleText, setIsVisibleText] = useState(false)
  const [isVisibleButton, setIsVisibleButton] = useState(false)

  // TODO: возможно стоит написать свою реализацию для отслеживания высоты элемента, т.к. нужно раз отследить высоту и если была смена isVisibleButton, то убрать наблюдатель. Но здесь еще нужно подумать про ресайз экрана.
  const [setRefText, { height }] = useMeasure()

  const refText = useRef<HTMLParagraphElement>(null)

  useEffect(() => {
    if (refText.current != null) {
      setRefText(refText.current)
    }
  }, [setRefText])

  useEffect(() => {
    if (refText.current != null && !isVisibleButton) {
      const { clientHeight } = refText.current
      const { lineHeight } = getComputedStyle(refText.current)
      const lineHeightNumber = parseFloat(lineHeight)

      setIsVisibleButton(Math.round(clientHeight / lineHeightNumber) > lineText)
    }
  }, [height, isVisibleButton, lineText])

  useEffect(() => {
    isVisibleButton && setIsVisibleText(true)
  }, [isVisibleButton])

  return {
    isVisibleText,
    setIsVisibleText,
    isVisibleButton,
    refText,
  }
}
