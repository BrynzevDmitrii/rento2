import { FilterItem, IMetroStation, SortBy, SortDirection } from '@shared/api'

const getSort = (sort: {
  value: string
  label: string
}): {
  sortBy: SortBy
  sortDirection: SortDirection
} => {
  const sortDirectionPosition = sort.value.search(/Asc|Desc/g)
  if (sortDirectionPosition === -1) {
    throw new Error(
      'Неверный формат строки. Строка должна содержать сортировку Asc или Desc'
    )
  }
  const sortBy = sort.value.slice(0, sortDirectionPosition) as SortBy
  const sortDirection = sort.value
    .slice(sortDirectionPosition, sort.value.length)
    .toLowerCase() as SortDirection

  return {
    sortBy,
    sortDirection,
  }
}

const getValueArray = <T extends Record<'key', FilterItem>>(
  array: T[]
): FilterItem[] | undefined => {
  return array.length > 0 ? array.map((item) => item.key) : undefined
}

const convertArrayInValue = <T extends Record<'key', FilterItem>>(
  filters: FilterItem[] | undefined,
  arraySearch: T[]
): T[] => {
  if (filters === undefined) return []
  return arraySearch.filter((item) => {
    return filters.some((element) => item.key === element)
  })
}

const getValueRange = (value: string): number | undefined => {
  return value === '' ? undefined : +value
}

const convertValueInRange = (
  from?: number,
  to?: number
): {
  min: string
  max: string
} => {
  return {
    min: from !== undefined ? String(from) : '',
    max: to !== undefined ? String(to) : '',
  }
}

const getMetroStations = (
  metroList: IMetroStation[],
  metroId: number[]
): IMetroStation[] => {
  const metro: Record<string, IMetroStation> = {}
  for (const value of metroList) {
    metro[value.id] = value
  }

  return metroId.map((item) => metro[item]) as IMetroStation[]
}

const getArrayInQuery = (stringQuery?: string): string[] | undefined => {
  return stringQuery !== undefined ? stringQuery.split(',') : undefined
}

const getNumberInQuery = (stringQuery?: string): number | undefined => {
  return stringQuery !== undefined ? +stringQuery : undefined
}

export {
  getSort,
  getValueArray,
  convertArrayInValue,
  getValueRange,
  convertValueInRange,
  getMetroStations,
  getArrayInQuery,
  getNumberInQuery,
}
