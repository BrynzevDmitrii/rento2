export const maskPhoneNumber = (number: string): string => {
  const phoneValue = number
    .replace('+7', '')
    .replace(/\D/g, '')
    .match(/(\d{1,3})(\d{0,3})(\d{0,4})/)
  let str

  if (phoneValue !== null) {
    if (phoneValue[2] === undefined || phoneValue[2] === '') {
      str = `+7(${phoneValue[1] ?? ''}`
    } else {
      str = `+7(${phoneValue[1] ?? ''})-${phoneValue[2]}`
      if (phoneValue[3] !== undefined && phoneValue[3] !== '')
        str += `-${phoneValue[3] ?? ''}`
    }
  }

  return str ?? ''
}
