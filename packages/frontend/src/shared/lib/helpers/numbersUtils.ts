const roundInteger = (number: number): number => {
  return Math.round(number)
}

export { roundInteger }
