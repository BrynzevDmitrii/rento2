type TWords = Record<TWord, string[]>

type TWord =
  | 'ночь'
  | 'минута'
  | 'кровать'
  | 'ребёнок'
  | 'гость'
  | 'комната'
  | 'взрослый'
  | 'сутки'
  | 'диван'
  | 'вариант'
  | 'отзыв'

const pluralize = (word: TWord, count: number): string => {
  const words: TWords = {
    ночь: ['ночь', 'ночи', 'ночей'],
    минута: ['минута', 'минуты', 'минут'],
    кровать: ['кровать', 'кровати', 'кроватей'],
    взрослый: ['взрослый', 'взрослых', 'взрослых'],
    ребёнок: ['ребёнок', 'ребёнка', 'детей'],
    гость: ['гость', 'гостя', 'гостей'],
    комната: ['комната', 'комнаты', 'комнат'],
    сутки: ['сутки', 'суток', 'суток'],
    диван: ['диван', 'дивана', 'диванов'],
    вариант: ['вариант', 'варианта', 'вариантов'],
    отзыв: ['отзыв', 'отзыва', 'отзывов'],
  }

  if (count % 10 === 1 && count % 100 !== 11) {
    return words[word][0] as string
  } else if (
    count % 10 >= 2 &&
    count % 10 <= 4 &&
    (count % 100 < 10 || count % 100 >= 20)
  ) {
    return words[word][1] as string
  } else {
    return words[word][2] as string
  }
}

/**
 * Функция возвращает цвет в hex формате
 * @param colorMetro Цвет в формате 099DD4
 * @returns #099DD4
 */
const getColorMetro = (colorMetro: string): string => {
  return `#${colorMetro}`
}

// TODO: Удалить, если на бэке будет сразу формироваться в нужном виде
const getSlug = (latinName: string): string => {
  return latinName.replaceAll(' ', '-')
}

export { pluralize, getColorMetro, getSlug }
