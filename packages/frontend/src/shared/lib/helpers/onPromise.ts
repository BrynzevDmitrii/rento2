import { SyntheticEvent } from 'react'

/**
 * Временно решает конфликт ts с handleSubmit в react-hook-form.
 * Убрать, когда react-hook-form поправит ошибку
 * https://github.com/react-hook-form/react-hook-form/discussions/8020#discussioncomment-2584580
 */
export function onPromise<T>(promise: (event: SyntheticEvent) => Promise<T>) {
  return (event: SyntheticEvent) => {
    promise(event).catch((error) => {
      console.log('Unexpected error', error)
    })
  }
}
