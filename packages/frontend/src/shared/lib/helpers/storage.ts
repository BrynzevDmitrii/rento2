export const getItemLocalStorage = (key: string): string | undefined => {
  const data = localStorage.getItem(key)
  if (data != null) {
    return JSON.parse(data)
  }
  return undefined
}

export const setItemLocalStorage = <T>(key: string, data: T): void => {
  try {
    localStorage.setItem(key, JSON.stringify(data))
  } catch (e) {
    throw new Error('Error saving data from localStorage')
  }
}

export const removeItemLocalStorage = (key: string): void => {
  localStorage.removeItem(key)
}
