/**
 * Сделать один или несколько параметров необязательными
 */
type PartialBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>

export type { PartialBy }
