import { FC, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import classNames from 'classnames'

import { stringUtils } from '@shared/lib'
import { IMetroStation } from '@shared/api'
import { defaultOptionsSort } from '@shared/config'

import { AdvancedFilterShort } from '@features/filters/advanced-filter-short'
import { MainFilterShort } from '@features/filters/main-filter-short'
import { MetroFilter } from '@features/filters/metro-filter'
import { SelectGuests } from '@features/select-guests'
import { SelectDateButton } from '@features/select-date-button'

import { IFormFilter, IPropsFilter } from '../lib/types'
import { transformDataForRequest } from '../lib/helpers'

import styles from './FilterShort.module.scss'

const defaultValues: IFormFilter = {
  roomsNumber: [],
  dates: null,
  selectGuests: {
    totalAmount: 1,
    adults: 1,
    kidsAmount: 0,
    babiesKids: 0,
    grownKids: 0,
  },
  area: { min: '', max: '' },
  sortRange: defaultOptionsSort,
  appliances: [],
  availabilityTimeTo: [],
  pricePerNight: [],
  options: [],
  availablityBy: 'foot',
  metroStations: [] as IMetroStation[],
}

export const FilterShort: FC<IPropsFilter> = ({
  quantityApartment,
  isHeaderMode,
  showHeader,
  metroList,
}) => {
  const [isOpenAdvancedFilter, setIsOpenAdvancedFilter] = useState(false)
  const [isOpenMetroFilter, setIsOpenMetroFilter] = useState(false)

  const { control, handleSubmit, reset, setValue, getValues } = useForm({
    defaultValues,
  })

  const onReset = (): void => {
    reset()
  }

  const onSubmit = handleSubmit((data) => {
    console.log(data)
    console.log(transformDataForRequest(data))
  })

  return (
    <>
      <div className={classNames(styles.inner, 'container')}>
        <div className={styles['text-wrapper']}>
          <h1 className={styles.heading}>Посуточная аренда</h1>
          <p className={styles.text}>Хорошие квартиры в&nbsp;центре Москвы</p>
        </div>
        <div className={styles.image} />
      </div>

      <MainFilterShort
        classWrapperMainFilter={classNames(styles.filter)}
        control={control}
        handleClickAdvanced={() =>
          setIsOpenAdvancedFilter(!isOpenAdvancedFilter)
        }
        isHeaderMode={isHeaderMode}
        selectDateButton={
          <Controller
            control={control}
            name="dates"
            render={({ field: { value, onChange } }) => (
              <SelectDateButton fill="gray" value={value} onChange={onChange} />
            )}
          />
        }
        selectGuests={
          <Controller
            control={control}
            name="selectGuests"
            render={({ field: { value, onChange } }) => (
              <SelectGuests
                defaultGuests={defaultValues.selectGuests}
                value={value}
                onChangeGuests={onChange}
                onReset={() =>
                  reset({
                    ...getValues(),
                    selectGuests: {
                      totalAmount: 1,
                      adults: 1,
                      kidsAmount: 0,
                      babiesKids: 0,
                      grownKids: 0,
                    },
                  })
                }
              />
            )}
          />
        }
        showHeader={showHeader}
        onReset={onReset}
        onSubmit={onSubmit}
      />

      <p className={classNames(styles.quantity, 'container')}>
        {quantityApartment != null && (
          <span>
            Найдено {quantityApartment}{' '}
            {stringUtils.pluralize('вариант', quantityApartment)}
          </span>
        )}
      </p>

      <AdvancedFilterShort
        control={control}
        handleClickMetro={() => {
          setIsOpenAdvancedFilter(!isOpenAdvancedFilter)
          setIsOpenMetroFilter(!isOpenMetroFilter)
        }}
        isOpen={isOpenAdvancedFilter}
        selectGuests={
          <Controller
            control={control}
            name="selectGuests"
            render={({ field: { value, onChange } }) => (
              <SelectGuests
                defaultGuests={defaultValues.selectGuests}
                value={value}
                onChangeGuests={onChange}
                onReset={() =>
                  reset({
                    ...getValues(),
                    selectGuests: {
                      totalAmount: 1,
                      adults: 1,
                      kidsAmount: 0,
                      babiesKids: 0,
                      grownKids: 0,
                    },
                  })
                }
              />
            )}
          />
        }
        onClose={() => setIsOpenAdvancedFilter(!isOpenAdvancedFilter)}
        onReset={onReset}
        onSubmit={onSubmit}
      />
      <MetroFilter
        controlMain={control}
        isOpen={isOpenMetroFilter}
        metroStations={metroList}
        onChange={(value) => {
          setValue('metroStations', value)
          setIsOpenAdvancedFilter(!isOpenAdvancedFilter)
        }}
        onClose={() => {
          setIsOpenMetroFilter(!isOpenMetroFilter)
          setIsOpenAdvancedFilter(!isOpenAdvancedFilter)
        }}
      />
    </>
  )
}
