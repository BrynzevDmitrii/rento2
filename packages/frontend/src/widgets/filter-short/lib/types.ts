import { IMetroStation, AvailablityBy } from '@shared/api'

import {
  AppliancesChips,
  AvailabilityTimeToChips,
  OptionsChips,
  PricePerNightChips,
  RoomsNumberChips,
} from '@features/filters/advanced-filter-short'
import { IGuests } from '@features/select-guests'

interface IPropsFilter {
  isHeaderMode: boolean
  showHeader: boolean
  quantityApartment?: number
  metroList: IMetroStation[]
}

interface IChip<T> {
  key: T
  title: string
}

interface IFormFilter {
  roomsNumber: RoomsNumberChips
  dates: any // типизировать после календаря
  selectGuests: IGuests // ofAdults, ofInfants, ofChilds
  pricePerNight: PricePerNightChips
  options: OptionsChips
  area: { min: string; max: string } // areaFrom, areaTo
  sortRange: { value: string; label: string } // sortBy, sortDirection
  appliances: AppliancesChips
  availabilityTimeTo: AvailabilityTimeToChips // массив с одним значением. availabilityTimeFrom = 0
  availablityBy: AvailablityBy
  metroStations: IMetroStation[]
}

export type { IPropsFilter, IFormFilter, IChip }
