import { IRequestFilterShort } from '@shared/api'
import { defaultTimeFrom } from '@shared/config'
import { getSort } from '@shared/lib'

import { IFormFilter } from './types'

// TODO: Добавить dateIn и dateOut (dates)
const transformDataForRequest = (
  dataFilter: IFormFilter
): IRequestFilterShort => {
  const roomsNumber = dataFilter.roomsNumber.map((item) => item.key)
  const pricePerNight = dataFilter.pricePerNight.map((item) => item.key)
  const options = dataFilter.options.map((item) => item.key)
  const appliances = dataFilter.appliances.map((item) => item.key)
  const availabilityTimeTo =
    dataFilter.availabilityTimeTo[0]?.key ?? defaultTimeFrom
  const metroStationIds = dataFilter.metroStations.map((item) => item.id)

  const sort = getSort(dataFilter.sortRange)

  return {
    roomsNumber,
    ofAdults: dataFilter.selectGuests.adults,
    ofInfants: dataFilter.selectGuests.babiesKids,
    ofChilds: dataFilter.selectGuests.grownKids,
    pricePerNight,
    options,
    areaFrom: +dataFilter.area.min,
    areaTo: +dataFilter.area.max,
    sortBy: sort.sortBy,
    sortDirection: sort.sortDirection,
    appliances,
    availablityBy: dataFilter.availablityBy,
    availabilityTimeFrom: defaultTimeFrom,
    availabilityTimeTo,
    metroStationIds,
  }
}

export { transformDataForRequest }
