import Image from 'next/image'
import classNames from 'classnames'

import { CallbackForm } from '@features/callback-form'

import styles from './PersonalSearchForm.module.scss'

interface IPersonalSearchForm {
  onSubmit: () => void
}

export const PersonalSearchForm = ({
  onSubmit,
}: IPersonalSearchForm): JSX.Element => (
  <div className={styles.form}>
    <div className={styles.info}>
      <div className={styles.info__text}>
        <h2 className={styles['info__text-header']}>Новоселье с гарантией</h2>
        <p className={styles['info__text-description']}>
          Если в течение года что-то идёт не так, найдём вам новый дом
          бесплатно.
        </p>
      </div>
      <div className={styles.info__image}>
        <Image
          layout="fill"
          objectFit="cover"
          src="/images/personal-search/personal-search.png"
        />
      </div>
    </div>
    <CallbackForm
      classForm={classNames(styles.form__callback)}
      isStatic={true}
      onSubmit={onSubmit}
    />
  </div>
)
