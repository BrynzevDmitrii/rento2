import classNames from 'classnames'

import { stringUtils } from '@shared/lib'
import { RatingRange, StarWithRating } from '@shared/ui'

import styles from './ShortRating.module.scss'

interface IPropsRating {
  quantityReviews: number
  rating: {
    repairs: number
    purity: number
    location: number
    priceQuality: number
    total: number
  }
}

export const ShortRating = ({
  quantityReviews,
  rating,
}: IPropsRating): JSX.Element => {
  return (
    <>
      <div className={styles.wrapper}>
        <h2 className={styles.heading}>Рейтинг</h2>
        <div className={styles['total-rating']}>
          <StarWithRating rating={Number(rating.total ?? 0)} />(
          {quantityReviews} {stringUtils.pluralize('отзыв', quantityReviews)})
        </div>
      </div>

      <div className={styles.rating}>
        <RatingRange
          classWrapper={classNames(styles.rating__item)}
          label="Ремонт:"
          rating={rating.repairs}
        />
        <RatingRange
          classWrapper={classNames(styles.rating__item)}
          label="Расположение:"
          rating={rating.location}
        />
        <RatingRange
          classWrapper={classNames(styles.rating__item)}
          label="Чистота:"
          rating={rating.purity}
        />
        <RatingRange
          classWrapper={classNames(styles.rating__item)}
          label="Цена / качество:"
          rating={rating.priceQuality}
        />
      </div>
    </>
  )
}
