import classNames from 'classnames'
import { DetailedHTMLProps, HTMLAttributes } from 'react'

import styles from './ShortServices.module.scss'

interface IPropsServices
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}

export const ShortServices = ({
  className,
  ...props
}: IPropsServices): JSX.Element => {
  return <div className={classNames(styles.wrapper, className)} {...props} />
}
