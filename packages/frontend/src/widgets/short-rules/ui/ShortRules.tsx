import { DetailedHTMLProps, HTMLAttributes } from 'react'
import { DateTime } from 'luxon'
import Link from 'next/link'

import { messengers } from '@shared/config'
import { ButtonGeneral, IconArrowRight } from '@shared/ui'

import { ChatButtons } from '@features/messengers/chat-buttons'

import styles from './ShortRules.module.scss'

interface IPropsRules
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  checkInStart: string
  checkInEnd: string
  checkOutEnd: string
}

export const ShortRules = ({
  checkInStart,
  checkInEnd,
  checkOutEnd,
  className,
  ...props
}: IPropsRules): JSX.Element => {
  const messengerPath = {
    telegram: messengers.short.telegram,
    whatsapp: messengers.short.whatsApp,
  }

  const getTime = (date: string): string => {
    return DateTime.fromISO(date, { zone: 'Europe/Moscow' }).toLocaleString(
      DateTime.TIME_24_SIMPLE
    )
  }

  return (
    <div className={className} {...props}>
      <div className={styles.rules} />
      <div className={styles.wrapper}>
        <div className={styles.info}>
          <div className={styles.info__time}>
            <span>Заселение</span>
            <span>
              c {getTime(checkInStart)} до {getTime(checkInEnd)}
            </span>

            <span>Выезд</span>
            <span>до {getTime(checkOutEnd)}</span>
          </div>
          <p className={styles.info__text}>
            Возможность раннего или позднего заселения и въезда вы можете
            уточнить у менеджера.
          </p>
          <ChatButtons
            className={styles.info__messenger}
            paths={messengerPath}
            text="Задать вопрос"
          />
        </div>
        <div className={styles.text}>
          <p className={styles.text__block}>
            Заселение и выезд в отличное от установленного времени оплачиваются
            дополнительно.
          </p>
          <p className={styles.text__block}>
            Нарушение правил и порядка в квартире могут потребовать удержания
            залоговой суммы частично или полностью.
          </p>
        </div>
      </div>
      <Link passHref href="/docs/agreements/short">
        <ButtonGeneral
          className={styles.button}
          font="s"
          full="text"
          grade="accent"
          height="40"
          target="_blank"
        >
          Прочитать договор
          <IconArrowRight className={styles.button__icon} />
        </ButtonGeneral>
      </Link>
    </div>
  )
}
