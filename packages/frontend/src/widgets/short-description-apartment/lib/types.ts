import { DetailedHTMLProps, HTMLAttributes } from 'react'

import { ISleepingPlaces } from '@shared/api'

interface IGuest {
  maxAdults: number
  maxChildren: number
}

interface IApartmentInfo {
  guests: IGuest
  sleepingPlaces: ISleepingPlaces[]
  roomsNum: string
  area: number
}

const enum SleepingPlaceType {
  BED = 'bed',
  SOFA = 'sofa',
}

interface IDescriptionText
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  text: string
  parameters: IApartmentInfo
}

export type { IApartmentInfo, IDescriptionText, IGuest }

export { SleepingPlaceType }
