import { ISleepingPlaces } from '@shared/api'
import { stringUtils } from '@shared/lib'

import { IGuest, SleepingPlaceType } from './types'

const getGuests = (guests: IGuest): number => {
  return guests.maxAdults + guests.maxChildren
}

const getSleepingPlaces = (sleepingPlaces: ISleepingPlaces[]): string => {
  // TODO: Может reduce не нужен, если в number будет 1, то брать длину массива. Но если число будет в number, то все же нужен
  const sofa = sleepingPlaces
    .filter((item) => item.type === SleepingPlaceType.SOFA)
    .reduce(
      (previousValue, currentValue) => previousValue + currentValue.number,
      0
    )
  const bed = sleepingPlaces
    .filter((item) => item.type === SleepingPlaceType.BED)
    .reduce(
      (previousValue, currentValue) => previousValue + currentValue.number,
      0
    )

  return `${sofa} ${stringUtils.pluralize(
    'диван',
    sofa
  )} • ${bed} ${stringUtils.pluralize('кровать', bed)}`
}

export { getGuests, getSleepingPlaces }
