import { FC } from 'react'
import classNames from 'classnames'

import { ButtonGeneral, IconArrowDown, IconArrowUp } from '@shared/ui'
import { useMoreText } from '@shared/lib'

import { IDescriptionText } from '../lib/types'
import { getGuests, getSleepingPlaces } from '../lib/helpers'

import styles from './ShortDescriptionApartment.module.scss'

export const ShortDescriptionApartment: FC<IDescriptionText> = ({
  text,
  parameters,
  className,
  ...props
}) => {
  const { refText, isVisibleText, isVisibleButton, setIsVisibleText } =
    useMoreText(4)

  return (
    <div className={classNames(styles.description, className)} {...props}>
      <h2 className={styles.description__heading}>Описание</h2>

      <div className={styles.description__parameters}>
        <div>Гостей: {getGuests(parameters.guests)}</div>
        <div>Комнат: {parameters.roomsNum}</div>
        <div>{getSleepingPlaces(parameters.sleepingPlaces)}</div>
        <div>Площадь: {parameters.area} м2</div>
      </div>
      <p
        ref={refText}
        className={classNames(styles.description__text, {
          [styles.description__text_visible]: isVisibleText,
        })}
      >
        {text}
      </p>
      <div className={styles['description__img']}>
        <div className={styles['description__circle']} />
        <div className={styles.description__key} />
      </div>
      {isVisibleButton && (
        <ButtonGeneral
          className={styles.description__btn}
          font="xs"
          full="text"
          grade="neutral"
          height="40"
          onClick={() => setIsVisibleText(!isVisibleText)}
        >
          {isVisibleText ? (
            <>
              Подробнее
              <IconArrowDown className={styles['description__btn-icon']} />
            </>
          ) : (
            <>
              Свернуть
              <IconArrowUp className={styles['description__btn-icon']} />
            </>
          )}
        </ButtonGeneral>
      )}
    </div>
  )
}
