import { IMetroStation } from '@shared/api'

export interface IAddress {
  street: string
  metro: IMetroStation[]
  timeFoot: number
  timeVehicle: number
}

export interface IParameters {
  roomsNum: number | string
  area: number | string
  storey: number | string
  totalStoreys: number | string
  pricePerMonth: string
}

export interface IDetails {
  communal: string
  commission: number | string
  deposit: number | string
}
