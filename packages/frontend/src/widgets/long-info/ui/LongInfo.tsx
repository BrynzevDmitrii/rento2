import classNames from 'classnames'
import { DetailedHTMLProps, HTMLAttributes, useMemo } from 'react'

import {
  ButtonGeneral,
  IconLocation,
  IconCallOutgoing,
  ButtonIcon,
  IconHeart,
  IconLink,
  PriceLong,
} from '@shared/ui'
import { messengers } from '@shared/config'
import { useWindowDimensions } from '@shared/lib'

import { MetroInfo } from '@entities/MetroInfo'

import LongInfoDetails from './details/LongInfoDetails'
import { LongInfoParameters } from './parameters/LongInfoParameters'
import { IAddress, IDetails, IParameters } from '../lib/types'

import styles from './LongInfo.module.scss'

interface IPropsInfo
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  details: IDetails
  parameters: IParameters
  address: IAddress
  onClickBooking: () => void
  onClickCallMe: () => void
  onClickShareLink: () => void
  onClickMap: () => void
}

export function LongInfo({
  details,
  parameters,
  address,
  className,
  onClickBooking,
  onClickCallMe,
  onClickShareLink,
  onClickMap,
  ...props
}: IPropsInfo): JSX.Element {
  const clickFavorite = (): void => {
    console.log('clickFavorite')
  }

  const { widthWindow } = useWindowDimensions()

  const getHeightButton = useMemo(
    () => (widthWindow > 1023 || widthWindow < 744 ? '44' : '40'),
    [widthWindow]
  )

  const getFontButton = useMemo(
    () => (widthWindow > 1023 || widthWindow < 744 ? 'm' : 's'),
    [widthWindow]
  )

  return (
    <div className={classNames(styles.container, className)} {...props}>
      <div className={classNames(styles.action, 'flex-s-b')}>
        <ButtonGeneral
          className={styles.action__btn}
          font="s"
          full="text"
          grade="neutral"
          height="40"
          href="#mapRef"
          onClick={onClickMap}
        >
          Расположение
          <IconLocation />
        </ButtonGeneral>

        <div className={classNames(styles.action__inner, 'flex-s-b-c')}>
          <ButtonIcon
            appearance="buttonAction"
            full="stroke"
            grade="iris"
            size="32"
            onClick={clickFavorite}
          >
            <IconHeart />
          </ButtonIcon>
          <ButtonIcon
            appearance="buttonAction"
            full="stroke"
            grade="iris"
            size="32"
            onClick={onClickShareLink}
          >
            <IconLink />
          </ButtonIcon>
        </div>
      </div>

      <div className={styles.address}>
        <p className={styles.address__title}>{address.street}</p>
        <MetroInfo
          classWrapper={classNames(styles.address__metro)}
          metroStations={address.metro}
          timeFoot={address.timeFoot}
          timeVehicle={address.timeVehicle}
        />
      </div>
      <LongInfoParameters parameters={parameters} />
      <p className={classNames(styles['price'])}>
        {PriceLong(parameters.pricePerMonth)}
      </p>
      <LongInfoDetails details={details} />
      <div className={classNames(styles['buttons'])}>
        {
          // TODO: Условие должно быть на определение сенсорное устройство или нет
          widthWindow > 479 ? (
            <ButtonGeneral
              className={classNames(styles['btn'], styles['btn_first'])}
              font={getFontButton}
              full="filled"
              grade="neutral"
              height={getHeightButton}
              onClick={onClickCallMe}
            >
              <IconCallOutgoing className={styles['icon-call']} />
            </ButtonGeneral>
          ) : (
            <ButtonGeneral
              className={classNames(styles['btn'], styles['btn_first'])}
              font={getFontButton}
              full="filled"
              grade="neutral"
              height={getHeightButton}
              href={`tel:${messengers.long.phone}`}
            >
              Позвонить
            </ButtonGeneral>
          )
        }
        <ButtonGeneral
          className={classNames(styles['btn'], styles['btn_last'])}
          font={getFontButton}
          full="filled"
          height={getHeightButton}
          onClick={onClickBooking}
        >
          Записаться на просмотр
        </ButtonGeneral>
      </div>
    </div>
  )
}
