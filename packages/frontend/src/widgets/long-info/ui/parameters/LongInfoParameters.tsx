import classNames from 'classnames'

import { IParameters } from '../../lib/types'
import styles from './LongInfoParameters.module.scss'

export const LongInfoParameters = ({
  parameters,
}: {
  parameters: IParameters
}): JSX.Element => {
  return (
    <ul className={classNames(styles['list'])}>
      <li className={classNames(styles['item'])}>
        Комнат: {parameters.roomsNum}
      </li>
      <li className={classNames(styles['item'])}>
        Площадь: {parameters.area} м2
      </li>
      <li className={classNames(styles['item'])}>
        Этаж: {parameters.storey}/{parameters.totalStoreys}
      </li>
    </ul>
  )
}
