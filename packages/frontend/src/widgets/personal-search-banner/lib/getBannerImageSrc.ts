export const getBannerImageSrc = (
  widthWindow: number,
  grade: 'primary' | 'secondary'
): string => {
  const srcFolder = grade === 'primary' ? 'long-one' : 'catalog'
  switch (true) {
    case widthWindow >= 1024:
      return `/images/personal-search/${srcFolder}/personal-search-1024+.png`
    case widthWindow >= 744 && widthWindow < 1024:
      return `/images/personal-search/${srcFolder}/personal-search-744+.png`
    case widthWindow >= 480 && widthWindow < 744:
      return `/images/personal-search/${srcFolder}/personal-search-480+.png`
    case widthWindow < 480:
      return `/images/personal-search/${srcFolder}/personal-search-320+.png`
    default:
      return `/images/personal-search/${srcFolder}/personal-search-320+.png`
  }
}
