import { useMemo } from 'react'
import Image from 'next/image'
import classNames from 'classnames'

import { ButtonGeneral } from '@shared/ui'
import { useWindowDimensions } from '@shared/lib'

import { getBannerImageSrc } from '../lib/getBannerImageSrc'
import styles from './PersonalSearchBanner.module.scss'

interface IBanner {
  onClick: () => void
  grade?: 'primary' | 'secondary'
  classProps?: string
}

export const PersonalSearchBanner = ({
  onClick,
  grade = 'primary',
  classProps,
}: IBanner): JSX.Element => {
  const { widthWindow } = useWindowDimensions()
  const bannerImageSrc = useMemo(
    () => getBannerImageSrc(widthWindow, grade),
    [grade, widthWindow]
  )

  return (
    <div className={classNames(styles.banner, classProps)}>
      <div className={styles.banner__content}>
        <div className={styles.banner__image}>
          <Image layout="fill" objectFit="cover" src={bannerImageSrc} />
        </div>
        <div className={classNames(styles.banner__text, styles[grade])}>
          <p className={styles['banner__text-title']}>
            Персональный поиск квартиры
          </p>
          <p className={styles['banner__text-subtitle']}>
            Ванная с окном или французский балкон? Конечно, найдём.
          </p>
          <p className={styles['banner__text-paragraph']}>
            Поможем найти хорошую квартиру по вашим параметрам.
          </p>
        </div>
      </div>
      <ButtonGeneral
        round
        className={styles.banner__button}
        font="s"
        full="filled"
        grade={grade}
        height="40"
        onClick={onClick}
      >
        Подробнее
      </ButtonGeneral>
    </div>
  )
}
