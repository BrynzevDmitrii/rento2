import { DetailedHTMLProps, FC, HTMLAttributes } from 'react'
import classNames from 'classnames'
import Image from 'next/image'

import { DetailsLong, ModalBase, PriceLong } from '@shared/ui'
import { IMetroStation } from '@shared/api'

import { AdressInfo } from '@entities/AdressInfo'
import { MetroInfo } from '@entities/MetroInfo'

import { CallQr } from '@features/call-qr'

import styles from './CallApartmentOneModal.module.scss'

interface IApartment {
  pricePerMonth: string
  imageSrc: string
  roomNum: string
  area: number
  storey: number
  totalStoreys: number
  street: string
  metro: IMetroStation[]
  metroAvailabilityByFoot: number
  metroAvailabilityByVehicle: number
}

interface IPropsCallApartmentOne
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  phone: string
  isOpen: boolean
  apartmentInfo: IApartment
  onClose: () => void
}

export const CallApartmentOneModal: FC<IPropsCallApartmentOne> = ({
  phone,
  isOpen,
  onClose,
  apartmentInfo,
  className,
  ...props
}) => {
  return (
    <ModalBase isOpen={isOpen} onClose={onClose}>
      <div className={classNames(styles.wrapper, className)} {...props}>
        <CallQr className={styles.call} phone={phone} />

        <div className={styles['apartment-prewiew']}>
          <div className={styles['apartment-prewiew__image']}>
            <Image
              alt="room3"
              layout="fill"
              objectFit="cover"
              src={apartmentInfo.imageSrc}
            />
          </div>
          <div className={styles['apartment-prewiew__info']}>
            <div>{PriceLong(apartmentInfo.pricePerMonth)}</div>
            <div className={styles.info__details}>
              {DetailsLong({
                roomsNum: apartmentInfo.roomNum,
                area: apartmentInfo.area,
                storey: apartmentInfo.storey,
                totalStoreys: apartmentInfo.totalStoreys,
              })}
            </div>
            <div className={styles.info__adress}>
              <AdressInfo adress={apartmentInfo.street} />
            </div>
            <MetroInfo
              classDistance={classNames(styles['info__metro-distanse'])}
              classSubway={classNames(styles['info__metro-subway'])}
              metroStations={apartmentInfo.metro}
              timeFoot={apartmentInfo.metroAvailabilityByFoot}
              timeVehicle={apartmentInfo.metroAvailabilityByVehicle}
            />
          </div>
        </div>
      </div>
    </ModalBase>
  )
}
