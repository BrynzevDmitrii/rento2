export const dataSearchModal = [
  {
    id: 1,
    title: 'Действительно удобно',
    description: 'Находим жемчужины и отправляем их вам в мессенджер',
  },
  {
    id: 2,
    title: 'В ваших интересах',
    description:
      'Едем с вами на просмотр, торгуемсяс собственником за лучшие условияи составляем договор.',
  },
  {
    id: 3,
    title: 'Чувствуйте себя как дома',
    description:
      'Перед вашим въездом проведём генеральную уборку и позовём хоум-мастера.',
  },
  {
    id: 4,
    title: 'Новоселье с гарантией',
    description:
      'Оплата после заключения договора. Если в течение года что-то идёт не так, найдём вам новый дом бесплатно',
  },
]
