import { useMemo } from 'react'
import Image from 'next/image'

import { ButtonGeneral, ModalBase } from '@shared/ui'
import { useWindowDimensions } from '@shared/lib'

import { dataSearchModal } from '../model/dataSearchModal'
import styles from './PersonalSearchModal.module.scss'

interface IPersonalSearchModal {
  isOpen: boolean
  onClose: () => void
  showForm: () => void
}

const getModalImageSrc = (widthWindow: number): string => {
  switch (true) {
    case widthWindow >= 744:
      return '/images/personal-search/key-744+.png'
    case widthWindow >= 480 && widthWindow < 744:
      return '/images/personal-search/key-480+.png'
    case widthWindow < 480:
      return '/images/personal-search/key-320+.png'
    default:
      return '/images/personal-search/key-320+.png'
  }
}

export function PersonalSearchModal({
  isOpen,
  onClose,
  showForm,
}: IPersonalSearchModal): JSX.Element {
  const { widthWindow } = useWindowDimensions()

  const modalImageSrc = useMemo(
    () => getModalImageSrc(widthWindow),
    [widthWindow]
  )

  return (
    <ModalBase isOpen={isOpen} onClose={onClose}>
      <div className={styles.body}>
        <div className={styles.headline}>
          <h3 className={styles.headline__title}>
            Поможем найти{' '}
            <span className={styles['headline__title-highlighted']}>
              хорошую квартиру
            </span>{' '}
            по вашим параметрам
          </h3>
          <div className={styles.headline__image}>
            <Image layout="fill" objectFit="cover" src={modalImageSrc} />
          </div>
        </div>
        <ul className={styles.list}>
          {dataSearchModal.map((item) => (
            <li key={item.id} className={styles.list__item}>
              <p className={styles['list__item-title']}>{item.title}</p>
              <p className={styles['list__item-description']}>
                {item.description}
              </p>
            </li>
          ))}
          <ButtonGeneral
            round
            font={widthWindow < 480 ? 'm' : 'l'}
            full="filled"
            height={widthWindow < 480 ? '44' : '56'}
            onClick={showForm}
          >
            Заказать поиск
          </ButtonGeneral>
        </ul>
      </div>
    </ModalBase>
  )
}
