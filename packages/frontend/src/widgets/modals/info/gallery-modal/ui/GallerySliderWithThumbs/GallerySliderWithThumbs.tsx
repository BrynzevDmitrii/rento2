import { Swiper, SwiperProps, SwiperSlide } from 'swiper/react'
import { FC, useState } from 'react'
import SwiperCore, { Navigation, Thumbs, FreeMode } from 'swiper'
import Image from 'next/image'
import classNames from 'classnames'

import { SwiperWithButton } from '@entities/carousels/SwiperWithButton'

import styles from './GallerySliderWithThumbs.module.scss'

interface IGallerySliderWithThumbs {
  images: IImage[]
  updateActiveSlide: (swiper: SwiperCore) => void
}

interface IImage {
  alt: string
  src: string
}

export const GallerySliderWithThumbs: FC<IGallerySliderWithThumbs> = ({
  images,
  updateActiveSlide,
}) => {
  const [thumbsSwiper, setThumbsSwiper] = useState<SwiperCore | null>(null)

  const thumbsSwiperParams: SwiperProps = {
    watchSlidesProgress: true,
    freeMode: true,
    modules: [FreeMode, Navigation, Thumbs],
    spaceBetween: 10,
    onSwiper: setThumbsSwiper,
    slidesPerView: 'auto',
  }

  const mainSwiperParams = {
    loop: false,
    draggable: true,
    grabCursor: true,
    centeredSlides: false,
    modules: [FreeMode, Navigation, Thumbs],
    spaceBetween: 10,
    thumbs: {
      swiper: thumbsSwiper,
      multipleActiveThumbs: false,
    },
    onRealIndexChange: updateActiveSlide,
  }

  return (
    <div className={styles['gallery-slider']}>
      <div className={styles['main-swiper-container']}>
        <SwiperWithButton
          btnProps={{ grade: 'iris', size: '40', full: 'stroke' }}
          classBtnNext={classNames(
            styles['gallery-slider__button--next'],
            styles['gallery-slider__btn-hover']
          )}
          classBtnPrev={classNames(
            styles['gallery-slider__button--prev'],
            styles['gallery-slider__btn-hover']
          )}
          classSlideWrapper={classNames(styles.slider__image)}
          elementData={images}
          initSwiperParams={mainSwiperParams}
        >
          {(image) => (
            <Image
              alt={image.alt}
              layout="fill"
              objectFit="contain"
              src={image.src}
            />
          )}
        </SwiperWithButton>
      </div>

      <div className={styles['swiper-container']}>
        <Swiper {...thumbsSwiperParams} className={styles.slider__thumbs}>
          {images.map((image, i) => (
            <SwiperSlide key={i} className={styles['slider__thumbs-inner']}>
              <Image
                className={styles['slider__thumbs-img']}
                layout="fill"
                objectFit="cover"
                src={image.src}
              />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  )
}
