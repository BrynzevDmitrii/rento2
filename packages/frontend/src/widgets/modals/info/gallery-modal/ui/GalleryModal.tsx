import { FC, MouseEvent, useState } from 'react'
import SwiperCore from 'swiper'
import classNames from 'classnames'

import {
  IconHeart,
  ButtonIcon,
  PriceLong,
  ButtonGeneral,
  ModalBase,
} from '@shared/ui'

import { GallerySliderWithThumbs } from './GallerySliderWithThumbs/GallerySliderWithThumbs'

import styles from './GalleryModal.module.scss'

interface IApartment {
  pricePerMonth: string
  roomsNum: string
  area: number
  storey: number
  totalStoreys: number
}

interface IImage {
  alt: string
  src: string
}

interface IGalleryModalProps {
  images: IImage[]
  apartmentInfo: IApartment
  onClose: () => void
  isOpen: boolean
  onClickBooking: () => void
}

export const GalleryModal: FC<IGalleryModalProps> = ({
  images,
  isOpen,
  onClose,
  apartmentInfo,
  onClickBooking,
}) => {
  // TODO: После календаря сделать разделение для долгосрока и краткосрока. Пока непонятно как его сделать
  const renderApartmentPrewiew = (): JSX.Element => {
    const click = (
      e: MouseEvent<HTMLButtonElement | HTMLAnchorElement>
    ): void => {
      e.preventDefault()
    }

    return (
      <div className={styles['apartment-prewiew']}>
        <div className={styles['apartment-prewiew__container']}>
          <div className={styles['apartment-prewiew__info']}>
            <div className={styles.info__price}>
              {PriceLong(apartmentInfo.pricePerMonth)}
            </div>
            <div className={styles.info__details}>
              <div className={classNames(styles['info__details--item'])}>
                {`Комнат: ${apartmentInfo.roomsNum}`}
              </div>
              <div className={styles['info__details--item']}>
                {`Площадь: ${apartmentInfo.area} м2`}
              </div>
              <div className={styles['info__details--item']}>
                {`Этаж: ${apartmentInfo.storey}/${apartmentInfo.totalStoreys}`}
              </div>
            </div>
          </div>

          <div className={styles['apartment-prewiew__footer']}>
            <ButtonGeneral
              className={classNames(styles['btn-general'])}
              font="s"
              full="filled"
              height="40"
              onClick={onClickBooking}
            >
              Записаться на просмотр
            </ButtonGeneral>
            <ButtonIcon
              appearance="buttonAction"
              className={classNames(styles.favourite)}
              full="stroke"
              grade="iris"
              size="32"
              onClick={click}
            >
              <IconHeart />
            </ButtonIcon>
          </div>
        </div>
      </div>
    )
  }
  const [activeSlide, setActiveSlide] = useState(1)
  const updateActiveSlide = (swiper: SwiperCore): void => {
    setActiveSlide(swiper.realIndex + 1)
  }
  return (
    <ModalBase
      classModal={classNames(styles.modal)}
      classTitle={classNames(styles.modal__title)}
      isDarkTheme={true}
      isOpen={isOpen}
      title={`${activeSlide} из ${images.length}`}
      translate="bottom"
      onClose={onClose}
    >
      <div className={styles['modal__body']}>
        <div className={styles['modal__body--content']}>
          <div className={styles.gallery}>
            <GallerySliderWithThumbs
              images={images}
              updateActiveSlide={updateActiveSlide}
            />
          </div>

          <div>{renderApartmentPrewiew()}</div>
        </div>
      </div>
    </ModalBase>
  )
}
