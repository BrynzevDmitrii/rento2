import { FC, useEffect } from 'react'
import classNames from 'classnames'
import { useCopyToClipboard } from 'react-use'

import {
  ButtonMessenger,
  IconCopy,
  IconTelegram,
  IconVk,
  IconWhatsapp,
  ModalBase,
} from '@shared/ui'
import { useFullPath } from '@shared/lib'

import styles from './ShareLinkModal.module.scss'

interface IPropsShareLink {
  isOpen: boolean
  onClose: () => void
}

export const ShareLinkModal: FC<IPropsShareLink> = ({ isOpen, onClose }) => {
  const [state, copyToClipboard] = useCopyToClipboard()

  // TODO: Обработать ссылку на наличие квери параметров
  // TODO: Доделать шар для иконок ТГ и ВК
  const link = useFullPath()

  useEffect(() => {
    if (isOpen) state.value = ''
  }, [isOpen, state])

  return (
    <ModalBase
      classBody={classNames(styles.wrapper)}
      isOpen={isOpen}
      onClose={onClose}
    >
      <h3 className={styles.title}>Поделиться ссылкой</h3>
      <div className={styles.messengers}>
        <ButtonMessenger
          path={`https://t.me/share/url?url=${encodeURIComponent(link)}`}
        >
          <IconTelegram isColor />
        </ButtonMessenger>
        <ButtonMessenger
          path={`https://wa.me/?text=${encodeURIComponent(link)}`}
        >
          <IconWhatsapp isColor />
        </ButtonMessenger>
        <ButtonMessenger
          path={`https://vk.com/share.php?url=${encodeURIComponent(link)}`}
        >
          <IconVk isColor />
        </ButtonMessenger>
      </div>
      <div className={styles['text-wrapper']}>
        <p className={styles.text}>Или копируйте ссылку</p>
      </div>
      <div className={styles.link} onClick={() => copyToClipboard(link)}>
        <span className={styles.link__text}>{link}</span>
        <IconCopy />
      </div>
      {state.value != null && state.value !== '' && (
        <span className={styles.copy}>Ссылка скопирована</span>
      )}
    </ModalBase>
  )
}
