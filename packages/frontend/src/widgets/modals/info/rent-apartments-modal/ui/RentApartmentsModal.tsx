import { FC } from 'react'
import classNames from 'classnames'

import {
  ModalBase,
  ButtonGeneral,
  IconTelegram,
  IconWhatsapp,
} from '@shared/ui'

import styles from './RentApartmentsModal.module.scss'

interface IRentApartmentsPopupProps {
  onClose: () => void
  isOpen: boolean
  whatsappLink: string
  telegramLink: string
}

export const RentApartmentsPopup: FC<IRentApartmentsPopupProps> = ({
  onClose,
  isOpen,
  whatsappLink,
  telegramLink,
}) => {
  return (
    <ModalBase isOpen={isOpen} onClose={onClose}>
      <div className={styles.modal__body}>
        <div className={styles.img__container}>
          <div className={styles.img} />
          <div className={styles.circle} />
        </div>

        <div className={styles.text__container}>
          <div className={styles.title}>Быстро сдадим вашу квартиру</div>
          <div className={styles.subtitle}>
            Просто напишите менеждеру в чат, и мы начнём работу
          </div>
        </div>

        <ButtonGeneral
          className={classNames(styles.button, styles['button-tg'])}
          font="l"
          full="filled"
          grade="neutral"
          height="56"
          href={telegramLink}
          round={true}
          target="_blank"
        >
          Чат в Telegram
          <IconTelegram classProps={classNames(styles.icon)} isColor={true} />
        </ButtonGeneral>

        <ButtonGeneral
          className={classNames(styles.button)}
          font="l"
          full="filled"
          grade="neutral"
          height="56"
          href={whatsappLink}
          round={true}
          target="_blank"
        >
          Чат в WhatsApp
          <IconWhatsapp classProps={classNames(styles.icon)} isColor={true} />
        </ButtonGeneral>
      </div>
    </ModalBase>
  )
}
