import { ModalBase } from '@shared/ui'

import { ChatButtons } from '@features/messengers/chat-buttons'

import styles from './ErrorCallbackPopup.module.scss'

interface IErrorCallbackPopup {
  isOpen: boolean
  onClose: () => void
  paths: {
    telegram: string
    whatsapp: string
  }
}

export const ErrorCallbackPopup = ({
  isOpen,
  onClose,
  paths,
}: IErrorCallbackPopup): JSX.Element => (
  <ModalBase isOpen={isOpen} onClose={onClose}>
    <div className={styles.content}>
      <h3 className={styles.content__title}>Упс! У нас ошибка</h3>
      <p className={styles.content__description}>
        Данные не отправленны. Попробуйте ещё раз немного позже.
      </p>

      <ChatButtons appearance="neutral" paths={paths} />
    </div>
  </ModalBase>
)
