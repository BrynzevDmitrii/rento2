import { useForm } from 'react-hook-form'

import { ButtonGeneral, TextareaBase } from '@shared/ui'

import styles from './ErrorMessage.module.scss'
import { onPromise } from '@shared/lib'

interface IForm {
  errorBackend: string
}

export const ErrorMessage = ({
  handleClick,
}: {
  handleClick: (nameModal: string) => void
}): JSX.Element => {
  const { register, handleSubmit, reset } = useForm<IForm>()

  const onFormSubmit = handleSubmit((data) => {
    console.log(data)
    reset()
    handleClick('ErrorThanks')
  })

  return (
    <div className={styles.error}>
      <p className={styles.error__title}>Сообщение</p>
      <form onSubmit={onPromise(onFormSubmit)}>
        <TextareaBase
          {...register('errorBackend')}
          className={styles.input}
          placeholder="Опишите ошибку"
        />

        <ButtonGeneral
          className={styles.secondary_btn}
          font="s"
          full="filled"
          grade="secondary"
          height="40"
          type="submit"
        >
          Сообщить об ошибке
        </ButtonGeneral>
      </form>
    </div>
  )
}
