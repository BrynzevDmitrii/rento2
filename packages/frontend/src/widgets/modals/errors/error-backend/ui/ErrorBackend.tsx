import { FC } from 'react'

import { ModalBase } from '@shared/ui'
import { useControlModal } from '@shared/lib'

import { initRegistry, registry } from '../model/registry'

interface IProps {
  isOpenModal: boolean
  setIsOpenModal: (newValue: boolean) => void
}

export const ErrorBackend: FC<IProps> = ({ isOpenModal, setIsOpenModal }) => {
  const { Component, handleClick } = useControlModal<keyof typeof initRegistry>(
    registry,
    'ErrorBackendMain'
  )

  // TODO: Подумать как и куда будет отправляться сообщение если пользователь выбирает "Отправить без сообщения".
  const handleSendMessage = (): void => {
    console.log('handleSendMessage')
    setIsOpenModal(false)
  }

  return (
    <ModalBase
      isOpen={isOpenModal}
      translate="bottom"
      onClose={() => setIsOpenModal(false)}
    >
      <Component
        handleClick={handleClick}
        handleSendMessage={handleSendMessage}
      />
    </ModalBase>
  )
}
