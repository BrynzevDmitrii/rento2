import styles from './ErrorThanks.module.scss'

export const ErrorThanks = (): JSX.Element => {
  return (
    <div className={styles.error}>
      <p className={styles.error__title}>Спасибо</p>
      <p className={styles.error__description}>Скоро всё починим</p>
    </div>
  )
}
