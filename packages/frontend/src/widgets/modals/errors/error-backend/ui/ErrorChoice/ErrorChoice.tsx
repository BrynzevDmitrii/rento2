import { ButtonGeneral } from '@shared/ui'

import style from './ErrorChoice.module.scss'

export const ErrorChoice = ({
  handleClick,
  handleSendMessage,
}: {
  handleClick: (nameModal: string) => void
  handleSendMessage: () => void
}): JSX.Element => {
  return (
    <div className={style.error}>
      <p className={style.error__title}>Сообщить об ошибке</p>
      <ButtonGeneral
        font="s"
        full="filled"
        grade="neutral"
        height="40"
        onClick={() => handleSendMessage()}
      >
        Отправить без сообщения
      </ButtonGeneral>
      <ButtonGeneral
        className={style.secondary_btn}
        font="s"
        full="filled"
        grade="secondary"
        height="40"
        onClick={() => handleClick('ErrorMessage')}
      >
        Написать сообщение
      </ButtonGeneral>
    </div>
  )
}
