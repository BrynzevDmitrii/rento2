import { ElementType } from 'react'

import { Registry } from '@entities/Registry'

import { ErrorBackendMain } from '../ui/ErrorBackendMain/ErrorBackendMain'
import { ErrorChoice } from '../ui/ErrorChoice/ErrorChoice'
import { ErrorMessage } from '../ui/ErrorMessage/ErrorMessage'
import { ErrorThanks } from '../ui/ErrorThanks/ErrorThanks'

const initRegistry = {
  ErrorBackendMain,
  ErrorChoice,
  ErrorMessage,
  ErrorThanks,
}

const registry = new Registry<ElementType>(initRegistry)

export { initRegistry, registry }
