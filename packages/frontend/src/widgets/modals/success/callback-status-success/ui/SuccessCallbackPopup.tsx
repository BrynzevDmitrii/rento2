import { ButtonGeneral, ModalBase } from '@shared/ui'

import { ChatButtons } from '@features/messengers/chat-buttons'

import styles from './SuccessCallbackPopup.module.scss'

interface ISuccessCallbackPopup {
  isOpen: boolean
  onClose: () => void
  paths: {
    telegram: string
    whatsapp: string
  }
}

export const SuccessCallbackPopup = ({
  isOpen,
  onClose,
  paths,
}: ISuccessCallbackPopup): JSX.Element => (
  <ModalBase isOpen={isOpen} onClose={onClose}>
    <div className={styles.content}>
      <div className={styles.content__info}>
        <h3 className={styles['content__info-title']}>Мы приняли заявку!</h3>
        <p className={styles['content__info-description']}>
          И свяжемся с вами в рабочее время (с 9:00 до 20:00) в течении 15 минут
        </p>
      </div>

      <ChatButtons appearance="neutral" paths={paths} />

      <ButtonGeneral
        font="s"
        full="text"
        grade="neutral"
        height="40"
        onClick={onClose}
      >
        Вернуться на главную
      </ButtonGeneral>
    </div>
  </ModalBase>
)
