import { useState } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import SwiperCore, { FreeMode, Navigation, Thumbs } from 'swiper'
import Image from 'next/image'
import classNames from 'classnames'

import { SwiperWithButton } from '@entities/carousels/SwiperWithButton'

import { GallerySliderIndicator } from './GallerySliderIndicator/GallerySliderIndicator'

import styles from './GallerySlider.module.scss'

interface IImage {
  src: string
  alt: string
}
interface IGalleryProps {
  images: IImage[]
  onCloseGalleryModal: () => void
}

export function GallerySlider({
  images,
  onCloseGalleryModal,
}: IGalleryProps): JSX.Element {
  const [thumbsSwiper, setThumbsSwiper] = useState<SwiperCore | null>(null)
  const [currentSlide, setCurrentSlide] = useState(1)

  const thumbs = {
    swiper: thumbsSwiper,
    multipleActiveThumbs: false,
  }

  const mainSwiperParams = {
    draggable: true,
    grabCursor: true,
    centeredSlides: false,
    className: classNames(styles['main-swiper']),
    loop: true,
    modules: [FreeMode, Navigation, Thumbs],
    spaceBetween: 10,
    thumbs,
    onRealIndexChange: onIndexChange,
  }

  const thumbsSwiperParams = {
    watchSlidesProgress: true,
    freeMode: true,
    modules: [FreeMode, Navigation, Thumbs],
    slidesPerView: 5,
    spaceBetween: 10,
    onSwiper: setThumbsSwiper,
    className: styles.thumb,
  }

  function onIndexChange(swiper: SwiperCore): void {
    setCurrentSlide(swiper.realIndex + 1)
  }

  return (
    <div className={styles.gallery}>
      {/* основное изображение */}
      <div className={styles.gallery__inner}>
        <SwiperWithButton
          classBtnNext={classNames(styles['gallery__button-next'])}
          classBtnPrev={classNames(styles['gallery__button-prev'])}
          classSlideWrapper={classNames(styles['main-image'])}
          elementData={images}
          initSwiperParams={mainSwiperParams}
        >
          {(image) => (
            <Image
              alt={image.alt}
              layout="fill"
              objectFit="cover"
              src={image.src}
              onClick={onCloseGalleryModal}
            />
          )}
        </SwiperWithButton>
        <GallerySliderIndicator current={currentSlide} total={images.length} />
      </div>

      {/* галлерея под основным изображением */}
      <Swiper {...thumbsSwiperParams}>
        {images.map((image, i) => (
          <SwiperSlide key={i} className={classNames(styles['thumb-image'])}>
            <Image
              alt={image.alt}
              layout="fill"
              objectFit="cover"
              src={image.src}
            />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  )
}
