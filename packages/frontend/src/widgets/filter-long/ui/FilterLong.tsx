import { FC, useEffect, useMemo, useState } from 'react'
import { useForm } from 'react-hook-form'
import classNames from 'classnames'

import { stringUtils } from '@shared/lib'

import { AdvancedFilterLong } from '@features/filters/advanced-filter-long'
import { MainFilterLong } from '@features/filters/main-filter-long'
import { MetroFilter } from '@features/filters/metro-filter'

import { IPropsFilter } from '../lib/types'
import {
  transformDataForRequest,
  transformDataFilterForFormFilter,
} from '../lib/helpers'
import { defaultFilters } from '../model/const'

import styles from './FilterLong.module.scss'

export const FilterLong: FC<IPropsFilter> = ({
  filters,
  quantityApartment,
  isHeaderMode,
  showHeader,
  metroList,
  onSubmit,
  onReset,
}) => {
  const [isOpenAdvancedFilter, setIsOpenAdvancedFilter] = useState(false)
  const [isOpenMetroFilter, setIsOpenMetroFilter] = useState(false)

  const defaultValuesFilter = useMemo(() => {
    return transformDataFilterForFormFilter(
      filters ?? defaultFilters,
      metroList
    )
  }, [filters, metroList])

  const { control, handleSubmit, reset, setValue } = useForm({
    defaultValues: defaultValuesFilter,
  })

  useEffect(() => {
    reset(defaultValuesFilter)
  }, [defaultValuesFilter, reset])

  const onResetFilter = (): void => {
    setIsOpenAdvancedFilter(false)
    reset()
    onReset()
  }

  const submit = handleSubmit((data) => {
    onSubmit(transformDataForRequest(data))
  })

  return (
    <>
      <div className={classNames(styles.inner, 'container')}>
        <div className={styles['text-wrapper']}>
          <h1 className={styles.heading}>Аренда на длительный срок</h1>
          <p className={styles.text}>
            Квартиры со всех ресурсов в одном каталоге
          </p>
        </div>
        <div className={styles.image} />
      </div>

      <MainFilterLong
        classWrapperMainFilter={classNames(styles.filter)}
        control={control}
        handleClickAdvanced={() =>
          setIsOpenAdvancedFilter(!isOpenAdvancedFilter)
        }
        isHeaderMode={isHeaderMode}
        showHeader={showHeader}
        onReset={onResetFilter}
        onSubmit={submit}
      />

      <p className={classNames(styles.quantity, 'container')}>
        {quantityApartment != null && (
          <span>
            Найдено {quantityApartment}{' '}
            {stringUtils.pluralize('вариант', quantityApartment)}
          </span>
        )}
      </p>

      <AdvancedFilterLong
        control={control}
        handleClickMetro={() => {
          setIsOpenAdvancedFilter(!isOpenAdvancedFilter)
          setIsOpenMetroFilter(!isOpenMetroFilter)
        }}
        isOpen={isOpenAdvancedFilter}
        onClose={() => setIsOpenAdvancedFilter(!isOpenAdvancedFilter)}
        onReset={onResetFilter}
        onSubmit={submit}
      />
      <MetroFilter
        controlMain={control}
        isOpen={isOpenMetroFilter}
        metroStations={metroList}
        onChange={(value) => {
          setValue('metroStations', value)
          setIsOpenAdvancedFilter(!isOpenAdvancedFilter)
        }}
        onClose={() => {
          setIsOpenMetroFilter(!isOpenMetroFilter)
          setIsOpenAdvancedFilter(!isOpenAdvancedFilter)
        }}
      />
    </>
  )
}
