import { IMetroStation, AvailablityBy, IRequestFilterLong } from '@shared/api'

import {
  AdditionalFeaturesChips,
  ApartmentFurnitureChips,
  AppliancesChips,
  AvailabilityTimeToChips,
  BuildingTypeChips,
  FloorTypeChips,
  RoomsNumberChips,
} from '@features/filters/advanced-filter-long'

interface IPropsFilter {
  filters: IRequestFilterLong | null
  isHeaderMode: boolean
  showHeader: boolean
  quantityApartment?: number
  metroList: IMetroStation[]
  onSubmit: (filters: IRequestFilterLong) => void
  onReset: () => void
}

interface IChip<T> {
  key: T
  title: string
}

interface IFormFilter {
  roomsNumber: RoomsNumberChips
  additionalFeatures: AdditionalFeaturesChips // areChildrenAllowed, arePetsAllowed
  priceRange: { min: string; max: string } // priceFrom, priceTo
  area: { min: string; max: string } // areaFrom, areaTo
  floorRange: { min: string; max: string } // floorFrom, floorTo
  floorType: FloorTypeChips
  sortRange: { value: string; label: string } // sortBy, sortDirection
  appliances: AppliancesChips
  availabilityTimeTo: AvailabilityTimeToChips // массив с одним значением. availabilityTimeFrom = 0
  apartmentFurniture: ApartmentFurnitureChips
  buildingType: BuildingTypeChips
  availablityBy: AvailablityBy | ''
  metroStations: IMetroStation[]
}

type QueryFilters = Partial<Record<keyof IRequestFilterLong, string>>

export type { IPropsFilter, IFormFilter, IChip, QueryFilters }
