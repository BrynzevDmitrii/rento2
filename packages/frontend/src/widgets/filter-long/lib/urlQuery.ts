import { stringify } from 'query-string'

import { IRequestFilterLong, SortBy, SortDirection } from '@shared/api'
import { getArrayInQuery, getNumberInQuery } from '@shared/lib'

import { QueryFilters } from './types'

/**
 *
 * @param filters Отфильтрованные значение, подготовленные для отправки на бэк
 */
const getUrlQuery = (filters: IRequestFilterLong): string => {
  return stringify(filters, { arrayFormat: 'comma' })
}

// TODO: Может быть провалидировать их на удаление скриптов и др...
/**
 * Преобразует квери параметры из Урл к виду запроса, который отправляется на бэк
 * @param query Квери параметры, полученные из Url
 * @returns IRequestFilterLong
 */
const parseUrlQuery = (query: QueryFilters): IRequestFilterLong => {
  const data = {
    roomsNumber: getArrayInQuery(query.roomsNumber),
    areChildrenAllowed:
      query.areChildrenAllowed !== undefined ? true : undefined,
    arePetsAllowed: query.arePetsAllowed !== undefined ? true : undefined,
    priceFrom: getNumberInQuery(query.priceFrom),
    priceTo: getNumberInQuery(query.priceTo),
    areaFrom: getNumberInQuery(query.areaFrom),
    areaTo: getNumberInQuery(query.areaTo),
    floorFrom: getNumberInQuery(query.floorFrom),
    floorTo: getNumberInQuery(query.floorTo),
    floorType: getArrayInQuery(query.floorType),
    appliances: getArrayInQuery(query.appliances),
    availablityBy: query.availablityBy,
    availabilityTimeTo: getNumberInQuery(query.availabilityTimeTo),
    apartmentFurniture: getArrayInQuery(query.apartmentFurniture),
    buildingType: getArrayInQuery(query.buildingType),
    metroStationIds:
      query.metroStationIds !== undefined
        ? query.metroStationIds.split(',').map((item) => +item)
        : undefined,
  }

  const dataRequest: IRequestFilterLong = {
    sortBy: query.sortBy as SortBy,
    sortDirection: query.sortDirection as SortDirection,
  }

  return {
    ...dataRequest,
    ...Object.fromEntries(Object.entries(data).filter(([_, v]) => v != null)),
  }
}

export { getUrlQuery, parseUrlQuery }
