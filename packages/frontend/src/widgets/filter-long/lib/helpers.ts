import { IMetroStation, IRequestFilterLong } from '@shared/api'
import { defaultOptionsSort, optionsSort } from '@shared/config'
import {
  convertArrayInValue,
  convertValueInRange,
  getMetroStations,
  getSort,
  getValueArray,
  getValueRange,
} from '@shared/lib'

import { filtersLong } from '@features/filters/advanced-filter-long'

import { IFormFilter } from './types'

/**
 * Преобразует данные фильтра долгоскрока из формата бэка в формат удобный на фронте
 * @param dataFilter IRequestFilterLong
 * @param metroList Список станций метро
 * @returns Данные формы
 */
const transformDataFilterForFormFilter = (
  dataFilter: IRequestFilterLong,
  metroList: IMetroStation[]
): IFormFilter => {
  const areChildrenAllowed =
    dataFilter.areChildrenAllowed !== undefined
      ? filtersLong.additionalFeatures.filter(
          (item) => item.key === 'areChildrenAllowed'
        )
      : []

  const arePetsAllowed =
    dataFilter.areChildrenAllowed !== undefined
      ? filtersLong.additionalFeatures.filter(
          (item) => item.key === 'arePetsAllowed'
        )
      : []

  const roomsNumber = convertArrayInValue(
    dataFilter.roomsNumber,
    filtersLong.roomsNumber
  )
  const floorType = convertArrayInValue(
    dataFilter.floorType,
    filtersLong.floorType
  )
  const appliances = convertArrayInValue(
    dataFilter.appliances,
    filtersLong.appliancesFirst.concat(filtersLong.appliancesSecond)
  )
  const apartmentFurniture = convertArrayInValue(
    dataFilter.apartmentFurniture,
    filtersLong.apartmentFurniture
  )
  const buildingType = convertArrayInValue(
    dataFilter.buildingType,
    filtersLong.buildingType
  )

  const availabilityTimeTo =
    dataFilter.availabilityTimeTo !== undefined
      ? filtersLong.availabilityTimeTo.filter((item) => {
          return dataFilter.availabilityTimeTo === item.key
        })
      : []

  const metroStations =
    dataFilter.metroStationIds !== undefined
      ? getMetroStations(metroList, dataFilter.metroStationIds)
      : []

  const priceRange = convertValueInRange(
    dataFilter.priceFrom,
    dataFilter.priceTo
  )
  const area = convertValueInRange(dataFilter.areaFrom, dataFilter.areaTo)
  const floorRange = convertValueInRange(
    dataFilter.floorFrom,
    dataFilter.floorTo
  )

  const sort =
    dataFilter.sortBy +
    dataFilter.sortDirection.charAt(0).toUpperCase() +
    dataFilter.sortDirection.slice(1)
  const sortRange =
    optionsSort.find((item) => item.value === sort) ?? defaultOptionsSort

  return {
    roomsNumber,
    additionalFeatures: [...areChildrenAllowed, ...arePetsAllowed],
    priceRange,
    area,
    floorRange,
    floorType,
    sortRange,
    appliances,
    availabilityTimeTo,
    apartmentFurniture,
    buildingType,
    availablityBy: dataFilter.availablityBy ?? '',
    metroStations,
  }
}

/**
 * Преобразует данные фильтра долгоскрока из формата удобного на фронте в формат необходимый для отправки на бэк
 * @param dataFilter IFormFilter
 * @returns Данные для отправки на бэк
 */
const transformDataForRequest = (
  dataFilter: IFormFilter
): IRequestFilterLong => {
  const areChildrenAllowed = dataFilter.additionalFeatures.some(
    (item) => item.key === 'areChildrenAllowed'
  )
    ? true
    : undefined
  const arePetsAllowed = dataFilter.additionalFeatures.some(
    (item) => item.key === 'arePetsAllowed'
  )
    ? true
    : undefined

  const roomsNumber = getValueArray(dataFilter.roomsNumber)
  const floorType = getValueArray(dataFilter.floorType)
  const appliances = getValueArray(dataFilter.appliances)
  const availabilityTimeTo = dataFilter.availabilityTimeTo?.[0]?.key
  const apartmentFurniture = getValueArray(dataFilter.apartmentFurniture)

  const buildingType = getValueArray(dataFilter.buildingType)
  const metroStationIds =
    dataFilter.metroStations.length > 0
      ? dataFilter.metroStations.map((item) => item.id)
      : undefined

  const sort = getSort(dataFilter.sortRange)

  const data = {
    roomsNumber,
    areChildrenAllowed,
    arePetsAllowed,
    priceFrom: getValueRange(dataFilter.priceRange.min),
    priceTo: getValueRange(dataFilter.priceRange.max),
    areaFrom: getValueRange(dataFilter.area.min),
    areaTo: getValueRange(dataFilter.area.max),
    floorFrom: getValueRange(dataFilter.floorRange.min),
    floorTo: getValueRange(dataFilter.floorRange.max),
    floorType,
    appliances,
    availablityBy:
      dataFilter.availablityBy.length > 0
        ? dataFilter.availablityBy
        : undefined,
    availabilityTimeTo,
    apartmentFurniture,
    buildingType,
    metroStationIds,
  }

  const dataRequest: IRequestFilterLong = {
    sortBy: sort.sortBy,
    sortDirection: sort.sortDirection,
  }

  return {
    ...dataRequest,
    ...Object.fromEntries(Object.entries(data).filter(([_, v]) => v != null)),
  }
}

export { transformDataFilterForFormFilter, transformDataForRequest }
