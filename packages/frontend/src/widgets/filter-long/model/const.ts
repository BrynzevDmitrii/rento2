import { IRequestFilterLong } from '@shared/api'
import { defaultOptionsSort } from '@shared/config'
import { getSort } from '@shared/lib'

export const defaultFilters: IRequestFilterLong = getSort(defaultOptionsSort)

export const KEY_FILTER_LOCAL_STORAGE = 'filter_long'
