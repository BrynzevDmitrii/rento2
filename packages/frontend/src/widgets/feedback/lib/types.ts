export interface IFeedbackSingle {
  id: number
  user: IUser
  apartment: IApartment
  rating: IRating
  totalRating: number
  feedback: string
}

export interface IUser {
  profilePhoto: string
  name: string
}

export interface IApartment {
  address: string
  endDate: string
  apartmentPhoto: string
}

export interface IRating {
  renovation: number
  cleanliness: number
  location: number
  priceQuality: number
}

export type TModal =
  | 'FeedbackLeaveOne'
  | 'FeedbackLeaveTwo'
  | 'FeedbackLeaveThree'
  | 'PhotoUploader'
  | 'FeedbackInfo'

export type TFeedbackForm = Omit<IFeedbackSingle, 'apartment' | 'totalRating'>
