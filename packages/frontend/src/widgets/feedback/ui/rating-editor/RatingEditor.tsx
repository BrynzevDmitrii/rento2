import { DetailedHTMLProps, HTMLAttributes } from 'react'
import classNames from 'classnames'

import { RatingStar } from '@shared/ui'

import styles from './RatingEditor.module.scss'

interface IRatingEditor
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  label: string
  rating: number
  setRating: (rating: number) => void
}

export const RatingEditor = ({
  rating,
  label,
  setRating,
  ...props
}: IRatingEditor): JSX.Element => (
  <div {...props} className={classNames(props.className, styles.wrapper)}>
    <p className={styles.label}>{label}</p>
    <RatingStar
      className={classNames(styles.star)}
      isEditable={true}
      rating={rating}
      setRating={(rating) => setRating(rating)}
    />
  </div>
)
