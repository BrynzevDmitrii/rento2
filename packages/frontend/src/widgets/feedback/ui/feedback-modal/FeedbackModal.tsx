import classNames from 'classnames'

import {
  Avatar,
  ButtonGeneral,
  IconArrowRight,
  RatingRange,
  StarWithRating,
} from '@shared/ui'

import { AdressInfo } from '@entities/AdressInfo'

import { IFeedbackSingle } from '@widgets/feedback/lib/types'

import styles from './FeedbackModal.module.scss'

interface IFeedbackModal {
  feedback: IFeedbackSingle | null
}

export const FeedbackModal = ({
  feedback,
}: IFeedbackModal): JSX.Element | null => {
  if (feedback === null) return null
  return (
    <div className={styles.feedback}>
      <div className={styles.feedback__user}>
        <div className={styles['feedback__user-photo']}>
          <Avatar
            classProps={classNames(styles.avatar__first)}
            src={feedback.apartment.apartmentPhoto}
          />
          <Avatar
            classProps={classNames(styles.avatar__second)}
            name={feedback.user.name}
            src={feedback.user.profilePhoto}
          />
        </div>
        <span className={styles['feedback__user-name']}>
          {feedback.user.name}
        </span>
      </div>
      <div className={styles.feedback__apartment}>
        <AdressInfo
          adress={feedback.apartment.address}
          classAdress={classNames(styles['feedback__apartment-address'])}
          tagName="span"
        />
        <span className={styles['feedback__apartment-date']}>
          {feedback.apartment.endDate}
        </span>
      </div>
      <div className={styles.feedback__rating}>
        <div className={styles['feedback__rating-title']}>
          Рейтинг
          <StarWithRating rating={feedback.totalRating} />
        </div>
        <div className={styles['feedback__rating-box']}>
          <RatingRange
            classWrapper={classNames(styles['feedback__rating-item'])}
            label="Ремонт:"
            rating={feedback.rating.renovation}
          />
          <RatingRange
            classWrapper={classNames(styles['feedback__rating-item'])}
            label="Расположение:"
            rating={feedback.rating.location}
          />
          <RatingRange
            classWrapper={classNames(styles['feedback__rating-item'])}
            label="Чистота:"
            rating={feedback.rating.cleanliness}
          />
          <RatingRange
            classWrapper={classNames(styles['feedback__rating-item'])}
            label="Цена / качество:"
            rating={feedback.rating.priceQuality}
          />
        </div>
      </div>
      <div className={styles.feedback__text}>{feedback.feedback}</div>
      <div className={styles.feedback__button}>
        {/* //TODO: заменить на кнопку-ссылку, как появится */}
        <ButtonGeneral
          round
          className={classNames(styles['feedback__button-item'])}
          font="s"
          full="text"
          grade="neutral"
          height="40"
        >
          Все отзывы
          <IconArrowRight
            className={classNames(styles['feedback__button-arrow'])}
          />
        </ButtonGeneral>
      </div>
    </div>
  )
}
