import { useEffect, useState } from 'react'
import Image from 'next/image'
import classNames from 'classnames'
import { Control, Controller, useWatch } from 'react-hook-form'
import ImageUploading, { ImageListType } from 'react-images-uploading'

import {
  ButtonGeneral,
  ButtonIcon,
  IconArrowDownRound,
  IconArrowLeft,
  IconTrushSquare,
  IconDocumentUpload,
} from '@shared/ui'
import { useWindowDimensions } from '@shared/lib'

import { TFeedbackForm, TModal } from '@widgets/feedback/lib/types'

import styles from './PhotoUploader.module.scss'

interface IPhotoUploader {
  handleClick: (nameModal: TModal) => void
  control: Control<TFeedbackForm>
}

interface IPhotoUploaderBlock {
  dragProps: {
    onDrop: (e: any) => void
    onDragEnter: (e: any) => void
    onDragLeave: (e: any) => void
    onDragOver: (e: any) => void
    onDragStart: (e: any) => void
  }
  onImageUpload: () => void
}

interface IPhotoEditor {
  src: string
  onImageRemove: (index: number) => void
}

const LargePhotoUploader = ({
  dragProps,
  onImageUpload,
}: IPhotoUploaderBlock): JSX.Element => (
  <div className={styles.uploader__zone} {...dragProps}>
    <span className={styles['uploader__zone-text']}>
      Загрузите фото, или перетяните в это поле
      <IconArrowDownRound className={classNames(styles.uploader__icon)} />
    </span>
    <ButtonGeneral
      className={classNames(styles['uploader__zone-button'])}
      font="s"
      full="filled"
      grade="neutral"
      height="40"
      onClick={onImageUpload}
    >
      Загрузить фото
      <IconDocumentUpload />
    </ButtonGeneral>
  </div>
)

const SmallPhotoUploader = ({
  dragProps,
  onImageUpload,
}: IPhotoUploaderBlock): JSX.Element => (
  <ButtonGeneral
    className={classNames(styles['uploader__zone-button'])}
    font="s"
    full="filled"
    grade="neutral"
    height="40"
    onClick={onImageUpload}
    {...dragProps}
  >
    Выбрать фото
    <IconDocumentUpload />
  </ButtonGeneral>
)

const PhotoEditor = ({ src, onImageRemove }: IPhotoEditor): JSX.Element => (
  <div
    className={classNames(
      styles.uploader__zone,
      styles['uploader__zone-filled']
    )}
  >
    <Image height={180} src={src} width={180} />
    <ButtonGeneral
      className={classNames(styles['uploader__zone-button'])}
      font="s"
      full="filled"
      grade="neutral"
      height="40"
      onClick={() => onImageRemove(0)}
    >
      Удалить
      <IconTrushSquare />
    </ButtonGeneral>
  </div>
)

export const PhotoUploader = ({
  handleClick,
  control,
}: IPhotoUploader): JSX.Element => {
  const userInfo = useWatch({ control, name: 'user' })
  const [images, setImages] = useState<ImageListType>([
    { dataURL: userInfo.profilePhoto },
  ])
  const { widthWindow } = useWindowDimensions()
  useEffect(() => {
    setImages([{ dataURL: userInfo.profilePhoto }])
  }, [userInfo.profilePhoto])
  const src = images[0] !== undefined ? (images[0]?.dataURL as string) : ''
  return (
    <Controller
      control={control}
      name="user"
      render={({ field: { onChange } }) => (
        <div className={styles.uploader}>
          <div className={styles.uploader__head}>
            <div className={styles['uploader__head-title']}>
              <ButtonIcon
                appearance="arrowNavigation"
                full="stroke"
                grade="neutral"
                size="24"
                onClick={() => handleClick('FeedbackLeaveOne')}
              >
                <IconArrowLeft />
              </ButtonIcon>
              Фото для отзыва
            </div>
          </div>
          <ImageUploading
            value={images}
            onChange={(imageList: ImageListType) => setImages(imageList)}
          >
            {({ onImageUpload, onImageRemove, dragProps }) =>
              src === '' ? (
                widthWindow < 743 ? (
                  <SmallPhotoUploader
                    dragProps={dragProps}
                    onImageUpload={onImageUpload}
                  />
                ) : (
                  <LargePhotoUploader
                    dragProps={dragProps}
                    onImageUpload={onImageUpload}
                  />
                )
              ) : (
                <PhotoEditor src={src} onImageRemove={onImageRemove} />
              )
            }
          </ImageUploading>
          <div className={styles.uploader__button}>
            <ButtonGeneral
              className={classNames(styles['uploader__button-item'])}
              font="s"
              full="filled"
              grade="secondary"
              height="40"
              round={true}
              onClick={() => {
                onChange({ ...userInfo, profilePhoto: src })
                handleClick('FeedbackLeaveOne')
              }}
            >
              Применить
            </ButtonGeneral>
          </div>
        </div>
      )}
    />
  )
}
