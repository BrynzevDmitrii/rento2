import Image from 'next/image'

import styles from './FeedbackInfo.module.scss'

export const FeedbackInfo = (): JSX.Element => (
  <div className={styles.info}>
    <div className={styles.info__image}>
      <Image
        layout="fill"
        objectFit="cover"
        src="/images/popups/feedback/feedback-done.png"
      />
      <div className={styles['info__image-circle']} />
    </div>
    <div className={styles.info__text}>
      <h2 className={styles['info__text-title']}>Спасибо за отзыв!</h2>
      <span className={styles['info__text-subtitle']}>
        Он скоро появится на сайте
      </span>
    </div>
  </div>
)
