import classNames from 'classnames'

import styles from './ProgressBar.module.scss'

interface IProgressBar {
  stage: '1/3' | '2/3' | '3/3'
}

export const ProgressBar = ({ stage }: IProgressBar): JSX.Element => (
  <div className={styles.progress}>
    {stage}
    <div className={styles.stage}>
      <div
        className={classNames(styles.stage__item, styles['stage__item-active'])}
      />
      <div
        className={classNames(styles.stage__item, {
          [styles['stage__item-active']]: stage === '2/3' || stage === '3/3',
        })}
      />
      <div
        className={classNames(styles.stage__item, {
          [styles['stage__item-active']]: stage === '3/3',
        })}
      />
    </div>
  </div>
)
