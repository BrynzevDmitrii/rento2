import classNames from 'classnames'
import { Control, Controller, useWatch } from 'react-hook-form'

import {
  Avatar,
  ButtonGeneral,
  IconAdd,
  IconEdit2,
  InputBase,
} from '@shared/ui'

import { TFeedbackForm, TModal } from '@widgets/feedback/lib/types'

import { ProgressBar } from '../progress-bar/ProgressBar'
import styles from './FeedbackLeaveOne.module.scss'

interface IFeedbackLeaveOne {
  handleClick: (nameModal: TModal) => void
  control: Control<TFeedbackForm>
}

const PhotoButtonContent = ({ isEmpty }: { isEmpty: boolean }): JSX.Element => (
  <>
    {isEmpty ? 'Добавить фото' : 'Редактировать'}
    {isEmpty ? (
      <IconAdd
        className={classNames(styles['feedback__user-icon'])}
        figure="circle"
      />
    ) : (
      <IconEdit2 className={classNames(styles['feedback__user-icon'])} />
    )}
  </>
)

export const FeedbackLeaveOne = ({
  handleClick,
  control,
}: IFeedbackLeaveOne): JSX.Element => {
  const userInfo = useWatch({ control, name: 'user' })
  return (
    <Controller
      control={control}
      name="user"
      render={({ field: { onChange } }) => (
        <div className={styles.feedback}>
          <div className={styles.feedback__head}>
            Личные данные
            <ProgressBar stage="1/3" />
          </div>
          <div className={styles.feedback__user}>
            <div className={styles['feedback__user-avatar']}>
              <Avatar name={userInfo.name} src={userInfo.profilePhoto} />
              <ButtonGeneral
                className={classNames(styles['feedback__user-button'])}
                font="s"
                full="text"
                grade="neutral"
                height="40"
                onClick={() => handleClick('PhotoUploader')}
              >
                <PhotoButtonContent isEmpty={userInfo.profilePhoto === ''} />
              </ButtonGeneral>
            </div>
            <div className={styles['feedback__user-name']}>
              <InputBase
                label=""
                placeholder="Имя*"
                value={userInfo.name}
                onChange={(e) =>
                  onChange({ ...userInfo, name: e.target.value })
                }
              />
            </div>
          </div>
          <div className={styles.feedback__button}>
            <ButtonGeneral
              className={classNames(styles['feedback__button-item'])}
              disabled={userInfo.name === ''}
              font="s"
              full="filled"
              grade="secondary"
              height="40"
              round={true}
              onClick={() => handleClick('FeedbackLeaveTwo')}
            >
              Далее
            </ButtonGeneral>
          </div>
        </div>
      )}
    />
  )
}
