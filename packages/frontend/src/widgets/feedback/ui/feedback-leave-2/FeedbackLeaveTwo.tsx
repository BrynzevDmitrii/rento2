import classNames from 'classnames'
import {
  Control,
  Controller,
  UseFormGetValues,
  useWatch,
} from 'react-hook-form'

import { ButtonGeneral, ButtonIcon, IconArrowLeft } from '@shared/ui'

import {
  IFeedbackSingle,
  TFeedbackForm,
  TModal,
} from '@widgets/feedback/lib/types'

import { RatingEditor } from '../rating-editor/RatingEditor'
import { ProgressBar } from '../progress-bar/ProgressBar'
import styles from './FeedbackLeaveTwo.module.scss'

interface IFeedbackLeaveTwo {
  handleClick: (nameModal: TModal) => void
  control: Control<TFeedbackForm>
  getValues: UseFormGetValues<TFeedbackForm>
}

const checkRating = (rating: IFeedbackSingle['rating']): boolean =>
  Object.values(rating).includes(0)

export const FeedbackLeaveTwo = ({
  handleClick,
  control,
  getValues,
}: IFeedbackLeaveTwo): JSX.Element => {
  const rating = useWatch({ control, name: 'rating' })
  return (
    <Controller
      control={control}
      name="rating"
      render={({ field: { onChange } }) => (
        <div className={styles.feedback}>
          <div className={styles.feedback__head}>
            <div className={styles['feedback__head-title']}>
              <ButtonIcon
                appearance="arrowNavigation"
                full="stroke"
                grade="neutral"
                size="24"
                onClick={() => handleClick('FeedbackLeaveOne')}
              >
                <IconArrowLeft />
              </ButtonIcon>
              Рейтинг
            </div>
            <ProgressBar stage="2/3" />
          </div>
          <div className={styles.feedback__rating}>
            <RatingEditor
              label="Ремонт:"
              rating={rating.renovation}
              setRating={(value) =>
                onChange({ ...getValues('rating'), renovation: value })
              }
            />
            <RatingEditor
              label="Чистота:"
              rating={rating.cleanliness}
              setRating={(value) =>
                onChange({ ...getValues('rating'), cleanliness: value })
              }
            />
            <RatingEditor
              label="Расположение:"
              rating={rating.location}
              setRating={(value) =>
                onChange({ ...getValues('rating'), location: value })
              }
            />
            <RatingEditor
              label="Цена / качество:"
              rating={rating.priceQuality}
              setRating={(value) =>
                onChange({ ...getValues('rating'), priceQuality: value })
              }
            />
          </div>
          <div className={styles.feedback__button}>
            <ButtonGeneral
              className={classNames(styles['feedback__button-item'])}
              disabled={checkRating(rating)}
              font="s"
              full="filled"
              grade="secondary"
              height="40"
              round={true}
              onClick={() => handleClick('FeedbackLeaveThree')}
            >
              Далее
            </ButtonGeneral>
          </div>
        </div>
      )}
    />
  )
}
