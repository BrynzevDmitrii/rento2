import { SwiperProps } from 'swiper/react'
import { Navigation, Pagination } from 'swiper'
import classNames from 'classnames'

import { useWindowDimensions } from '@shared/lib'
import { ButtonGeneral, StarWithRating, Avatar } from '@shared/ui'

import { SwiperWithButton } from '@entities/carousels/SwiperWithButton'
import { AdressInfo } from '@entities/AdressInfo'

import { IFeedbackSingle } from '@widgets/feedback/lib/types'

import styles from './FeedbackSlider.module.scss'

interface IFeedbackSlider {
  feedbacks: IFeedbackSingle[]
  onSingleOpen: (feedback: IFeedbackSingle) => void
  onFeedbackLeave: () => void
}

export const FeedbackSlider = ({
  feedbacks,
  onSingleOpen,
  onFeedbackLeave,
}: IFeedbackSlider): JSX.Element => {
  const { widthWindow } = useWindowDimensions()

  const needNavigation = (): boolean => {
    if (widthWindow >= 1024) {
      return feedbacks.length > 3
    } else {
      return widthWindow >= 744 ? feedbacks.length > 2 : feedbacks.length > 1
    }
  }

  const initParams: SwiperProps = {
    modules: [Navigation, Pagination],
    slidesPerView: 3,
    className: classNames(styles['feedback__slider-wrapper']),
    breakpoints: {
      1248: {
        slidesPerView: 3,
        pagination: { enabled: false },
      },
      1024: {
        slidesPerView: 3,
        pagination: { enabled: false },
      },

      744: {
        slidesPerView: 2,
        pagination: { enabled: true },
      },
      480: {
        slidesPerView: 1,
        pagination: { enabled: true },
      },
      0: {
        slidesPerView: 1,
        pagination: { enabled: true },
      },
    },
    pagination: {
      dynamicBullets: true,
      clickable: true,
      horizontalClass: classNames(styles.feedback__pagination),
      bulletClass: classNames(styles['feedback__pagination-bullet']),
    },
  }
  return (
    <div className={styles.feedback}>
      <h2 className={styles.feedback__title}>Отзывы</h2>
      {feedbacks.length !== 0 && (
        <div
          className={classNames(styles.feedback__slider, {
            [styles['feedback__slider-large']]: needNavigation(),
          })}
        >
          <SwiperWithButton
            classBtnNext={classNames(styles['feedback__btn-next'], {
              [styles['feedback__btn-hidden']]:
                widthWindow < 1024 || !needNavigation(),
            })}
            classBtnPrev={classNames(styles['feedback__btn-prev'], {
              [styles['feedback__btn-hidden']]:
                widthWindow < 1024 || !needNavigation(),
            })}
            elementData={feedbacks}
            initSwiperParams={initParams}
          >
            {(feedback) => (
              <div
                key={feedback.id}
                className={styles.card}
                onClick={() => onSingleOpen(feedback)}
              >
                <div className={styles.card__info}>
                  <div className={styles.info__user}>
                    <div className={styles['info__user-photo']}>
                      <Avatar
                        classProps={classNames(styles.avatar__first)}
                        src={feedback.apartment.apartmentPhoto}
                      />
                      <Avatar
                        classProps={classNames(styles.avatar__second)}
                        name={feedback.user.name}
                        src={feedback.user.profilePhoto}
                      />
                    </div>
                    <span className={styles['info__user-name']}>
                      {feedback.user.name}
                    </span>
                  </div>
                  <div className={styles.info__apartment}>
                    <div className={styles['info__apartment-wrapper']}>
                      <AdressInfo
                        adress={feedback.apartment.address}
                        classAdress={classNames(
                          styles['info__apartment-address']
                        )}
                        tagName="span"
                      />
                      <StarWithRating rating={feedback.totalRating} />
                    </div>
                    <span className={styles['info__apartment-date']}>
                      {feedback.apartment.endDate}
                    </span>
                  </div>
                </div>
                <div className={styles.card__feedback}>
                  <p className={styles['card__feedback-text']}>
                    {feedback.feedback}
                  </p>
                  <ButtonGeneral
                    className={classNames(styles['card__feedback-button'])}
                    font="xs"
                    full="text"
                    grade="neutral"
                    height="32"
                    onClick={() => onSingleOpen(feedback)}
                  >
                    Читать полностью
                  </ButtonGeneral>
                </div>
              </div>
            )}
          </SwiperWithButton>
        </div>
      )}
      <div className={styles.feedback__buttons}>
        {/* //TODO: заменить на кнопку-ссылку, как появится */}
        {feedbacks.length !== 0 && (
          <ButtonGeneral
            round
            className={classNames(styles['feedback__buttons-item'])}
            font="s"
            full="filled"
            grade="neutral"
            height="40"
          >
            Все отзывы
          </ButtonGeneral>
        )}
        <ButtonGeneral
          round
          className={classNames(styles['feedback__buttons-item'])}
          font="s"
          full="filled"
          grade="secondary"
          height="40"
          onClick={onFeedbackLeave}
        >
          Оставить отзыв
        </ButtonGeneral>
      </div>
    </div>
  )
}
