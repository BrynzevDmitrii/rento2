import { useState } from 'react'
import { useForm } from 'react-hook-form'

import { ModalBase } from '@shared/ui'
import { useControlModal } from '@shared/lib'

import { IFeedbackSingle, TFeedbackForm } from '../lib/types'
import { FeedbackModal } from './feedback-modal/FeedbackModal'
import { FeedbackSlider } from './feedback-slider/FeedbackSlider'
import { initRegistry, registry } from '../model/registry'

interface IFeedback {
  feedbacks: IFeedbackSingle[]
}

const defaultValues: TFeedbackForm = {
  user: {
    name: '',
    profilePhoto: '',
  },
  rating: {
    cleanliness: 0,
    location: 0,
    priceQuality: 0,
    renovation: 0,
  },
  // TODO: передавать id квартиры
  id: 0,
  feedback: '',
}

export const Feedback = ({ feedbacks }: IFeedback): JSX.Element => {
  const { Component, handleClick, nameOpenModal, resetModal } = useControlModal<
    keyof typeof initRegistry
  >(registry, 'FeedbackLeaveOne')
  const [singleFeedback, setSingleFeedback] = useState<IFeedbackSingle | null>(
    null
  )
  const [isSingleFeedbackShown, setIsSingleFeedbackShown] = useState(false)

  const [isFeedbackLeaveOpen, setIsFeedbackLeaveOpen] = useState(false)
  const { handleSubmit, control, getValues, reset } = useForm<TFeedbackForm>({
    defaultValues,
  })

  const onSubmit = handleSubmit((data) => console.log(data))

  return (
    <>
      <FeedbackSlider
        feedbacks={feedbacks}
        onFeedbackLeave={() => setIsFeedbackLeaveOpen(true)}
        onSingleOpen={(feedback: IFeedbackSingle) => {
          setSingleFeedback(feedback)
          setIsSingleFeedbackShown(true)
        }}
      />
      <ModalBase
        isOpen={isSingleFeedbackShown}
        isSwipe={true}
        onClose={() => setIsSingleFeedbackShown(false)}
      >
        <FeedbackModal feedback={singleFeedback} />
      </ModalBase>

      <ModalBase
        isOpen={isFeedbackLeaveOpen}
        isSwipe={nameOpenModal !== 'FeedbackInfo'}
        onClose={() => {
          setIsFeedbackLeaveOpen(false)
        }}
        onExited={() => {
          if (nameOpenModal === 'FeedbackInfo') {
            resetModal()
            reset()
          }
        }}
      >
        <Component
          control={control}
          getValues={getValues}
          handleClick={handleClick}
          onSubmit={onSubmit}
        />
      </ModalBase>
    </>
  )
}
