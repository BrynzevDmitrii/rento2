import classNames from 'classnames'
import { Control, Controller } from 'react-hook-form'

import {
  ButtonGeneral,
  ButtonIcon,
  IconArrowLeft,
  TextareaBase,
} from '@shared/ui'

import { TFeedbackForm, TModal } from '@widgets/feedback/lib/types'

import { ProgressBar } from '../progress-bar/ProgressBar'
import styles from './FeedbackLeaveThree.module.scss'

interface IFeedbackLeaveThree {
  handleClick: (nameModal: TModal) => void
  control: Control<TFeedbackForm>
  onSubmit: () => void
}

export const FeedbackLeaveThree = ({
  handleClick,
  control,
  onSubmit,
}: IFeedbackLeaveThree): JSX.Element => {
  return (
    <Controller
      control={control}
      name="feedback"
      render={({ field: { value, onChange } }) => (
        <div className={styles.feedback}>
          <div className={styles.feedback__head}>
            <div className={styles['feedback__head-title']}>
              <ButtonIcon
                appearance="arrowNavigation"
                full="stroke"
                grade="neutral"
                size="24"
                onClick={() => handleClick('FeedbackLeaveTwo')}
              >
                <IconArrowLeft />
              </ButtonIcon>
              Комментарий
            </div>
            <ProgressBar stage="3/3" />
          </div>
          <div className={styles.feedback__comment}>
            <p className={styles['feedback__comment-title']}>
              Поделитесь вашим опытом аренды. Это поможет нам стать лучше!
            </p>
            <TextareaBase
              className={classNames(styles['feedback__comment-textarea'], {
                [styles['textarea-full']]: value !== '',
              })}
              placeholder="Ваш отзыв"
              value={value}
              onChange={(e) => onChange(e.target.value)}
            />
          </div>
          <div className={styles.feedback__button}>
            <ButtonGeneral
              round
              className={classNames(styles['feedback__button-item'])}
              disabled={value === ''}
              font="s"
              full="filled"
              grade="secondary"
              height="40"
              onClick={() => {
                onSubmit()
                handleClick('FeedbackInfo')
              }}
            >
              Отправить
            </ButtonGeneral>
          </div>
        </div>
      )}
    />
  )
}
