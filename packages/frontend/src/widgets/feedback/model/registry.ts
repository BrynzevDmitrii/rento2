import { ElementType } from 'react'

import { Registry } from '@entities/Registry'

import { FeedbackLeaveOne } from '../ui/feedback-leave-1/FeedbackLeaveOne'
import { FeedbackLeaveTwo } from '../ui/feedback-leave-2/FeedbackLeaveTwo'
import { FeedbackLeaveThree } from '../ui/feedback-leave-3/FeedbackLeaveThree'
import { FeedbackInfo } from '../ui/feedback-info/FeedbackInfo'
import { PhotoUploader } from '../ui/photo-uploader/PhotoUploader'

const initRegistry = {
  FeedbackLeaveOne,
  FeedbackLeaveTwo,
  PhotoUploader,
  FeedbackLeaveThree,
  FeedbackInfo,
}

const registry = new Registry<ElementType>(initRegistry)

export { initRegistry, registry }
