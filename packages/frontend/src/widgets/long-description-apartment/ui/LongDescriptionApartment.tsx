import { DetailedHTMLProps, FC, HTMLAttributes } from 'react'
import classNames from 'classnames'

import { ButtonGeneral, IconArrowDown, IconArrowUp } from '@shared/ui'
import { useMoreText } from '@shared/lib'

import styles from './LongDescriptionApartment.module.scss'

interface IDescriptionText
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  text: string
}

export const LongDescriptionApartment: FC<IDescriptionText> = ({
  text,
  className,
  ...props
}) => {
  const { refText, isVisibleText, isVisibleButton, setIsVisibleText } =
    useMoreText(4)

  return (
    <div className={classNames(styles.description, className)} {...props}>
      <p className={styles.description__heading}>Описание</p>
      <p
        ref={refText}
        className={classNames(styles.description__text, {
          [styles.description__text_visible]: isVisibleText,
        })}
      >
        {text}
      </p>
      <div className={styles['description__img']}>
        <div className={styles['description__circle']} />
        <div className={styles.description__key} />
      </div>
      {isVisibleButton && (
        <ButtonGeneral
          className={styles.description__btn}
          font="xs"
          full="text"
          grade="neutral"
          height="40"
          onClick={() => setIsVisibleText(!isVisibleText)}
        >
          {isVisibleText ? (
            <>
              Подробнее
              <IconArrowDown className={styles['description__btn-icon']} />
            </>
          ) : (
            <>
              Свернуть
              <IconArrowUp className={styles['description__btn-icon']} />
            </>
          )}
        </ButtonGeneral>
      )}
    </div>
  )
}
