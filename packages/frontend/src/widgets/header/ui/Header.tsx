import { useState, useRef } from 'react'
import Image from 'next/image'
import classNames from 'classnames'

import { ButtonGeneral, ButtonIcon, LinkGeneral, IconMenu } from '@shared/ui'
import { useOnClickOutside } from '@shared/lib/hooks/useClickOutside'

import { MobileMenu } from './mobile-menu/MobileMenu'
import { dataNavLinks } from '../model/dataHeader'

import styles from './Header.module.scss'

interface IProps {
  showHeader?: boolean
}

export const Header = ({ showHeader = false }: IProps): JSX.Element => {
  const [showMenu, setShowMenu] = useState(false)
  const dropdown = useRef<HTMLDivElement | null>(null)

  useOnClickOutside(dropdown, () => setShowMenu(false))

  return (
    <header
      className={classNames(styles['header'], {
        [styles.sticky]: showHeader,
      })}
    >
      <div
        className={classNames(
          styles['header__inner'],
          'container',
          'flex-s-b-c'
        )}
      >
        <LinkGeneral classProps={styles['header__logo']} href="/">
          <Image alt="Logo" height={14} src="/images/logo.svg" width={60} />
        </LinkGeneral>
        <nav>
          <ul className={classNames(styles['header__list'], 'flex-center')}>
            {dataNavLinks.map(({ hrefProps, textProps }) => (
              <li key={textProps} className={styles['header__item']}>
                <LinkGeneral
                  classProps={styles['header__link']}
                  href={hrefProps}
                >
                  {textProps}
                </LinkGeneral>
              </li>
            ))}
          </ul>
        </nav>
        {/* TODO когда будет авторизация */}
        {/* <ul className={ classNames(styles['header__auth-list'], 'flex-center') }>
          <li className={ styles['header__auth-item'] }>
            <LinkGeneral href='#'>
              <IconHeart />
            </LinkGeneral>
          </li>
          <li className={ styles['header__auth-item'] }>
            <LinkGeneral classProps='flex-center'
              href='#'
            >
              <IconUserSquare />
              <span>
                Войти
              </span>
            </LinkGeneral>
          </li>
        </ul> */}
        <ButtonGeneral
          className={styles['header__phone-btn']}
          font="s"
          full="filled"
          grade="neutral"
          height="40"
          href="tel:+74993213185"
        >
          +7 499 321 31 85
        </ButtonGeneral>
        {showMenu ? (
          <MobileMenu ref={dropdown} onClose={() => setShowMenu(false)} />
        ) : (
          <ButtonIcon
            appearance="buttonAction"
            className={styles['header__btn-open']}
            grade="iris"
            size="32"
            onClick={() => setShowMenu(true)}
          >
            <IconMenu />
          </ButtonIcon>
        )}
      </div>
    </header>
  )
}
