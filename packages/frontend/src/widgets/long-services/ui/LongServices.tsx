import classNames from 'classnames'
import { DetailedHTMLProps, HTMLAttributes } from 'react'

import styles from './LongServices.module.scss'

interface IPropsServices
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}

export const LongServices = ({
  className,
  ...props
}: IPropsServices): JSX.Element => {
  return <div className={classNames(styles.wrapper, className)} {...props} />
}
