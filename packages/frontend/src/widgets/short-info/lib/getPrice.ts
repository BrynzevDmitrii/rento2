import { pluralize } from '@shared/lib/helpers/stringUtils'

// TODO: после создания календаря поменять тип для даты
export const getPriceWithDates = (sum: number, dates: any): string =>
  dates === null
    ? `от ${sum}₽ / ${pluralize('сутки', 1)}`
    : `${sum * dates}₽ / ${dates as number} ${pluralize('сутки', dates)}`

export const getPriceDeposit = (sum: number): string => `Залог: ${sum}₽`
