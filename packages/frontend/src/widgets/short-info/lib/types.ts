import { DetailedHTMLProps, HTMLAttributes } from 'react'

import { IMetroStation } from '@shared/api'

import { IGuests } from '@features/select-guests'

interface IShortInfo
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  price: IPrice
  address: IAddress
  onClickMap: () => void
  onClickShareLink: () => void
}

interface IPrice {
  price: number
  deposit: number
}

interface IAddress {
  street: string
  metro: IMetroStation[]
  timeFoot: number
  timeVehicle: number
}

interface IShortInfoForm {
  // TODO: после создания календаря поменять на нормальный тип
  dates: any
  guests: IGuests
}

export type { IShortInfo, IShortInfoForm, IAddress }
