import { Controller, useForm, useWatch } from 'react-hook-form'
import classNames from 'classnames'

import {
  IconLocation,
  ButtonIcon,
  IconHeart,
  IconLink,
  Tooltip,
  IconInfo,
} from '@shared/ui/index'
import { ButtonGeneral } from '@shared/ui'
import { useWindowDimensions } from '@shared/lib'

import { SelectDateButton } from '@features/select-date-button'
import { SelectGuests } from '@features/select-guests'

import { IShortInfo, IShortInfoForm } from '../lib/types'
import ShortInfoAddress from './address/ShortInfoAddress'
import { getPriceDeposit, getPriceWithDates } from '../lib/getPrice'

import styles from './ShortInfo.module.scss'

const defaultValues: IShortInfoForm = {
  dates: null,
  guests: {
    totalAmount: 1,
    adults: 1,
    kidsAmount: 0,
    babiesKids: 0,
    grownKids: 0,
  },
}

export function ShortInfo({
  price,
  address,
  onClickMap,
  onClickShareLink,
  className,
  ...props
}: IShortInfo): JSX.Element {
  const { control, reset, getValues } = useForm<IShortInfoForm>({
    defaultValues,
  })

  // TODO: Скорее всего даты не отсюда должны приходить, т.к. они тянутся еще с каталога (скорее всего из стора нужно будет забирать)
  const dates = useWatch({ control, name: 'dates' })

  const { widthWindow } = useWindowDimensions()

  return (
    <div className={classNames(styles.info, className)} {...props}>
      <div className={styles.toolbar}>
        <div className={styles.toolbar__left}>
          <ButtonIcon
            appearance="buttonIconMain"
            full="stroke"
            grade="neutral"
            href="#mapRef"
            size="40"
            onClick={onClickMap}
          >
            <IconLocation
              className={classNames(styles['toolbar__left-icon'])}
            />
          </ButtonIcon>
        </div>
        <div className={styles.toolbar__right}>
          <ButtonIcon
            appearance="buttonAction"
            full="stroke"
            grade="iris"
            size="32"
          >
            <IconHeart />
          </ButtonIcon>
          <ButtonIcon
            appearance="buttonAction"
            full="stroke"
            grade="iris"
            size="32"
            onClick={onClickShareLink}
          >
            <IconLink />
          </ButtonIcon>
        </div>
      </div>

      <ShortInfoAddress address={address} />

      <div className={styles.params}>
        <Controller
          control={control}
          name="dates"
          render={({ field: { value, onChange } }) => (
            <SelectDateButton fill="gray" value={value} onChange={onChange} />
          )}
        />

        <Controller
          control={control}
          name="guests"
          render={({ field: { value, onChange } }) => (
            <SelectGuests
              defaultGuests={defaultValues.guests}
              value={value}
              onChangeGuests={onChange}
              onReset={() =>
                reset({
                  ...getValues(),
                  guests: {
                    totalAmount: 1,
                    adults: 1,
                    kidsAmount: 0,
                    babiesKids: 0,
                    grownKids: 0,
                  },
                })
              }
            />
          )}
        />
      </div>

      <div className={styles.price}>
        <Tooltip
          label="Обычно, в выходные и праздники цена выше. Выбрав даты, вы увидите итоговую стоимость."
          position="bottom-left"
          size="xs"
        >
          <div className={styles.price__full}>
            <span className={styles['price__full-amount']}>
              {' '}
              {getPriceWithDates(price.price, dates)}
            </span>
            <IconInfo className={classNames(styles['price__full-icon'])} />
          </div>
        </Tooltip>
        <span className={styles.price__deposit}>
          {getPriceDeposit(price.deposit)}
        </span>
      </div>

      <ButtonGeneral
        className={styles.btn}
        font={widthWindow > 1023 ? 'm' : 's'}
        full="filled"
        height={widthWindow > 1023 ? '44' : '40'}
        onClick={() => {}}
      >
        Забронировать
      </ButtonGeneral>
    </div>
  )
}
