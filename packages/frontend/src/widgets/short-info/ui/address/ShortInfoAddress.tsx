import classNames from 'classnames'

import { MetroInfo } from '@entities/MetroInfo'

import { IAddress } from '../../lib/types'
import styles from './ShortInfoAddress.module.scss'

// TODO: Проверить типы (address) при подключении компонента на страницу. Стили тоже проверить (удалила одну обертку)
interface IShortInfoAddress {
  address: IAddress
}

const ShortInfoAddress = ({ address }: IShortInfoAddress): JSX.Element => (
  <div className={styles.address}>
    <div className={styles.address__street}>{address.street}</div>
    {address.metro != null && address.metro.length !== 0 && (
      <MetroInfo
        classDistance={classNames(styles.metro__distance)}
        classSubway={classNames(styles.metro__station)}
        classWrapper={classNames(styles.metro)}
        metroStations={address.metro}
        timeFoot={address.timeFoot}
        timeVehicle={address.timeVehicle}
      />
    )}
  </div>
)

export default ShortInfoAddress
