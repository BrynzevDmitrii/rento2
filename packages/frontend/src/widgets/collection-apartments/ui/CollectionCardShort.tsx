import classNames from 'classnames'
import Image from 'next/image'

import { IApartmentItem } from '@shared/api'
import { roundInteger } from '@shared/lib'
import { DetailsShort, PriceShort } from '@shared/ui'

import { ApartmentCard } from '@entities/cards/ApartmentCard'
import { MetroInfo } from '@entities/MetroInfo'

import styles from './CollectionCardShort.module.scss'

// TODO: Изменить путь к картинке и alt, когда появится функционал на бэке
export const CollectionCardShort = ({
  apartmentData,
  path,
}: {
  apartmentData: IApartmentItem
  path: string
}): JSX.Element => {
  const area = roundInteger(Number(apartmentData.area))

  return (
    <ApartmentCard
      {...apartmentData}
      isRating
      detailsInfo={DetailsShort({
        roomsNum: apartmentData.roomsNum,
        area,
        storey: apartmentData.storey,
      })}
      media={
        <div className={styles.card__image}>
          <Image
            alt="room1"
            layout="fill"
            objectFit="cover"
            src="/images/long/room1.jpg"
          />
        </div>
      }
      metroInfo={
        <MetroInfo
          classSubway={classNames(styles.card__subway)}
          classWrapper={classNames(styles['card__metro-wrapper'])}
          metroStations={apartmentData.metroStations}
          timeFoot={apartmentData.metroAvailabilityByFoot}
          timeVehicle={apartmentData.metroAvailabilityByVehicle}
        />
      }
      pathPage={path}
      priceInfo={PriceShort(apartmentData.price)}
      typeCard="collection"
    />
  )
}
