import { useRef } from 'react'
import { Swiper, SwiperProps, SwiperSlide } from 'swiper/react'
import SwiperCore from 'swiper'
import classNames from 'classnames'

import {
  IconArrowLeft,
  IconArrowRight,
  ButtonIcon,
  TButtonIcon,
} from '@shared/ui'
import { PartialBy } from '@shared/lib'

import styles from './SwiperWithButton.module.scss'

type TButtonProps = PartialBy<
  Omit<TButtonIcon<'arrowNavigation'>, 'appearance' | 'refProps'>,
  'grade'
>

interface ISwiperWithButtonProps<T> {
  elementData: T[]
  children: (element: T) => JSX.Element
  initSwiperParams?: SwiperProps
  classSlideWrapper?: string
  classBtnNext?: string
  classBtnPrev?: string
  classIcon?: string
  btnProps?: TButtonProps
}

export const SwiperWithButton = <T,>({
  elementData,
  children,
  initSwiperParams,
  classSlideWrapper,
  classBtnNext,
  classBtnPrev,
  classIcon,
  btnProps,
}: ISwiperWithButtonProps<T>): JSX.Element => {
  const prevRef = useRef<HTMLButtonElement>(null)
  const nextRef = useRef<HTMLButtonElement>(null)

  const initParams = {
    onBeforeInit,
    ...initSwiperParams,
  }

  function onBeforeInit(Swiper: SwiperCore): void {
    const navigation = Swiper.params.navigation
    if (typeof navigation !== 'boolean' && typeof navigation !== 'undefined') {
      navigation.prevEl = prevRef.current
      navigation.nextEl = nextRef.current
    }
  }
  return (
    <>
      <ButtonIcon
        appearance="arrowNavigation"
        className={classNames(styles['button-prev'], classBtnPrev)}
        full="stroke"
        grade="iris"
        refProps={prevRef}
        size="40"
        {...btnProps}
      >
        <IconArrowLeft className={classNames(classIcon)} />
      </ButtonIcon>

      <ButtonIcon
        appearance="arrowNavigation"
        className={classNames(styles['button-next'], classBtnNext)}
        full="stroke"
        grade="iris"
        refProps={nextRef}
        size="40"
        {...btnProps}
      >
        <IconArrowRight className={classNames(classIcon)} />
      </ButtonIcon>

      <Swiper {...initParams}>
        {elementData.map((item, idx) => (
          <SwiperSlide key={idx} className={classNames(classSlideWrapper)}>
            {children(item)}
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  )
}
