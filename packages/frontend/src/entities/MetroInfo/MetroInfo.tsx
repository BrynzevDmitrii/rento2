import classNames from 'classnames'

import { IconWalker, IconMetro, IconBus } from '@shared/ui'
import { stringUtils } from '@shared/lib'
import { IMetroStation } from '@shared/api'

import styles from './MetroInfo.module.scss'

interface IMetroInfo {
  metroStations: IMetroStation[]
  timeFoot: number
  timeVehicle: number | null
  classSubway?: string
  classDistance?: string
  classWrapper?: string
  tagName?: React.ElementType
}

export const MetroInfo = ({
  metroStations,
  timeFoot,
  timeVehicle,
  classSubway,
  classDistance,
  tagName,
  classWrapper,
}: IMetroInfo): JSX.Element => {
  const Component = tagName ?? 'div'

  const getMetroStations = (): JSX.Element => {
    return (
      <>
        {metroStations?.map((metro) => (
          <span
            key={metro.id}
            className={classNames(styles['metro-info__subway'], classSubway)}
          >
            <IconMetro
              className={styles['metro-info__icon']}
              style={{ fill: stringUtils.getColorMetro(metro.metroLine.color) }}
            />
            {metro.name}
          </span>
        ))}
      </>
    )
  }

  return (
    <Component className={classWrapper}>
      {getMetroStations()}
      <span
        className={classNames(styles['metro-info__distance'], classDistance)}
      >
        <IconWalker className={styles['metro-info__icon']} />
        {`${timeFoot} ${stringUtils.pluralize('минута', timeFoot)}`}
      </span>
      {timeVehicle != null && (
        <span
          className={classNames(styles['metro-info__distance'], classDistance)}
        >
          <IconBus className={styles['metro-info__icon']} />
          {`${timeVehicle} ${stringUtils.pluralize('минута', timeVehicle)}`}
        </span>
      )}
    </Component>
  )
}
