import classNames from 'classnames'

import styles from './AdressInfo.module.scss'

interface IAdressInfo {
  adress: string
  classAdress?: string
  tagName?: React.ElementType
}

export const AdressInfo = ({
  adress,
  classAdress,
  tagName,
}: IAdressInfo): JSX.Element => {
  const Component = tagName ?? 'div'

  return (
    <Component className={classNames(styles['adress-info'], classAdress)}>
      {adress}
    </Component>
  )
}
