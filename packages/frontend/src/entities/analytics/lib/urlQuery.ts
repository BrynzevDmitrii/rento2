import { parse } from 'query-string'

import { NAME_TRAFFIC_SOURCE } from '@shared/config'

const parseQuery = (query: string): string | undefined => {
  const parseQuery = parse(query)
  const value = parseQuery[NAME_TRAFFIC_SOURCE]
  if (typeof value !== 'string') return

  return value
}

export { parseQuery }
