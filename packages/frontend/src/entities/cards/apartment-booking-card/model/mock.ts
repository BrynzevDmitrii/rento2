export const aptInfoTest = {
  adress: '2й Спассоналивковский пер. 16/2',
  guests: 2,
  sofas: 1,
  beds: 2,
  rooms: 2,
  image: '/long-one/GallerySlider1.jpg',
}
export const bookingInfoTest = {
  daysNumber: 20,
  dateFrom: '29.12.22',
  dateTo: '30.12.22',
}
