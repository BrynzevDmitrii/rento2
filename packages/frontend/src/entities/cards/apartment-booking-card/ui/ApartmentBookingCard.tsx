import { FC } from 'react'
import Image from 'next/image'

import { ButtonGeneral } from '@shared/ui'
import { stringUtils } from '@shared/lib'

import stylesBooking from './ApartmentBookingCard.module.scss'
import stylesSuccessfulBooking from './SuccessfulApartmentCard.module.scss'

interface IApartmentBookingCardProps {
  typeCard: 'booking' | 'success'
  apartmentInfo: IApartmentInfo
  bookingInfo: IBookingInfo
}

interface IApartmentInfo {
  adress: string
  guests: number
  sofas: number
  beds: number
  rooms: number
  image: string
}

interface IBookingInfo {
  daysNumber: number
  dateFrom: string
  dateTo: string
}

export const ApartmentBookingCard: FC<IApartmentBookingCardProps> = ({
  typeCard,
  bookingInfo,
  apartmentInfo,
}) => {
  const stylesObject = {
    booking: stylesBooking,
    success: stylesSuccessfulBooking,
  }

  const styles = stylesObject[typeCard]

  const bookingDetails = (): JSX.Element => {
    return (
      <div className={styles.header}>
        <div className={styles.dates}>
          <div>
            {`${bookingInfo.daysNumber} ${stringUtils.pluralize(
              'сутки',
              bookingInfo.daysNumber
            )}`}
          </div>
          <div>{`${bookingInfo.dateFrom} - ${bookingInfo.dateTo}`}</div>
        </div>
        <div className={styles.button__container}>
          <ButtonGeneral
            className={styles.button}
            font="xs"
            full="text"
            grade="accent"
            height="32"
          >
            Изменить даты
          </ButtonGeneral>
        </div>
      </div>
    )
  }
  const apartmentPrewiew = (): JSX.Element => {
    return (
      <div className={styles['apartment-prewiew']}>
        <div className={styles['image-container']}>
          <div>
            <Image layout="fill" objectFit="cover" src={apartmentInfo.image} />
          </div>
        </div>
        <div className={styles['info-container']}>
          <div className={styles.info}>{apartmentInfo.adress}</div>
          <div className={styles.info}>{`Гостей: ${apartmentInfo.guests}`}</div>
          <div className={styles.info}>
            {`${apartmentInfo.sofas} ${stringUtils.pluralize(
              'диван',
              apartmentInfo.sofas
            )} • ${apartmentInfo.beds} ${stringUtils.pluralize(
              'кровать',
              apartmentInfo.beds
            )}`}
          </div>
          <div className={styles.info}>{`Комнат: ${apartmentInfo.rooms}`}</div>
        </div>
      </div>
    )
  }

  return (
    <div className={styles.card}>
      {bookingDetails()}
      {apartmentPrewiew()}
    </div>
  )
}
