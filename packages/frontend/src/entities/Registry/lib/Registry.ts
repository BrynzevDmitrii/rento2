interface IComponentRegistry<T> {
  [key: string]: T
}

class Registry<T> {
  private readonly registry: IComponentRegistry<T>

  constructor(initRegistry: IComponentRegistry<T>) {
    this.registry = initRegistry
  }

  public getComponent(id: string): T {
    const component = this.registry[id]

    if (component === undefined) {
      throw Error(`Component ${id} not found in the registry`)
    }

    return component
  }

  public addComponent(id: string, component: T): void {
    this.registry[id] = component
  }
}

export { Registry }
