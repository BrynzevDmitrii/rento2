import { useMemo } from 'react'
import classNames from 'classnames'
import { Controller, useForm } from 'react-hook-form'

import {
  ButtonGeneral,
  CheckBox,
  InputBase,
  LinkGeneral,
  SwitchBase,
} from '@shared/ui'
import { useWindowDimensions, onPromise, maskPhoneNumber } from '@shared/lib'

import styles from './CallbackForm.module.scss'

interface ICallbackForm {
  onSubmit: () => void
  classForm?: string
  isStatic?: boolean
}

const defaultValues = {
  isTelegram: false,
  number: '',
  telegram: '',
  isAgree: true,
}

export function CallbackForm({
  onSubmit,
  classForm,
  isStatic = false,
}: ICallbackForm): JSX.Element {
  const { widthWindow } = useWindowDimensions()

  const title = useMemo(
    () =>
      widthWindow <= 479 || isStatic
        ? 'Найти квартиру быстрее?'
        : 'Оставьте заявку, и мы начнём поиск уже сегодня',
    [isStatic, widthWindow]
  )
  const subTitle = useMemo(
    () =>
      widthWindow <= 479 || isStatic
        ? 'Закажите Персональный поиск от Ренто'
        : '',
    [isStatic, widthWindow]
  )

  const { getValues, reset, handleSubmit, control, watch } = useForm({
    mode: 'onChange',
    defaultValues,
  })
  const isTelegram = watch('isTelegram')

  const onFormSubmit = handleSubmit((data) => {
    console.log(data)
    onSubmit()
    reset()
  })

  return (
    <form
      className={classNames(styles.form, classForm)}
      onSubmit={onPromise(onFormSubmit)}
    >
      <div
        className={classNames(styles.form__info, { [styles.static]: isStatic })}
      >
        <p className={styles['form__info-title']}>{title}</p>
        {subTitle.length !== 0 && (
          <p
            className={classNames(styles['form__info-subtitle'], {
              [styles.static]: isStatic,
            })}
          >
            {subTitle}
          </p>
        )}
      </div>
      <Controller
        control={control}
        name="isTelegram"
        render={({ field: { onChange, name } }) => (
          <SwitchBase
            containerClass={classNames(styles.form__switch, {
              [styles.static]: isStatic,
            })}
            labelText="Использовать для связи Telegram"
            name={name}
            onChange={(e) => {
              onChange(e.target.checked)
              reset({ ...getValues(), telegram: '', number: '' })
            }}
          />
        )}
      />

      {isTelegram ? (
        <Controller
          control={control}
          name="telegram"
          render={({ field: { onChange, value } }) => (
            <div>
              <InputBase
                required
                autoComplete="off"
                id="telegram"
                label="Никнейм в Telegram*"
                placeholder="@nickname"
                value={value}
                onChange={onChange}
              />
            </div>
          )}
          rules={{ required: true }}
        />
      ) : (
        <Controller
          control={control}
          name="number"
          render={({ field: { onChange, value } }) => (
            <div>
              <InputBase
                required
                autoComplete="off"
                id="number"
                label="Телефон для связи*"
                maxLength={16}
                placeholder="+7 (___)-___-____"
                type="tel"
                value={value}
                onChange={(e) => onChange(maskPhoneNumber(e.target.value))}
              />
            </div>
          )}
          rules={{ required: true }}
        />
      )}

      <Controller
        control={control}
        name="isAgree"
        render={({ field: { onChange, value } }) => (
          <CheckBox
            classLabel={classNames(styles['form__checkbox-label'])}
            classWrapper={classNames(styles.form__checkbox, {
              [styles.static]: isStatic,
            })}
            // TODO: поменять после того, как пропишем валидацию
            isValid={true}
            label={
              <span>
                Я согласен на{' '}
                <LinkGeneral
                  classProps={styles['form__checkbox-link']}
                  href="/docs/privacy"
                  target="_blank"
                >
                  Обработку персональных данных
                </LinkGeneral>
              </span>
            }
            value={value}
            onChange={onChange}
          />
        )}
        rules={{ required: true }}
      />

      <ButtonGeneral
        round
        className={styles.form__button}
        font="m"
        full="filled"
        grade="primary"
        height={widthWindow <= 479 ? '44' : '48'}
        type="submit"
      >
        Отправить заявку
      </ButtonGeneral>
    </form>
  )
}
