import { useEffect, useRef, useState } from 'react'
import classNames from 'classnames'

import { ButtonIcon } from '@shared/ui/buttons/ButtonIcon'
import { IconAdd, IconMinus } from '@shared/ui/icons'
import { ButtonGeneral } from '@shared/ui'

import { ISelectGuests, Guests } from '../lib/types'
import { getAdults, getKids } from '../lib/getTotalGuests'
import { SelectGuestsButton } from './SelectGuestsButton'
import styles from './SelectGuests.module.scss'

export const SelectGuests = ({
  value,
  defaultGuests,
  onChangeGuests,
  onReset,
  className,
  ...props
}: ISelectGuests): JSX.Element => {
  const [isActive, setIsActive] = useState(false)
  const selectorRef = useRef<HTMLDivElement>(null)

  const onIncrement = (guest: keyof typeof Guests): void =>
    onChangeGuests({
      ...value,
      [guest]: ++value[guest],
      kidsAmount: guest !== 'adults' ? ++value.kidsAmount : value.kidsAmount,
      totalAmount: ++value.totalAmount,
    })

  const onDecrement = (guest: keyof typeof Guests): void =>
    onChangeGuests({
      ...value,
      [guest]: --value[guest],
      kidsAmount: guest !== 'adults' ? --value.kidsAmount : value.kidsAmount,
      totalAmount: --value.totalAmount,
    })

  const renderGuestsItem = (
    el: string,
    key: Guests,
    idx: number
  ): JSX.Element => {
    const guest: keyof typeof Guests = el as keyof typeof Guests
    return (
      <li key={idx} className={styles.list__item}>
        {key}
        <div className={styles['list__item-control']}>
          <ButtonIcon
            appearance="buttonCount"
            disabled={value[guest] === defaultGuests[guest]}
            full="stroke"
            grade="iris"
            size="24"
            type="button"
            onClick={() => onDecrement(guest)}
          >
            <IconMinus />
          </ButtonIcon>
          <span
            className={classNames(styles.counter, {
              [styles['counter-disabled']]: value[guest] === 0,
            })}
          >
            {value[guest]}
          </span>
          <ButtonIcon
            appearance="buttonCount"
            full="stroke"
            grade="iris"
            size="24"
            type="button"
            onClick={() => onIncrement(guest)}
          >
            <IconAdd />
          </ButtonIcon>
        </div>
      </li>
    )
  }

  useEffect(() => {
    const onSelectorClose = (e: MouseEvent): void => {
      const node = e.target as Node
      if (!(selectorRef.current?.contains(node) ?? false)) {
        setIsActive(false)
      }
    }
    document.addEventListener('click', onSelectorClose)

    return () => document.removeEventListener('click', onSelectorClose)
  }, [])

  return (
    <div
      ref={selectorRef}
      className={classNames(styles.selector, className)}
      {...props}
    >
      <div className={styles.selector__header}>
        <SelectGuestsButton
          fill={true}
          isActive={isActive}
          type="button"
          onClick={() => setIsActive(!isActive)}
        >
          <span>{getAdults(value)}</span>
          <span
            className={classNames(styles.kids, {
              [styles['kids-default']]: value.kidsAmount === 0,
            })}
          >
            {getKids(value)}
          </span>
        </SelectGuestsButton>
      </div>
      {isActive && (
        <div className={styles.selector__body}>
          <ul className={styles.list}>
            {Object.entries(Guests).map(([el, key], idx) =>
              renderGuestsItem(el, key, idx)
            )}
          </ul>
          <ButtonGeneral
            font="s"
            full="text"
            grade="accent"
            height="40"
            type="button"
            onClick={onReset}
          >
            Сбросить
          </ButtonGeneral>
        </div>
      )}
    </div>
  )
}
