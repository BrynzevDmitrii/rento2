import { ButtonHTMLAttributes, DetailedHTMLProps, ReactNode } from 'react'
import classNames from 'classnames'

import styles from './SelectGuestsButton.module.scss'

interface ISelectGuestsButton
  extends DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  fill: boolean
  children: ReactNode
  isActive: boolean
}

export const SelectGuestsButton = ({
  fill = false,
  children,
  isActive,
  ...props
}: ISelectGuestsButton): JSX.Element => (
  <button
    className={classNames(styles.button, {
      [styles['button-filled']]: fill,
      [styles['button-active']]: isActive,
    })}
    {...props}
  >
    {children}
  </button>
)
