import { stringUtils } from '@shared/lib'

import { IGuests } from './types'

export const getAdults = (obj: IGuests): string =>
  `${obj.adults} ${stringUtils.pluralize('взрослый', obj.adults)}`

export const getKids = (obj: IGuests): string =>
  obj.kidsAmount === 0
    ? 'без детей'
    : `${obj.kidsAmount} ${stringUtils.pluralize('ребёнок', obj.kidsAmount)}`
