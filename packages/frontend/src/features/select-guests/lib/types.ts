import { DetailedHTMLProps, HTMLAttributes } from 'react'

export interface IGuests {
  totalAmount: number
  adults: number // ofAdults
  kidsAmount: number
  babiesKids: number // ofInfants
  grownKids: number // ofChilds
}

export interface ISelectGuests
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  value: IGuests
  defaultGuests: IGuests
  onChangeGuests: (value: IGuests) => void
  onReset: () => void
}

export enum Guests {
  adults = 'Взрослые',
  babiesKids = 'Дети 0-2',
  grownKids = 'Дети 3-17',
}
