// TODO: реализовать типы отправки данных на бэк, на основе этих данных (если нужно)

const additionallyFirst = [
  {
    key: 'foreigners',
    title: 'Иностранцы',
  },
  {
    key: 'family',
    title: 'Семья',
  },
  {
    key: 'moreThanFourPeople',
    title: 'Более 4х человек',
  },
  {
    key: 'students',
    title: 'Студенты',
  },
]

const additionallySecond = [
  {
    key: 'areChildrenAllowed',
    title: 'Есть дети',
  },
  {
    key: 'areDogAllowed',
    title: 'Есть собака',
  },
  {
    key: 'areCatAllowed',
    title: 'Есть котик',
  },
  {
    key: 'PaymentCompany',
    title: 'Оплата от юр.лица',
  },
]

const additionallyThird = [
  {
    key: 'short',
    title: 'Хочу снять квартиру на пару месяцев',
  },
  {
    key: 'onlineViewing',
    title: 'Онлайн-просмотр',
  },
]

export { additionallyFirst, additionallySecond, additionallyThird }
