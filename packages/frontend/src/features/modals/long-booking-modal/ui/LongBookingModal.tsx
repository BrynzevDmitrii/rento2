import { useState, FC } from 'react'
import Image from 'next/image'
import { useForm, SubmitHandler, Controller } from 'react-hook-form'
import classNames from 'classnames'

import {
  ButtonGeneral,
  DetailsLong,
  ModalBase,
  ChipBox,
  PriceLong,
  LinkGeneral,
  SwitchBase,
  InputBase,
  TextareaBase,
} from '@shared/ui'
import { maskPhoneNumber } from '@shared/lib'
import { IMetroStation } from '@shared/api'

import { MetroInfo } from '@entities/MetroInfo'
import { AdressInfo } from '@entities/AdressInfo'

import {
  additionallyFirst,
  additionallySecond,
  additionallyThird,
} from '../model/dataModal'

import styles from './LongBookingModal.module.scss'

interface IApartment {
  pricePerMonth: string
  imageSrc: string
  roomNum: string
  area: number
  storey: number
  totalStoreys: number
  street: string
  metro: IMetroStation[]
  metroAvailabilityByFoot: number
  metroAvailabilityByVehicle: number
}

interface ILongBookingModalProps {
  apartmentInfo: IApartment
  onClose: () => void
  isOpen: boolean
}

interface IBookingForm {
  phoneNumber: string
  nickname: string
  comment: string
  location: string
  checkbox: boolean
  additionally: []
}

// TODO: убрать any когда будет бэк
const defaultValues: any = {
  additionally: [],
}

export const LongBookingModal: FC<ILongBookingModalProps> = ({
  apartmentInfo,
  isOpen,
  onClose,
}: ILongBookingModalProps) => {
  const { control, register, handleSubmit, reset, getValues, setValue } =
    useForm<IBookingForm>({
      defaultValues,
    })
  const [switchTg, setSwitchTg] = useState(false)

  const onSubmit: SubmitHandler<IBookingForm> = (data) => {
    console.log(data)
    reset()
  }

  const renderApartmentPrewiew = (): JSX.Element => {
    return (
      <div className={styles['apartment-prewiew']}>
        <div className={styles['apartment-prewiew__image']}>
          <Image
            alt="room3"
            layout="fill"
            objectFit="cover"
            src={apartmentInfo.imageSrc}
          />
        </div>
        <div className={styles['apartment-prewiew__info']}>
          <div>{PriceLong(apartmentInfo.pricePerMonth)}</div>
          <div className={styles.info__details}>
            {DetailsLong({
              roomsNum: apartmentInfo.roomNum,
              area: apartmentInfo.area,
              storey: apartmentInfo.storey,
              totalStoreys: apartmentInfo.totalStoreys,
            })}
          </div>
          <div className={styles.info__adress}>
            <AdressInfo adress={apartmentInfo.street} />
          </div>
          <MetroInfo
            classDistance={classNames(styles['info__metro--distanse'])}
            classSubway={classNames(styles['info__metro--subway'])}
            metroStations={apartmentInfo.metro}
            timeFoot={apartmentInfo.metroAvailabilityByFoot}
            timeVehicle={apartmentInfo.metroAvailabilityByVehicle}
          />
        </div>
      </div>
    )
  }

  const checkbox = (): JSX.Element => {
    return (
      <div className={styles.checkbox}>
        <input
          {...register('checkbox')}
          className={styles.checkbox__input}
          name="checkbox"
          type="checkbox"
        />
        <div className={styles.checkbox__text}>
          <span>Я согласен на</span>

          <LinkGeneral
            classProps={styles['link-general']}
            href="/docs/privacy"
            target="_blank"
          >
            Обработку персональных данных
          </LinkGeneral>
        </div>
      </div>
    )
  }

  const renderForm = (): JSX.Element => {
    return (
      <>
        <div className={styles['switch-tg']}>
          <SwitchBase
            labelText="Связаться в Telegram"
            labelTextPlacement="right"
            name="contactTg"
            onChange={(e) => {
              setSwitchTg(e.target.checked)
            }}
          />
        </div>

        <form
          onSubmit={() => {
            handleSubmit(onSubmit)
          }}
        >
          <div className={styles.inputs__field}>
            <div className={classNames(styles.columns)}>
              <div className={styles['first__input']}>
                {switchTg && (
                  <InputBase
                    label="Никнейм в Telegram*"
                    placeholder="@nickname"
                    {...register('nickname')}
                    full="filled"
                  />
                )}

                {!switchTg && (
                  <InputBase
                    label="Телефон для связи*"
                    placeholder="+7(___)-___-___"
                    {...register('phoneNumber', {
                      onChange: (e) =>
                        setValue(
                          'phoneNumber',
                          maskPhoneNumber(e.target.value)
                        ),
                    })}
                    full="filled"
                  />
                )}
              </div>

              <InputBase
                label="Расположение*"
                {...register('location')}
                full="filled"
                placeholder="Адрес, метро"
              />
            </div>
            <div className={classNames(styles.columns)}>
              <div className={classNames(styles.textarea__field)}>
                <TextareaBase
                  label="Комментарий"
                  {...register('comment')}
                  className={styles.textarea}
                  placeholder="Кто будет жить в квартире?"
                />
              </div>
            </div>
          </div>

          <h3 className={styles.chips__title}>Дополнительно</h3>

          <Controller
            control={control}
            name="additionally"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={additionallyFirst}
                classProps={classNames(styles['additionally__chip-box'])}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />
          <Controller
            control={control}
            name="additionally"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={additionallySecond}
                classProps={classNames(styles['additionally__chip-box'])}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />
          <Controller
            control={control}
            name="additionally"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={additionallyThird}
                classProps={classNames(styles['additionally__chip-box'])}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />

          {checkbox()}
        </form>
      </>
    )
  }

  return (
    // TODO: добавить модалке параметр isSwipe (должна свайпаться вниз на мобильной версии)
    <ModalBase
      isSwipe
      classFooter={classNames(styles.modal__footer)}
      footer={
        <div>
          <ButtonGeneral
            className={styles['submit-button']}
            font="l"
            full="filled"
            height="48"
            type="submit"
            onClick={() => {
              onSubmit(getValues())
            }}
          >
            Отправить заявку
          </ButtonGeneral>
        </div>
      }
      isOpen={isOpen}
      title="Запись на просмотр"
      translate="bottom"
      onClose={onClose}
    >
      <div className={styles.modal}>
        {renderApartmentPrewiew()}
        {renderForm()}
      </div>
    </ModalBase>
  )
}
