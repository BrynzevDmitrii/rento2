import { ButtonHTMLAttributes, DetailedHTMLProps, FC } from 'react'
import classNames from 'classnames'

import styles from './SelectDateButton.module.scss'

interface ISelectDateButton
  extends DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  // TODO: определить тип value после того, как сделаем календарь
  fill?: 'gray' | 'light'
}

export const SelectDateButton: FC<ISelectDateButton> = ({
  value,
  className,
  fill = 'light',
  type,
  ...props
}) => (
  <button
    className={classNames(styles.button, styles[`button-${fill}`], className)}
    type="button"
    {...props}
  >
    {/* //TODO: доделать верстку после того, как сделаем календарь */}
    {value ?? 'Выберите даты'}
  </button>
)
