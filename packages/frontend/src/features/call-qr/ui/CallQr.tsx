import classNames from 'classnames'
import { QRCodeSVG } from 'qrcode.react'
import { DetailedHTMLProps, FC, HTMLAttributes } from 'react'

import styles from './CallQr.module.scss'

interface IPropsCallQr
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  phone: string
}

export const CallQr: FC<IPropsCallQr> = ({ phone, className, ...props }) => {
  return (
    <div className={classNames(styles.wrapper, className)} {...props}>
      <div className={styles.qr}>
        <QRCodeSVG size={70} value={`tel:${phone}`} />
      </div>
      <div>
        <p className={styles.title}>{phone}</p>
        <p className={styles.text}>
          Для быстрого набора номера наведите на код камеру телефона
        </p>
      </div>
    </div>
  )
}
