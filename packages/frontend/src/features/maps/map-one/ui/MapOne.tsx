import classNames from 'classnames'
import Image from 'next/image'
import { DetailedHTMLProps, HTMLAttributes } from 'react'

import styles from './MapOne.module.scss'

interface IPropsMap
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}

export const MapOne = ({ className, ...props }: IPropsMap): JSX.Element => {
  return (
    <div className={classNames(styles.wrapper, className)} {...props}>
      <Image
        alt="Карта"
        layout="fill"
        objectFit="cover"
        src="/images/long/map-one.jpg"
      />
    </div>
  )
}
