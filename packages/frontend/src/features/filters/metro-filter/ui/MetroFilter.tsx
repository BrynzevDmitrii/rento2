import { FC, useCallback, useEffect, useState } from 'react'
import { Controller, useForm, useWatch } from 'react-hook-form'
import classNames from 'classnames'

import { ButtonGeneral, CheckBox, SearchInput, ModalBase } from '@shared/ui'
import { IMetroStation } from '@shared/api'

import { IMetroForm, IPropsMetroFilter, SortTypesEnum } from '../lib/types'
import { MetroList } from './metro-list/MetroList'

import styles from './MetroFilter.module.scss'
import { MetroNoData } from './metro-list/MetroNoData'
import { MetroGroupLineList } from './metro-list/MetroGroupLineList'
import {
  filteringMetroLine,
  filteringMetroStation,
  isContainsMetro,
  mergedUnique,
} from '../lib/helpers'

const defaultValues: IMetroForm = {
  search: '',
  isCentral: false,
  isCircleLine: false,
  sortBy: SortTypesEnum.byAbc,
}

export const MetroFilter: FC<IPropsMetroFilter> = ({
  isOpen,
  onClose,
  metroStations,
  controlMain,
  onChange,
}) => {
  const { control, reset } = useForm<IMetroForm>({ defaultValues })
  const metroStationsSelect = useWatch({
    control: controlMain,
    name: 'metroStations',
  })

  const [filteredStations, setFilteredStations] = useState(metroStations)

  const [checkedStations, setCheckedStations] = useState<IMetroStation[]>([])

  const switchStation = (metroStation: IMetroStation): void =>
    isContainsMetro(checkedStations, metroStation)
      ? setCheckedStations(filteringMetroStation(checkedStations, metroStation))
      : setCheckedStations([...checkedStations, metroStation])

  const switchStationLine = (metroStations: IMetroStation[]): void =>
    metroStations.every((item) => isContainsMetro(checkedStations, item))
      ? setCheckedStations(filteringMetroLine(checkedStations, metroStations))
      : setCheckedStations(mergedUnique(checkedStations, metroStations))

  const search = useWatch({ control, name: 'search' })
  const sortBy = useWatch({ control, name: 'sortBy' })

  useEffect(() => {
    if (isOpen) {
      setCheckedStations(metroStationsSelect)
    }
  }, [isOpen, metroStationsSelect])

  const onSubmit = (): void => {
    onChange(sortStations(checkedStations))
    onClose()
  }
  const onReset = (): void => {
    reset(defaultValues)
    setCheckedStations([])
  }

  const getCentral = (isChecked: boolean): void => {
    isChecked
      ? setFilteredStations([
          ...filteredStations.filter((item) => item.isCentral),
        ])
      : setFilteredStations([...sortStations(metroStations)])
  }

  const sortStations = useCallback(
    (filteredStations: IMetroStation[]): IMetroStation[] => {
      switch (sortBy) {
        case SortTypesEnum.byAbc: {
          return [
            ...filteredStations.sort((a, b) => a.name.localeCompare(b.name)),
          ]
        }
        case SortTypesEnum.byLine: {
          return [
            ...filteredStations.sort((a, b) =>
              a.metroLine.name.localeCompare(b.metroLine.name) !== 0
                ? a.metroLine.name.localeCompare(b.metroLine.name)
                : b.metroLine.name.localeCompare(a.metroLine.name)
            ),
          ]
        }
        default: {
          return [...filteredStations]
        }
      }
    },
    [sortBy]
  )

  useEffect(() => {
    setFilteredStations(
      sortStations(metroStations).filter((metro) =>
        metro.name.toLowerCase().includes(search)
      )
    )
  }, [search, metroStations, sortStations])

  useEffect(() => {
    setFilteredStations((prevState) => sortStations(prevState))
  }, [sortBy, sortStations])

  return (
    <ModalBase
      classBody={classNames(styles.body__wrapper)}
      classFooter={classNames(styles.footer__wrapper)}
      footer={
        <div className={styles.footer}>
          <ButtonGeneral
            className={styles['footer__button-reset']}
            font="s"
            full="filled"
            grade="neutral"
            height="40"
            onClick={onReset}
          >
            Сбросить
          </ButtonGeneral>
          <ButtonGeneral
            className={styles['footer__button-accept']}
            font="s"
            form="metroFilter"
            full="filled"
            height="40"
            onClick={onSubmit}
          >
            Применить
          </ButtonGeneral>
        </div>
      }
      isOpen={isOpen}
      translate="right"
      onClose={onClose}
    >
      <div className={styles.metro__wrapper}>
        <div className={styles.metro__search}>
          <Controller
            control={control}
            name="search"
            render={({ field: { value, onChange } }) => (
              <SearchInput
                name="searchMetro"
                placeholder="Метро"
                value={value}
                onChange={onChange}
              />
            )}
          />
        </div>
        <div className={styles.metro__toolbar}>
          <div className={styles['metro__toolbar-checkbox']}>
            <Controller
              control={control}
              name="isCentral"
              render={({ field: { value, onChange } }) => (
                <CheckBox
                  label="Все станции внутри кольца"
                  value={value}
                  onChange={(e) => {
                    onChange(e)
                    getCentral(e)
                  }}
                />
              )}
            />
            <Controller
              control={control}
              name="isCircleLine"
              render={({ field: { value, onChange } }) => (
                <CheckBox
                  label="Все станции кольцевой"
                  value={value}
                  onChange={onChange}
                />
              )}
            />
          </div>
          <div className={styles['metro__toolbar-sort']}>
            <Controller
              control={control}
              name="sortBy"
              render={({ field: { value, onChange } }) => (
                <div className={classNames(styles.tabs__wrapper)}>
                  {Object.values(SortTypesEnum).map((sortValue, index) => (
                    <ButtonGeneral
                      key={index}
                      className={classNames(styles.tabs__button, {
                        [styles['tabs__button-active']]: sortValue === value,
                      })}
                      font="xs"
                      full="text"
                      grade="iris"
                      height="32"
                      type="button"
                      onClick={() => onChange(sortValue)}
                    >
                      {sortValue}
                    </ButtonGeneral>
                  ))}
                </div>
              )}
            />
          </div>
        </div>
      </div>

      {filteredStations.length === 0 && <MetroNoData />}
      {sortBy === SortTypesEnum.byAbc && filteredStations.length !== 0 && (
        <MetroList
          checkedStations={checkedStations}
          metroStations={filteredStations}
          onChangeMetroList={switchStation}
        />
      )}
      {sortBy === SortTypesEnum.byLine && filteredStations.length !== 0 && (
        <MetroGroupLineList
          checkedStations={checkedStations}
          metroStations={filteredStations}
          onChangeMetroLine={switchStationLine}
          onChangeMetroList={switchStation}
        />
      )}
    </ModalBase>
  )
}
