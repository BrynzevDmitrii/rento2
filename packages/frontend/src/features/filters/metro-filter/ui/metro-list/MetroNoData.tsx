import styles from './MetroNoData.module.scss'

export const MetroNoData = (): JSX.Element => {
  return (
    <div className={styles.empty}>
      По этому запросу ничего не нашлось. Попробуйте изменить запрос.
    </div>
  )
}
