import { FC } from 'react'
import classNames from 'classnames'

import { IconMetro, CheckBox } from '@shared/ui'
import { stringUtils } from '@shared/lib'

import { IMetroList } from '../../lib/types'

import styles from './MetroList.module.scss'
import { isContainsMetro } from '../../lib/helpers'

export const MetroList: FC<IMetroList> = ({
  metroStations,
  checkedStations,
  onChangeMetroList,
  className,
  ...props
}) => {
  return (
    <div className={classNames(styles.list, className)} {...props}>
      {metroStations.map((metroStation) => (
        <div
          key={metroStation.id}
          className={styles.item}
          onClick={() => onChangeMetroList(metroStation)}
        >
          <IconMetro
            className={styles.item__icon}
            style={{
              fill: stringUtils.getColorMetro(metroStation.metroLine.color),
            }}
          />
          <span className={styles.item__text}>{metroStation.name}</span>
          <CheckBox
            classBox={classNames(styles.checkbox)}
            value={isContainsMetro(checkedStations, metroStation)}
          />
        </div>
      ))}
    </div>
  )
}
