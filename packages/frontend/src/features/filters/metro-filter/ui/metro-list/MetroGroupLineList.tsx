import { FC } from 'react'
import classNames from 'classnames'

import { stringUtils } from '@shared/lib'
import { CheckBox } from '@shared/ui'

import { IMetroGroupLineList } from '../../lib/types'
import { groupByMetroLine, isContainsMetro } from '../../lib/helpers'
import { MetroList } from './MetroList'

import styles from './MetroGroupLineList.module.scss'

export const MetroGroupLineList: FC<IMetroGroupLineList> = ({
  metroStations,
  checkedStations,
  onChangeMetroList,
  onChangeMetroLine,
}) => {
  const stations = groupByMetroLine(metroStations)

  const isChecked = (item: string): boolean => {
    return (
      (checkedStations.length > 0 &&
        stations[item]?.every((item) =>
          isContainsMetro(checkedStations, item)
        )) ??
      false
    )
  }

  return (
    <div className={styles.wrapper}>
      {Object.keys(stations).map((item, idx) => {
        return (
          <div key={`stations-${idx}`}>
            <div
              className={styles['metro-line']}
              onClick={() => onChangeMetroLine(stations[item] ?? [])}
            >
              <div
                className={styles['metro-line__line']}
                style={{
                  backgroundColor: stringUtils.getColorMetro(
                    stations[item]?.[0]?.metroLine.color ?? ''
                  ),
                }}
              />
              {stations[item]?.[0]?.metroLine.name}
              <CheckBox
                classBox={classNames(styles['metro-line__checkbox'])}
                classWrapper={classNames(
                  styles['metro-line__checkbox-wrapper']
                )}
                value={isChecked(item)}
              />
            </div>
            <MetroList
              checkedStations={checkedStations}
              className={styles['metro-list']}
              metroStations={stations[item] ?? []}
              onChangeMetroList={onChangeMetroList}
            />
          </div>
        )
      })}
    </div>
  )
}
