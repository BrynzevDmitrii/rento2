import { IMetroStation } from '@shared/api'

const mergedUnique = (
  stations: IMetroStation[],
  newStations: IMetroStation[]
): IMetroStation[] => {
  const ids = new Set(stations.map((item) => item.id))
  return [...stations, ...newStations.filter((item) => !ids.has(item.id))]
}

const isContainsMetro = (
  checkedStations: IMetroStation[],
  metroStation: IMetroStation
): boolean => checkedStations.some((item) => item.id === metroStation.id)

const groupByMetroLine = (
  metroStations: IMetroStation[]
): Record<string, IMetroStation[]> => {
  return metroStations.reduce<Record<string, IMetroStation[]>>(
    (previousValue, currentValue) => {
      ;(previousValue[currentValue['metroLine']['name']] =
        previousValue[currentValue['metroLine']['name']] ?? []).push(
        currentValue
      )

      return previousValue
    },
    {}
  )
}

const filteringMetroLine = (
  checkedStations: IMetroStation[],
  metroStations: IMetroStation[]
): IMetroStation[] => {
  return checkedStations.filter(
    (station) => !metroStations.some((item) => station.id === item.id)
  )
}

const filteringMetroStation = (
  checkedStations: IMetroStation[],
  metroStation: IMetroStation
): IMetroStation[] =>
  checkedStations.filter((station) => station.id !== metroStation.id)

export {
  mergedUnique,
  isContainsMetro,
  groupByMetroLine,
  filteringMetroLine,
  filteringMetroStation,
}
