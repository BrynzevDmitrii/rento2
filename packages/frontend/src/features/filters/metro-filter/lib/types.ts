import { IMetroStation } from '@shared/api'
import { DetailedHTMLProps, HTMLAttributes } from 'react'
import { Control } from 'react-hook-form'

interface IPropsMetroFilter {
  isOpen: boolean
  onClose: () => void
  onChange: (value: IMetroStation[]) => void
  metroStations: IMetroStation[]
  controlMain: Control<any>
}

interface IMetroForm {
  search: string
  isCentral: boolean
  isCircleLine: boolean
  sortBy: string
}

enum SortTypesEnum {
  byLine = 'По Линиям',
  byAbc = 'По Алфавиту',
}

interface IMetroList
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  metroStations: IMetroStation[]
  checkedStations: IMetroStation[]
  onChangeMetroList: (metroStation: IMetroStation) => void
}

interface IMetroGroupLineList {
  metroStations: IMetroStation[]
  checkedStations: IMetroStation[]
  onChangeMetroLine: (metroStation: IMetroStation[]) => void
  onChangeMetroList: (metroStation: IMetroStation) => void
}

export type { IPropsMetroFilter, IMetroForm, IMetroList, IMetroGroupLineList }
export { SortTypesEnum }
