import { FC, useMemo } from 'react'
import classNames from 'classnames'
import { Controller } from 'react-hook-form'

import {
  ButtonGeneral,
  IconLocation,
  IconSetting4,
  Select,
  ChipBox,
  DoubleInput,
  ButtonIcon,
} from '@shared/ui'
import { onPromise } from '@shared/lib'
import { optionsSort } from '@shared/config'

import { roomsNumber } from '../model/dataFilter'
import { IPropsFilter } from '../lib/types'

import mainStyles from './MainFilter.module.scss'
import headerStyles from './HeaderFilter.module.scss'

export const MainFilterLong: FC<IPropsFilter> = ({
  classWrapperMainFilter = '',
  isHeaderMode,
  showHeader,
  control,
  onReset,
  onSubmit,
  handleClickAdvanced,
}) => {
  const styles = useMemo(
    () => (isHeaderMode ? headerStyles : mainStyles),
    [isHeaderMode]
  )

  return (
    <div
      className={classNames(styles.filter, {
        [styles.sticky]: showHeader,
        [classWrapperMainFilter]: !isHeaderMode,
      })}
    >
      <form
        className={classNames(styles.filter__container, {
          container: isHeaderMode,
        })}
        onSubmit={onPromise(onSubmit)}
      >
        <fieldset className={styles.fieldset}>
          <Controller
            control={control}
            name="priceRange"
            render={({ field: { value, onChange } }) => (
              <DoubleInput
                classProps={classNames(styles.fieldset__item, styles.double)}
                placeholder={{ min: '50000', max: '2000000' }}
                unit="₽"
                value={value}
                onChange={onChange}
              />
            )}
          />
          <Controller
            control={control}
            name="roomsNumber"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                chips={roomsNumber}
                classProps={classNames(
                  styles.fieldset__item,
                  styles['chip-box']
                )}
                value={value}
                onChange={onChange}
              />
            )}
          />
          {!isHeaderMode && (
            <ButtonGeneral
              className={styles['button-map']}
              font="s"
              full="filled"
              grade="neutral"
              height="40"
              type="button"
            >
              <span className={styles['button-map__text']}>На карте</span>
              <IconLocation className={styles['button-map__icon']} />
            </ButtonGeneral>
          )}
        </fieldset>
        <fieldset className={styles.fieldset}>
          <div className={styles.other}>
            <ButtonIcon
              appearance="buttonIconMain"
              className={styles['button-filters']}
              grade="neutral"
              size="40"
              type="button"
              onClick={handleClickAdvanced}
            >
              <IconSetting4 />
            </ButtonIcon>

            <Controller
              control={control}
              name="sortRange"
              render={({ field: { value, onChange } }) => (
                <Select
                  className={classNames(styles.select)}
                  instanceId="select"
                  isSearchable={false}
                  options={optionsSort}
                  value={value}
                  onChange={onChange}
                />
              )}
            />
            <ButtonGeneral
              className={styles['button-reset']}
              font="s"
              full="text"
              grade="accent"
              height="40"
              type="button"
              onClick={onReset}
            >
              Сбросить
            </ButtonGeneral>
          </div>
          <ButtonGeneral
            round
            className={styles['button-search']}
            font="s"
            full="filled"
            height="40"
            type="submit"
          >
            Найти квартиру
          </ButtonGeneral>
          {isHeaderMode && (
            <ButtonGeneral
              className={styles['button-map']}
              font="s"
              full="filled"
              grade="neutral"
              height="40"
              type="button"
            >
              <IconLocation />
            </ButtonGeneral>
          )}
        </fieldset>
      </form>
    </div>
  )
}
