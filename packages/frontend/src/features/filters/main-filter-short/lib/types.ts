import { Control, SubmitHandler } from 'react-hook-form'

import { RoomsNumber } from '@shared/api'

interface IPropsFilter {
  classWrapperMainFilter?: string
  isHeaderMode: boolean
  showHeader: boolean
  handleClickAdvanced: () => void
  // TODO: После типизации defaultValue, заменить any
  control: Control<any>
  onSubmit: SubmitHandler<any>
  onReset: () => void
  selectGuests: JSX.Element
  selectDateButton: JSX.Element
}

interface IChip<T> {
  key: T
  title: string
}

type RoomsNumberChips = Array<IChip<RoomsNumber>>

export type { IPropsFilter, RoomsNumberChips }
