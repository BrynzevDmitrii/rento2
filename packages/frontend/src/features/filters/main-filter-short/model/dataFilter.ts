import { RoomsNumberChips } from '../lib/types'

const roomsNumber: RoomsNumberChips = [
  {
    key: 'studio',
    title: 'Студия',
  },
  {
    key: '1',
    title: '1к',
  },
  {
    key: '2',
    title: '2к',
  },
  {
    key: '3',
    title: '3к+',
  },
]

export { roomsNumber }
