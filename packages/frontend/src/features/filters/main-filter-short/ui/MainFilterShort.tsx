import { FC, useMemo } from 'react'
import classNames from 'classnames'
import { Controller } from 'react-hook-form'

import {
  ButtonGeneral,
  IconLocation,
  IconSetting4,
  Select,
  ButtonIcon,
} from '@shared/ui'
import { onPromise } from '@shared/lib'
import { optionsSort } from '@shared/config'

import { IPropsFilter } from '../lib/types'

import mainStyles from './MainFilter.module.scss'
import headerStyles from './HeaderFilter.module.scss'

export const MainFilterShort: FC<IPropsFilter> = ({
  classWrapperMainFilter = '',
  isHeaderMode,
  showHeader,
  control,
  onReset,
  onSubmit,
  handleClickAdvanced,
  selectGuests,
  selectDateButton,
}) => {
  const styles = useMemo(
    () => (isHeaderMode ? headerStyles : mainStyles),
    [isHeaderMode]
  )

  return (
    <div
      className={classNames(styles.filter, {
        [styles.sticky]: showHeader,
        [classWrapperMainFilter]: !isHeaderMode,
      })}
    >
      <form
        className={classNames(styles.filter__container, {
          container: isHeaderMode,
        })}
        onSubmit={onPromise(onSubmit)}
      >
        <div className={styles['button-date']}>{selectDateButton}</div>
        <div className={styles['select-guests']}>{selectGuests}</div>

        <ButtonGeneral
          className={styles['button-map']}
          font="s"
          full="filled"
          grade="neutral"
          height="40"
          type="button"
        >
          <span className={styles['button-map__text']}>На карте</span>
          <IconLocation className={styles['button-map__icon']} />
        </ButtonGeneral>

        <ButtonIcon
          appearance="buttonIconMain"
          className={styles['button-filters']}
          grade="neutral"
          size="40"
          type="button"
          onClick={handleClickAdvanced}
        >
          <IconSetting4 />
        </ButtonIcon>

        <Controller
          control={control}
          name="sortRange"
          render={({ field: { value, onChange } }) => (
            <Select
              className={classNames(styles.select)}
              instanceId="select"
              isSearchable={false}
              options={optionsSort}
              value={value}
              onChange={onChange}
            />
          )}
        />
        <ButtonGeneral
          className={styles['button-reset']}
          font="xs"
          full="text"
          grade="accent"
          height="40"
          type="button"
          onClick={onReset}
        >
          Сбросить
        </ButtonGeneral>

        <ButtonGeneral
          round
          className={styles['button-search']}
          font="s"
          full="filled"
          height="40"
          type="submit"
        >
          Найти квартиру
        </ButtonGeneral>
      </form>
    </div>
  )
}
