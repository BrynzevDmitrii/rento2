import {
  AdditionalFeaturesChips,
  ApartmentFurnitureChips,
  AppliancesChips,
  AvailabilityTimeToChips,
  BuildingTypeChips,
  FloorTypeChips,
  RoomsNumberChips,
} from '../lib/types'

const additionalFeatures: AdditionalFeaturesChips = [
  {
    key: 'areChildrenAllowed',
    title: 'Можно с детьми',
  },
  {
    key: 'arePetsAllowed',
    title: 'Можно с животными',
  },
]

const roomsNumber: RoomsNumberChips = [
  {
    key: 'studio',
    title: 'Студия',
  },
  {
    key: '1',
    title: '1к',
  },
  {
    key: '2',
    title: '2к',
  },
  {
    key: '3',
    title: '3к+',
  },
]

const availabilityTimeTo: AvailabilityTimeToChips = [
  {
    key: 5,
    title: 'до 5 мин.',
  },
  {
    key: 10,
    title: 'до 10 мин.',
  },
  {
    key: 15,
    title: 'до 15 мин.',
  },
  {
    key: 20,
    title: '20+ мин.',
  },
]

const buildingType: BuildingTypeChips = [
  {
    key: 'stalinEra',
    title: 'Сталинки',
  },
  {
    key: 'newlyBuilt',
    title: 'Новостройки',
  },
  {
    key: 'inTheCenter',
    title: 'В центре',
  },
]

const floorType: FloorTypeChips = [
  {
    key: 'notFirst',
    title: 'Не первый',
  },
  {
    key: 'notLast',
    title: 'Не последний',
  },
  {
    key: 'last',
    title: 'Последний',
  },
]

const apartmentFurniture: ApartmentFurnitureChips = [
  {
    key: 'withFurniture',
    title: 'С мебелью',
  },
  {
    key: 'withoutFurniture',
    title: 'Без мебели',
  },
  {
    key: 'goodRepairs',
    title: 'Хороший ремонт',
  },
  {
    key: 'balcony',
    title: 'Балкон / лоджия',
  },
]

const appliancesFirst: AppliancesChips = [
  {
    key: 'refrigerator',
    title: 'Холодильник',
  },
  {
    key: 'tv',
    title: 'Телевизор',
  },
  {
    key: 'washingMachine',
    title: 'Стиральная машинка',
  },
]

const appliancesSecond: AppliancesChips = [
  {
    key: 'dishWasher',
    title: 'Посудомоечная машинка',
  },
  {
    key: 'airConditioner',
    title: 'Кондиционер',
  },
]

export {
  additionalFeatures,
  roomsNumber,
  availabilityTimeTo,
  buildingType,
  floorType,
  apartmentFurniture,
  appliancesFirst,
  appliancesSecond,
}
