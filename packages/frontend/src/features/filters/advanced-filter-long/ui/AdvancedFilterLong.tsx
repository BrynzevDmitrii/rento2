import { FC, useEffect } from 'react'
import classNames from 'classnames'
import { Controller, useWatch, useController } from 'react-hook-form'

import {
  ButtonGeneral,
  ChipBox,
  DoubleInput,
  IconArrowRight,
  IconBus,
  IconMetro,
  IconWalker,
  ModalBase,
  RadioButton,
} from '@shared/ui'

import {
  additionalFeatures,
  roomsNumber,
  availabilityTimeTo,
  buildingType,
  floorType,
  apartmentFurniture,
  appliancesFirst,
  appliancesSecond,
} from '../model/dataFilter'
import { IPropsFilter } from '../lib/types'
import { MetroChipTags } from './metro-chip-tags/MetroChipTags'

import styles from './AdvancedFilterLong.module.scss'

export const AdvancedFilterLong: FC<IPropsFilter> = ({
  isOpen,
  onClose,
  control,
  onReset,
  onSubmit,
  handleClickMetro,
}) => {
  const metroStations = useWatch({ control, name: 'metroStations' })

  const { field } = useController({
    name: 'availablityBy',
    control,
  })
  const availabilityTime = useWatch({
    control,
    name: 'availabilityTimeTo',
  })

  useEffect(() => {
    if (availabilityTime.length === 0 && field.value.length !== 0) {
      field.onChange('')
    }

    if (availabilityTime.length > 0 && field.value.length === 0) {
      field.onChange('foot')
    }
  }, [availabilityTime, field])

  return (
    <ModalBase
      footer={
        <div className={styles.footer}>
          <ButtonGeneral
            className={styles['footer__button-reset']}
            font="s"
            full="filled"
            grade="neutral"
            height="40"
            type="button"
            onClick={onReset}
          >
            Очистить фильтры
          </ButtonGeneral>
          <ButtonGeneral
            className={styles['footer__button-search']}
            font="s"
            form="detailFilter"
            full="filled"
            height="40"
            type="submit"
          >
            Показать квартиры
          </ButtonGeneral>
        </div>
      }
      isOpen={isOpen}
      translate="right"
      onClose={onClose}
    >
      <form
        id="detailFilter"
        onSubmit={(e) => {
          onSubmit(e)
          onClose()
        }}
      >
        <div className={styles['sub-header']}>
          <Controller
            control={control}
            name="priceRange"
            render={({ field: { value, onChange } }) => (
              <DoubleInput
                placeholder={{ min: '50000', max: '2000000' }}
                unit="₽"
                value={value}
                onChange={onChange}
              />
            )}
          />
        </div>
        <div className={styles.container}>
          <Controller
            control={control}
            name="additionalFeatures"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={additionalFeatures}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />
          <div className={styles.separator} />

          <h3 className={styles.subtitle}>Параметры квартиры</h3>
          <div className={styles.options}>
            <Controller
              control={control}
              name="area"
              render={({ field: { value, onChange } }) => (
                <DoubleInput
                  classProps={classNames(styles.options__double)}
                  placeholder={{ min: '10', max: '120' }}
                  unit="м2"
                  value={value}
                  onChange={onChange}
                />
              )}
            />
            <Controller
              control={control}
              name="roomsNumber"
              render={({ field: { value, onChange } }) => (
                <ChipBox
                  chips={roomsNumber}
                  classProps={classNames(styles['options__chip-box'])}
                  value={value}
                  onChange={onChange}
                />
              )}
            />
          </div>

          <button
            className={styles['button-metro']}
            type="button"
            onClick={handleClickMetro}
          >
            <IconMetro className={styles['button-metro__icon']} />
            Станция метро
            <IconArrowRight className={styles['button-metro__arrow']} />
          </button>
          {isOpen && metroStations.length !== 0 && (
            <div className={styles.metro}>
              <Controller
                control={control}
                name="metroStations"
                render={({ field: { value, onChange } }) => (
                  <MetroChipTags
                    chips={metroStations}
                    classPropsBox={classNames(styles.metro)}
                    value={value}
                    onChange={onChange}
                  />
                )}
              />
            </div>
          )}
          <div className={styles.separator} />

          <h3 className={styles.subtitle}>Время до метро</h3>
          <div className={classNames(styles.time, 'flex-center')}>
            <div className={classNames('flex-center')}>
              <Controller
                control={control}
                name="availablityBy"
                render={({ field }) => (
                  <RadioButton
                    {...field}
                    checked={field.value === 'foot'}
                    classProps={classNames(styles.time__radio)}
                    icon={<IconWalker />}
                    value="foot"
                  />
                )}
              />
              <Controller
                control={control}
                name="availablityBy"
                render={({ field }) => (
                  <RadioButton
                    {...field}
                    checked={field.value === 'vehicle'}
                    classProps={classNames(styles.time__radio)}
                    icon={<IconBus />}
                    value="vehicle"
                  />
                )}
              />
            </div>
            <Controller
              control={control}
              name="availabilityTimeTo"
              render={({ field: { value, onChange } }) => (
                <ChipBox
                  isRadio
                  isSwipeable
                  chips={availabilityTimeTo}
                  classProps={classNames(styles['time__chip-box'])}
                  marginRight={10}
                  value={value}
                  onChange={onChange}
                />
              )}
            />
          </div>
          <div className={styles.devider} />

          <h3 className={styles.subtitle}>Дом</h3>
          <Controller
            control={control}
            name="buildingType"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={buildingType}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />
          <div className={styles.separator} />

          <h3 className={styles.subtitle}>Этаж</h3>
          <div className={classNames(styles.floor)}>
            <Controller
              control={control}
              name="floorRange"
              render={({ field: { value, onChange } }) => (
                <DoubleInput
                  classProps={classNames(styles.floor__double)}
                  placeholder={{ min: '1', max: '40' }}
                  value={value}
                  onChange={onChange}
                />
              )}
            />
            <Controller
              control={control}
              name="floorType"
              render={({ field: { value, onChange } }) => (
                <ChipBox
                  isSwipeable
                  chips={floorType}
                  marginRight={4}
                  value={value}
                  onChange={onChange}
                />
              )}
            />
          </div>
          <div className={styles.separator} />

          <h3 className={styles.subtitle}>Квартира</h3>
          <Controller
            control={control}
            name="apartmentFurniture"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={apartmentFurniture}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />
          <div className={styles.separator} />

          <h3 className={styles.subtitle}>Техника</h3>
          <Controller
            control={control}
            name="appliances"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={appliancesFirst}
                classProps={classNames(styles['technique__chip-box'])}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />
          <Controller
            control={control}
            name="appliances"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={appliancesSecond}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />
        </div>
      </form>
    </ModalBase>
  )
}
