import {
  ApartmentFurniture,
  Appliances,
  BuildingType,
  FloorType,
  RoomsNumber,
} from '@shared/api'
import { Control, SubmitHandler } from 'react-hook-form'

interface IPropsFilter {
  isOpen: boolean
  onClose: () => void
  handleClickMetro: () => void
  // TODO: После типизации defaultValue, заменить any
  control: Control<any>
  onSubmit: SubmitHandler<any>
  onReset: () => void
}

interface IChip<T> {
  key: T
  title: string
}

type RoomsNumberChips = Array<IChip<RoomsNumber>>
type AdditionalFeaturesChips = Array<
  IChip<'areChildrenAllowed' | 'arePetsAllowed'>
>
type FloorTypeChips = Array<IChip<FloorType>>
type AppliancesChips = Array<IChip<Appliances>>
type AvailabilityTimeToChips = Array<IChip<number>>
type ApartmentFurnitureChips = Array<IChip<ApartmentFurniture>>
type BuildingTypeChips = Array<IChip<BuildingType>>

export type {
  IPropsFilter,
  RoomsNumberChips,
  AdditionalFeaturesChips,
  FloorTypeChips,
  AppliancesChips,
  AvailabilityTimeToChips,
  ApartmentFurnitureChips,
  BuildingTypeChips,
}
