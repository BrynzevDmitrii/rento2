import {
  Appliances,
  OptionsShort,
  PricePerNight,
  RoomsNumber,
} from '@shared/api'
import { Control, SubmitHandler } from 'react-hook-form'

interface IPropsFilter {
  isOpen: boolean
  onClose: () => void
  handleClickMetro: () => void
  // TODO: После типизации defaultValue, заменить any
  control: Control<any>
  onSubmit: SubmitHandler<any>
  onReset: () => void
  selectGuests: JSX.Element
}

interface IChip<T> {
  key: T
  title: string
}

type RoomsNumberChips = Array<IChip<RoomsNumber>>
type PricePerNightChips = Array<IChip<PricePerNight>>
type OptionsChips = Array<IChip<OptionsShort>>
type AppliancesChips = Array<IChip<Appliances>>
type AvailabilityTimeToChips = Array<IChip<number>>

export type {
  IPropsFilter,
  RoomsNumberChips,
  AppliancesChips,
  AvailabilityTimeToChips,
  PricePerNightChips,
  OptionsChips,
}
