import {
  AppliancesChips,
  AvailabilityTimeToChips,
  OptionsChips,
  PricePerNightChips,
  RoomsNumberChips,
} from '../lib/types'

const roomsNumber: RoomsNumberChips = [
  {
    key: 'studio',
    title: 'Студия',
  },
  {
    key: '1',
    title: '1к',
  },
  {
    key: '2',
    title: '2к',
  },
  {
    key: '3',
    title: '3к+',
  },
]

const availabilityTimeTo: AvailabilityTimeToChips = [
  {
    key: 5,
    title: 'до 5 мин.',
  },
  {
    key: 10,
    title: 'до 10 мин.',
  },
  {
    key: 15,
    title: 'до 15 мин.',
  },
  {
    key: 20,
    title: '20+ мин.',
  },
]

const appliances: AppliancesChips = [
  {
    key: 'dishWasher',
    title: 'Посудомоечная машинка',
  },
  {
    key: 'airConditioner',
    title: 'Кондиционер',
  },
]

const pricePerNight: PricePerNightChips = [
  {
    key: '<4000',
    title: 'до 4000₽',
  },
  {
    key: '4000-4500',
    title: '4000 — 4500₽',
  },
  {
    key: '4500-5000',
    title: '4500 — 5000₽',
  },
  {
    key: '>5000',
    title: 'От 5000₽',
  },
]

const optionsFirst: OptionsChips = [
  {
    key: 'childrenAllowed',
    title: 'Можно с детьми',
  },
  {
    key: 'petsAllowed',
    title: 'Можно с животными',
  },
]

const optionsSecond: OptionsChips = [
  {
    key: 'balcony',
    title: 'Есть балкон / лоджия',
  },
  {
    key: 'parking',
    title: 'Парковка рядом',
  },
  {
    key: 'moreThanAMonth',
    title: 'Дольше месяца',
  },
]

export {
  roomsNumber,
  availabilityTimeTo,
  appliances,
  pricePerNight,
  optionsFirst,
  optionsSecond,
}
