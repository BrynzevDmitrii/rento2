import { FC } from 'react'
import classNames from 'classnames'
import { Controller, useWatch } from 'react-hook-form'

import {
  ButtonGeneral,
  ChipBox,
  DoubleInput,
  IconArrowRight,
  IconBus,
  IconMetro,
  IconWalker,
  ModalBase,
  RadioButton,
  Select,
} from '@shared/ui'
import { optionsSort } from '@shared/config'

import {
  roomsNumber,
  availabilityTimeTo,
  appliances,
  pricePerNight,
  optionsFirst,
  optionsSecond,
} from '../model/dataFilter'
import { IPropsFilter } from '../lib/types'
import { MetroChipTags } from './metro-chip-tags/MetroChipTags'

import styles from './AdvancedFilterShort.module.scss'

export const AdvancedFilterShort: FC<IPropsFilter> = ({
  isOpen,
  onClose,
  control,
  onReset,
  onSubmit,
  handleClickMetro,
  selectGuests,
}) => {
  const metroStations = useWatch({ control, name: 'metroStations' })

  return (
    <ModalBase
      footer={
        <div className={styles.footer}>
          <ButtonGeneral
            className={styles['footer__button-reset']}
            font="s"
            full="filled"
            grade="neutral"
            height="40"
            type="button"
            onClick={onReset}
          >
            Очистить фильтры
          </ButtonGeneral>
          <ButtonGeneral
            className={styles['footer__button-search']}
            font="s"
            form="detailFilterShort"
            full="filled"
            height="40"
            type="submit"
          >
            Показать квартиры
          </ButtonGeneral>
        </div>
      }
      isOpen={isOpen}
      translate="right"
      onClose={onClose}
    >
      <form
        id="detailFilterShort"
        onSubmit={(e) => {
          onSubmit(e)
          onClose()
        }}
      >
        <div className={styles.container}>
          <h3 className={styles.subtitle}>Цена за ночь</h3>
          <Controller
            control={control}
            name="pricePerNight"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={pricePerNight}
                classProps={classNames(styles.price)}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />

          <div className={styles.inner}>
            <Controller
              control={control}
              name="sortRange"
              render={({ field: { value, onChange } }) => (
                <Select
                  isStroke
                  className={classNames(styles.select)}
                  instanceId="select"
                  isSearchable={false}
                  options={optionsSort}
                  value={value}
                  onChange={onChange}
                />
              )}
            />
            <div className={styles.guests}>{selectGuests}</div>
          </div>

          <h3 className={styles.subtitle}>Параметры квартиры</h3>
          <div className={styles.options}>
            <Controller
              control={control}
              name="area"
              render={({ field: { value, onChange } }) => (
                <DoubleInput
                  classProps={classNames(styles.options__double)}
                  placeholder={{ min: '10', max: '120' }}
                  unit="м2"
                  value={value}
                  onChange={onChange}
                />
              )}
            />
            <Controller
              control={control}
              name="roomsNumber"
              render={({ field: { value, onChange } }) => (
                <ChipBox
                  chips={roomsNumber}
                  classChip={classNames(styles['options__chip'])}
                  classProps={classNames(styles['options__chip-box'])}
                  value={value}
                  onChange={onChange}
                />
              )}
            />
          </div>

          <button
            className={styles['button-metro']}
            type="button"
            onClick={handleClickMetro}
          >
            <IconMetro className={styles['button-metro__icon']} />
            Станция метро
            <IconArrowRight className={styles['button-metro__arrow']} />
          </button>
          {isOpen && metroStations.length !== 0 && (
            <div className={styles.metro}>
              <Controller
                control={control}
                name="metroStations"
                render={({ field: { value, onChange } }) => (
                  <MetroChipTags
                    chips={metroStations}
                    classPropsBox={classNames(styles.metro)}
                    value={value}
                    onChange={onChange}
                  />
                )}
              />
            </div>
          )}
          <div className={styles.separator} />

          <h3 className={styles.subtitle}>Время до метро</h3>
          <div className={classNames(styles.time, 'flex-center')}>
            <div className={classNames('flex-center')}>
              <Controller
                control={control}
                name="availablityBy"
                render={({ field }) => (
                  <RadioButton
                    {...field}
                    checked={field.value === 'foot'}
                    classProps={classNames(styles.time__radio)}
                    icon={<IconWalker />}
                    value="foot"
                  />
                )}
              />
              <Controller
                control={control}
                name="availablityBy"
                render={({ field }) => (
                  <RadioButton
                    {...field}
                    checked={field.value === 'vehicle'}
                    classProps={classNames(styles.time__radio)}
                    icon={<IconBus />}
                    value="vehicle"
                  />
                )}
              />
            </div>
            <Controller
              control={control}
              name="availabilityTimeTo"
              render={({ field: { value, onChange } }) => (
                <ChipBox
                  isRadio
                  isSwipeable
                  chips={availabilityTimeTo}
                  classProps={classNames(styles['time__chip-box'])}
                  marginRight={10}
                  value={value}
                  onChange={onChange}
                />
              )}
            />
          </div>
          <div className={styles.devider} />

          <h3 className={styles.subtitle}>Основное</h3>
          <Controller
            control={control}
            name="options"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={optionsFirst}
                classProps={classNames(styles.basic)}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />
          <Controller
            control={control}
            name="options"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={optionsSecond}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />
          <div className={styles.separator} />

          <h3 className={styles.subtitle}>Техника</h3>
          <Controller
            control={control}
            name="appliances"
            render={({ field: { value, onChange } }) => (
              <ChipBox
                isSwipeable
                chips={appliances}
                classProps={classNames(styles['technique__chip-box'])}
                marginRight={10}
                value={value}
                onChange={onChange}
              />
            )}
          />
        </div>
      </form>
    </ModalBase>
  )
}
