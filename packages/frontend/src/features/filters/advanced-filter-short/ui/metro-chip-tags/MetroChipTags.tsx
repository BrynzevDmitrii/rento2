import { FC, useState } from 'react'
import classNames from 'classnames'

import { ButtonGeneral, IconClose, IconMetro } from '@shared/ui'
import { IMetroStation } from '@shared/api'
import { stringUtils } from '@shared/lib'

import styles from './MetroChipTags.module.scss'

interface IMetroChipTags {
  chips: IMetroStation[]
  value: IMetroStation[]
  classPropsBox?: string
  onChange: (value: IMetroStation[]) => void
}

const alwaysVisible: number = 4

export const MetroChipTags: FC<IMetroChipTags> = ({
  value,
  onChange,
  chips,
  classPropsBox,
}) => {
  const [showAll, setShowAll] = useState(false)

  const handleDelete = (chip: IMetroStation): void => {
    onChange(value.filter((el) => el !== chip))
  }

  return (
    <div className={styles.wrapper}>
      <div className={classNames(styles.box, classPropsBox)}>
        {(showAll ? chips : chips.slice(0, alwaysVisible)).map(
          (chip, index) => {
            return (
              <button
                key={index}
                className={classNames(styles.chip)}
                type="button"
                onClick={() => handleDelete(chip)}
              >
                <IconMetro
                  style={{
                    fill: stringUtils.getColorMetro(chip.metroLine.color),
                  }}
                />
                <span className={styles.chip__label}>{chip.name}</span>
                <IconClose className={classNames(styles['chip__icon-close'])} />
              </button>
            )
          }
        )}
      </div>
      <div className={styles.box__toolbar}>
        {value.length > alwaysVisible && (
          <ButtonGeneral
            font="xs"
            full="text"
            grade="accent"
            height="40"
            type="button"
            onClick={() => setShowAll(!showAll)}
          >
            {showAll ? 'Скрыть' : 'Показать'}
          </ButtonGeneral>
        )}
        <ButtonGeneral
          font="xs"
          full="text"
          grade="accent"
          height="40"
          onClick={() => onChange([])}
        >
          Очистить
        </ButtonGeneral>
      </div>
    </div>
  )
}
