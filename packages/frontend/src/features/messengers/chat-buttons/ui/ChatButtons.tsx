import { DetailedHTMLProps, FC, HTMLAttributes } from 'react'
import classNames from 'classnames'

import { ButtonMessenger, IconTelegram, IconWhatsapp } from '@shared/ui'

import styles from './ChatButtons.module.scss'

interface IPropsChatButtons
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  text?: string
  paths: {
    telegram: string
    whatsapp: string
  }
  appearance?: 'round' | 'neutral'
}

export const ChatButtons: FC<IPropsChatButtons> = ({
  text = 'Перейти в чат',
  paths,
  className,
  appearance = 'round',
  ...props
}) => {
  return (
    <div
      className={classNames(className, styles.messenger, {
        [styles.messenger_round]: appearance === 'round',
        [styles.messenger_neutral]: appearance === 'neutral',
      })}
      {...props}
    >
      {text}
      <div className={styles.messenger__inner}>
        <ButtonMessenger path={paths.telegram}>
          <IconTelegram isColor />
        </ButtonMessenger>
        <ButtonMessenger path={paths.whatsapp}>
          <IconWhatsapp isColor />
        </ButtonMessenger>
      </div>
    </div>
  )
}
