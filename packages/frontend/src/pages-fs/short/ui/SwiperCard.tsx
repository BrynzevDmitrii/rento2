import { Navigation, Pagination } from 'swiper'
import classNames from 'classnames'
import Image from 'next/image'

import { SwiperWithButton } from '@entities/carousels/SwiperWithButton'

import { mockPhotos } from '../model/mockData'

import styles from './SwiperCard.module.scss'

interface IProps {
  classBtnHover: string
}

export const SwiperCard = ({ classBtnHover }: IProps): JSX.Element => {
  const initParams = {
    loop: true,
    height: 220,
    modules: [Navigation, Pagination],
    pagination: {
      dynamicBullets: true,
      dynamicMainBullets: 4,
      clickable: true,
      bulletClass: classNames(styles.slider__bullet),
    },
  }
  return (
    <div className={styles.slider} onClick={(e) => e.preventDefault()}>
      <SwiperWithButton
        btnProps={{ size: '24', full: 'stroke' }}
        classBtnNext={classNames(styles['slider__btn-next'], classBtnHover)}
        classBtnPrev={classNames(styles['slider__btn-prev'], classBtnHover)}
        classIcon={classNames(styles['slider__btn-icon'])}
        classSlideWrapper={classNames(styles.slider__image)}
        elementData={mockPhotos}
        initSwiperParams={initParams}
      >
        {(image) => (
          <Image
            alt={image.alt}
            layout="fill"
            objectFit="cover"
            src={image.src}
          />
        )}
      </SwiperWithButton>
    </div>
  )
}
