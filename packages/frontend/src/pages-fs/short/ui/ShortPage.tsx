import classNames from 'classnames'
import { useEffect, useRef, useState } from 'react'

import { Layout, Breadcrumbs } from '@shared/ui'
import {
  IApartmentItem,
  IApartmentsListResponse,
  IMetroStation,
} from '@shared/api'
import { pathPages } from '@shared/config'
import { useSticky, stringUtils } from '@shared/lib'

import { Header } from '@widgets/header'
import { Footer } from '@widgets/footer'
import {
  CollectionApartments,
  CollectionCardShort,
} from '@widgets/collection-apartments'
import { FilterShort } from '@widgets/filter-short'

import { DISTANCE_BOTTOM_FILTER } from '../config/const'
import { ApartmentCardShort } from './ApartmentCardShort'
import { Banner } from './Banner'

import styles from './ShortPage.module.scss'

const titlePage = 'Снять хорошую квартиру от 6 месяцев &#128156;'

interface IBreadcrumbs {
  backPath: {
    path: string
    label: string
  }
  currentPathLabel: string
}

export const ShortPage = ({
  data,
  metroList,
}: {
  data: IApartmentsListResponse
  metroList: IMetroStation[]
}): JSX.Element => {
  const { data: apartments, collections } = data

  const [offsetTop, setOffsetTop] = useState(0)
  const anchorRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    setOffsetTop((anchorRef.current?.offsetTop ?? 0) + DISTANCE_BOTTOM_FILTER)
  }, [])

  const { isHeaderMode, showHeader } = useSticky(offsetTop)

  const getPath = (latinName: string): string => {
    return `/${pathPages.short}/${stringUtils.getSlug(latinName)}`
  }

  const getBreadcrumbs = (): IBreadcrumbs => ({
    backPath: {
      path: '/',
      label: 'Главная',
    },
    currentPathLabel: 'Посуточная аренда',
  })

  return (
    <Layout
      footer={<Footer />}
      header={<Header showHeader={showHeader} />}
      titlePage={titlePage}
    >
      <main>
        <section>
          <Breadcrumbs
            {...getBreadcrumbs()}
            className={classNames(styles.breadcrumbs, 'container')}
          />

          <FilterShort
            isHeaderMode={isHeaderMode}
            metroList={metroList}
            quantityApartment={5625462}
            showHeader={showHeader}
          />
          <div ref={anchorRef} />

          <ul className={classNames(styles['card-list'], 'container')}>
            {apartments?.slice(0, 10).map((el: IApartmentItem) => (
              <li key={el.id}>
                <ApartmentCardShort
                  apartmentData={el}
                  path={getPath(el.latinName)}
                />
              </li>
            ))}
          </ul>

          <Banner />

          <ul className={classNames(styles['card-list'], 'container')}>
            {apartments?.slice(10, 20).map((el: IApartmentItem) => (
              <li key={el.id}>
                <ApartmentCardShort
                  apartmentData={el}
                  path={getPath(el.latinName)}
                />
              </li>
            ))}
          </ul>

          <CollectionApartments
            cards={collections.inCityCenter}
            className={styles.collections}
            titleCollection="inCityCenter"
          >
            {(apartment) => (
              <CollectionCardShort
                apartmentData={apartment}
                path={getPath(apartment.latinName)}
              />
            )}
          </CollectionApartments>

          <ul className={classNames(styles['card-list'], 'container')}>
            {apartments?.slice(20, 30).map((el: IApartmentItem) => (
              <li key={el.id}>
                <ApartmentCardShort
                  apartmentData={el}
                  path={getPath(el.latinName)}
                />
              </li>
            ))}
          </ul>
        </section>
      </main>
    </Layout>
  )
}
