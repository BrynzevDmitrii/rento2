import classNames from 'classnames'

import { IApartmentItem } from '@shared/api'
import { roundInteger } from '@shared/lib'
import { DetailsShort, PriceShort } from '@shared/ui'
import { ApartmentCard } from '@entities/cards/ApartmentCard'
import { MetroInfo } from '@entities/MetroInfo'

import { SwiperCard } from './SwiperCard'

import styles from './ApartmentCardShort.module.scss'

export const ApartmentCardShort = ({
  apartmentData,
  path,
}: {
  apartmentData: IApartmentItem
  path: string
}): JSX.Element => {
  const area = roundInteger(Number(apartmentData.area))

  return (
    <ApartmentCard
      {...apartmentData}
      isRating
      classCard={classNames(styles.card)}
      detailsInfo={DetailsShort({
        roomsNum: apartmentData.roomsNum,
        area,
        storey: apartmentData.storey,
      })}
      media={
        <SwiperCard classBtnHover={classNames(styles['card__btn-hover'])} />
      }
      metroInfo={
        <MetroInfo
          metroStations={apartmentData.metroStations}
          timeFoot={apartmentData.metroAvailabilityByFoot}
          timeVehicle={apartmentData.metroAvailabilityByVehicle}
        />
      }
      pathPage={path}
      priceInfo={PriceShort(apartmentData.price)}
    />
  )
}
