import Image from 'next/image'
import classNames from 'classnames'

import { messengers } from '@shared/config'

import { ChatButtons } from '@features/messengers/chat-buttons'

import styles from './Banner.module.scss'

export const Banner = (): JSX.Element => {
  const messengerPath = {
    telegram: messengers.short.telegram,
    whatsapp: messengers.short.whatsApp,
  }

  return (
    <div className={classNames(styles.banner, 'container')}>
      <div className={styles.banner__inner}>
        <p className={styles.banner__title}>Нет времени искать?</p>
        <ul className={styles.banner__list}>
          <li className={styles.banner__text}>Подберем хороший вариант</li>
          <li className={styles.banner__text}>Отправим в удобный мессенджер</li>
          <li className={styles.banner__text}>Предложим скидку!</li>
        </ul>
        <ChatButtons
          className={styles.banner__messenger}
          paths={messengerPath}
        />
      </div>

      <div className={styles.banner__image}>
        <Image
          alt="Картинка баннера"
          layout="fill"
          objectFit="cover"
          src="/images/short/banner.jpg"
        />
      </div>
    </div>
  )
}
