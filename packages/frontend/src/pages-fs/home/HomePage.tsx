import { useEffect, useRef, useState } from 'react'

import { Layout } from '@shared/ui'
import { Header } from '@widgets/header'
import { Footer } from '@widgets/footer'
import { useSticky } from '@shared/lib'

const titlePage =
  'Аренда квартир на любой срок. Обслужим, проконтролируем, решим бытовые вопросы'

export const HomePage = (): JSX.Element => {
  const [offsetTop, setOffsetTop] = useState(0)
  const anchorRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    setOffsetTop((anchorRef.current?.offsetTop ?? 0) + 100)
  }, [])

  const { showHeader } = useSticky(offsetTop)

  return (
    <Layout
      footer={<Footer />}
      header={<Header showHeader={showHeader} />}
      titlePage={titlePage}
    >
      <main>
        <section className="container" style={{ height: '600px' }}>
          <h1>Главная</h1>
        </section>
        <div ref={anchorRef} />
        <section className="container" style={{ height: '2000px' }} />
      </main>
    </Layout>
  )
}
