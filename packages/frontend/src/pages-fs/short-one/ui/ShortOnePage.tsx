import classNames from 'classnames'
import { useRef } from 'react'

import { Breadcrumbs, Layout } from '@shared/ui'
import { IApartmentItem, IApartmentsOneResponse } from '@shared/api'
import { roundInteger, useToggle } from '@shared/lib'
import { pathPages } from '@shared/config'

import { MapOne } from '@features/maps/map-one'

import { Header } from '@widgets/header'
import { Footer } from '@widgets/footer'
import { ShortInfo } from '@widgets/short-info'
import { Feedback } from '@widgets/feedback'
import { GallerySlider } from '@widgets/gallery-slider'
import { GalleryModal } from '@widgets/modals/info/gallery-modal'
import { ShortDescriptionApartment } from '@widgets/short-description-apartment'
import { ShortServices } from '@widgets/short-services'
import { ShortRules } from '@widgets/short-rules'
import { ShortRating } from '@widgets/short-rating'
import {
  CollectionApartments,
  CollectionCardShort,
} from '@widgets/collection-apartments'

import { mockFeedbacks } from '../model/mockFeedbacks'
import { getBreadcrumbs } from '../lib/helpers'
import { imagesTest } from '../model/mock'

import styles from './ShortOnePage.module.scss'
import { ShareLinkModal } from '@widgets/modals/info/share-link-modal'

const titlePage = 'Снять хорошую квартиру на сутки'

interface IProps {
  apartment: IApartmentItem
  collections: IApartmentsOneResponse['collections']
}

export const ShortOnePage = ({
  apartment,
  collections,
}: IProps): JSX.Element => {
  const [isGalleryOpen, setIsGalleryOpen] = useToggle()
  const [isOpenShare, setIsOpenShare] = useToggle()

  const mapRef = useRef<HTMLDivElement>(null)

  const shortInfo = {
    address: {
      street: apartment.name,
      metro: apartment.metroStations,
      timeFoot: apartment.metroAvailabilityByFoot,
      timeVehicle: apartment.metroAvailabilityByVehicle,
    },
    price: {
      price: +apartment.price,
      deposit: roundInteger(+apartment.securityDepositShort),
    },
    parameters: {
      guests: {
        maxAdults: apartment.maxAdults,
        maxChildren: apartment.maxChildren,
      },
      sleepingPlaces: apartment.sleepingPlaces,
      roomsNum: apartment.roomsNum,
      area: roundInteger(+apartment.area),
    },
    gallery: {
      roomsNum: apartment.roomsNum,
      area: roundInteger(Number(apartment.area)),
      storey: apartment.storey,
      totalStoreys: apartment.totalStoreys,
      pricePerMonth: apartment.pricePerMonth,
    },
  }

  const ratingInfo = {
    quantityReviews: apartment.reviews.length,
    rating: apartment.reviewsStatistics,
  }

  const scrollToMap = (): void => {
    return mapRef.current?.scrollIntoView({
      behavior: 'smooth',
      block: 'center',
    })
  }

  const getPath = (latinName: string): string => {
    return `/${pathPages.short}/${latinName}`
  }

  return (
    <Layout footer={<Footer />} header={<Header />} titlePage={titlePage}>
      <main>
        <Breadcrumbs
          {...getBreadcrumbs(apartment.name)}
          className={classNames(styles.breadcrumbs, 'container')}
        />

        <div className={classNames(styles.wrapper, 'container')}>
          <section className={styles.gallery}>
            <h2 className="visually-hidden">Галерея</h2>
            <GallerySlider
              images={imagesTest}
              onCloseGalleryModal={() => setIsGalleryOpen()}
            />
          </section>

          <ShortInfo
            address={shortInfo.address}
            className={styles.info}
            price={shortInfo.price}
            onClickMap={scrollToMap}
            onClickShareLink={setIsOpenShare}
          />

          <section className={styles['description-wrapper']}>
            <ShortDescriptionApartment
              parameters={shortInfo.parameters}
              text={apartment.description}
            />
          </section>

          <section className={styles.services}>
            <h2 className="visually-hidden">Услуги</h2>
            <ShortServices />
          </section>

          <section ref={mapRef} className={styles.map}>
            <h2 className={styles.map__title}>Расположение квартиры</h2>
            <p className={styles.map__text}>{apartment.name}</p>
            <MapOne />
          </section>

          <section className={styles.rules}>
            <h2 className={styles.rules__heading}>Правила квартиры</h2>
            <ShortRules
              checkInEnd={apartment.checkInEnd}
              checkInStart={apartment.checkInStart}
              checkOutEnd={apartment.checkOutEnd}
            />
          </section>

          <section className={styles.rating}>
            <ShortRating {...ratingInfo} />
          </section>
        </div>

        <section className="container">
          <Feedback feedbacks={mockFeedbacks} />
        </section>

        <section className={styles.collections}>
          <CollectionApartments
            cards={collections.popular}
            titleCollection="popular"
          >
            {(apartment) => (
              <CollectionCardShort
                apartmentData={apartment}
                path={getPath(apartment.latinName)}
              />
            )}
          </CollectionApartments>
        </section>
      </main>

      {/* TODO: После календаря исправить renderApartmentPrewiew должны быть другие данные */}
      <GalleryModal
        apartmentInfo={shortInfo.gallery}
        images={imagesTest}
        isOpen={isGalleryOpen}
        onClickBooking={() => {
          setIsGalleryOpen()
        }}
        onClose={() => setIsGalleryOpen()}
      />

      <ShareLinkModal isOpen={isOpenShare} onClose={setIsOpenShare} />
    </Layout>
  )
}
