export interface IBreadcrumbs {
  backPath: {
    path: string
    label: string
  }
  currentPathLabel: string
}
