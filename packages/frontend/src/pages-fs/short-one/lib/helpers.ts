import { IBreadcrumbs } from './types'

const getBreadcrumbs = (currentPathLabel: string): IBreadcrumbs => ({
  backPath: {
    path: '/short',
    label: 'Каталог',
  },
  currentPathLabel,
})

export { getBreadcrumbs }
