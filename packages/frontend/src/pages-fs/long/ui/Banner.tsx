import { useState } from 'react'
import classNames from 'classnames'

import { ModalBase } from '@shared/ui'

import { CallbackForm } from '@features/callback-form'

import { PersonalSearchBanner } from '@widgets/personal-search-banner'
import { PersonalSearchModal } from '@widgets/modals/info/personal-search-modal'
import { ErrorCallbackPopup } from '@widgets/modals/errors/callback-status-error'
import { SuccessCallbackPopup } from '@widgets/modals/success/callback-status-success'

import styles from './Banner.module.scss'

interface IPropsBanner {
  paths: {
    telegram: string
    whatsapp: string
  }
  classProps?: string
}

export const Banner = ({ paths, classProps }: IPropsBanner): JSX.Element => {
  const [isModalShown, setIsModalShown] = useState(false)
  const [isFormShown, setIsFormShown] = useState(false)
  const [statusCallback, setStatusCallback] = useState<
    'success' | 'error' | 'initial'
  >('initial')

  return (
    <>
      <PersonalSearchBanner
        classProps={classNames(classProps, styles.banner)}
        grade="secondary"
        onClick={() => setIsModalShown(true)}
      />
      <PersonalSearchModal
        isOpen={isModalShown}
        showForm={() => {
          setIsModalShown(false)
          setIsFormShown(true)
        }}
        onClose={() => setIsModalShown(false)}
      />
      <ModalBase isOpen={isFormShown} onClose={() => setIsFormShown(false)}>
        <CallbackForm
          onSubmit={() => {
            setIsFormShown(false)
            setStatusCallback('success')
          }}
        />
      </ModalBase>
      <SuccessCallbackPopup
        isOpen={statusCallback === 'success'}
        paths={paths}
        onClose={() => {
          setStatusCallback('initial')
        }}
      />
      <ErrorCallbackPopup
        isOpen={statusCallback === 'error'}
        paths={paths}
        onClose={() => {
          setStatusCallback('initial')
        }}
      />
    </>
  )
}
