import { useEffect, useRef, useState } from 'react'
import classNames from 'classnames'

import { useSticky, stringUtils } from '@shared/lib'
import { Breadcrumbs, Layout, Preloader } from '@shared/ui'
import {
  IApartmentItem,
  IMetroStation,
  IApartmentsListResponse,
} from '@shared/api'
import { messengers, pathPages } from '@shared/config' // Я это могу считать с урла. Зачем мне это?

import { Header } from '@widgets/header'
import { Footer } from '@widgets/footer'
import {
  CollectionApartments,
  CollectionCardLong,
} from '@widgets/collection-apartments'
import { FilterLong } from '@widgets/filter-long'

import { DISTANCE_BOTTOM_FILTER } from '../config/const'
import { ApartmentCardLong } from './ApartmentCardLong'
import { Banner } from './Banner'
import { useFilter } from '../model/useFilter'
import { storageFilter } from '../lib/storageFilter'
import { useApartments } from '../model/useApartments'
import { useInitFilters } from '../model/useInitFilters'

import styles from './LongPage.module.scss'

const titlePage = 'Снять хорошую квартиру от 6 месяцев &#128156;'

interface IProps {
  data: IApartmentsListResponse
  metroList: IMetroStation[]
}

interface IBreadcrumbs {
  backPath: {
    path: string
    label: string
  }
  currentPathLabel: string
}

// TODO: Состояние ошибки, заменить текст на нужную модалку. Preloader нужно доделать, разместить посередине экрана и навесить оверлей, нужно как-то заблокировать каталог и фильтры
export const LongPage = ({ data, metroList }: IProps): JSX.Element => {
  const [offsetTop, setOffsetTop] = useState(0)
  const anchorRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    setOffsetTop((anchorRef.current?.offsetTop ?? 0) + DISTANCE_BOTTOM_FILTER)
  }, [])

  const { isHeaderMode, showHeader } = useSticky(offsetTop)

  const getPath = (latinName: string): string => {
    return `/${pathPages.long}/${stringUtils.getSlug(latinName)}`
  }

  const getBreadcrumbs = (): IBreadcrumbs => ({
    backPath: {
      path: '/',
      label: 'Главная',
    },
    currentPathLabel: 'Долгосрочная аренда',
  })

  const messengerPath = {
    telegram: messengers.long.telegram,
    whatsapp: messengers.long.whatsApp,
  }

  const { filtersInit } = useInitFilters({ storage: storageFilter })
  const { filters, onResetFilter, onSubmitFilter } = useFilter({
    filtersInit,
    storage: storageFilter,
  })

  const { apartments, collections, isLoading } = useApartments({
    filters,
    apartmentsInit: data.data,
    collectionsInit: data.collections,
  })

  return (
    <>
      {isLoading && <Preloader />}
      <Layout
        footer={<Footer />}
        header={<Header showHeader={showHeader} />}
        titlePage={titlePage}
      >
        <main>
          <section>
            <Breadcrumbs
              {...getBreadcrumbs()}
              className={classNames(styles.breadcrumbs, 'container')}
            />
            <FilterLong
              filters={filters}
              isHeaderMode={isHeaderMode}
              metroList={metroList}
              quantityApartment={5625462}
              showHeader={showHeader}
              onReset={onResetFilter}
              onSubmit={onSubmitFilter}
            />
            <div ref={anchorRef} />

            <ul className={classNames(styles['card-list'], 'container')}>
              {apartments.slice(0, 10).map((el: IApartmentItem) => (
                <li key={el.id}>
                  <ApartmentCardLong
                    apartmentData={el}
                    path={getPath(el.latinName)}
                  />
                </li>
              ))}
            </ul>

            <Banner paths={messengerPath} />

            <ul className={classNames(styles['card-list'], 'container')}>
              {apartments.slice(10, 20).map((el: IApartmentItem) => (
                <li key={el.id}>
                  <ApartmentCardLong
                    apartmentData={el}
                    path={getPath(el.latinName)}
                  />
                </li>
              ))}
            </ul>

            {collections != null && (
              <CollectionApartments
                cards={collections.inCityCenter}
                className={styles.collections}
                titleCollection="inCityCenter"
              >
                {(apartment) => (
                  <CollectionCardLong
                    apartmentData={apartment}
                    path={getPath(apartment.latinName)}
                  />
                )}
              </CollectionApartments>
            )}

            <ul className={classNames(styles['card-list'], 'container')}>
              {apartments.slice(20, 30).map((el: IApartmentItem) => (
                <li key={el.id}>
                  <ApartmentCardLong
                    apartmentData={el}
                    path={getPath(el.latinName)}
                  />
                </li>
              ))}
            </ul>
          </section>
        </main>
      </Layout>
    </>
  )
}
