import classNames from 'classnames'

import { IApartmentItem } from '@shared/api'
import { roundInteger } from '@shared/lib'
import { DetailsLong, PriceLong } from '@shared/ui'

import { ApartmentCard } from '@entities/cards/ApartmentCard'
import { MetroInfo } from '@entities/MetroInfo'

import { SwiperCard } from './SwiperCard'

import styles from './ApartmentCardLong.module.scss'

export const ApartmentCardLong = ({
  apartmentData,
  path,
}: {
  apartmentData: IApartmentItem
  path: string
}): JSX.Element => {
  const area = roundInteger(Number(apartmentData.area))

  return (
    <ApartmentCard
      {...apartmentData}
      isDeposit
      classCard={classNames(styles.card)}
      detailsInfo={DetailsLong({
        roomsNum: apartmentData.roomsNum,
        area,
        storey: apartmentData.storey,
        totalStoreys: apartmentData.totalStoreys,
      })}
      media={
        <SwiperCard classBtnHover={classNames(styles['card__btn-hover'])} />
      }
      metroInfo={
        <MetroInfo
          metroStations={apartmentData.metroStations}
          timeFoot={apartmentData.metroAvailabilityByFoot}
          timeVehicle={apartmentData.metroAvailabilityByVehicle}
        />
      }
      pathPage={path}
      priceInfo={PriceLong(apartmentData.pricePerMonth)}
    />
  )
}
