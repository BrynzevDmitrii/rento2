import {
  getItemLocalStorage,
  removeItemLocalStorage,
  setItemLocalStorage,
} from '@shared/lib'

import { KEY_FILTER_LOCAL_STORAGE } from '@widgets/filter-long'

import { IStorageFilter } from './types'

const storage = (): IStorageFilter => {
  const removeStorage = (): void => {
    removeItemLocalStorage(KEY_FILTER_LOCAL_STORAGE)
  }

  const setStorage = (data: string): void => {
    setItemLocalStorage(KEY_FILTER_LOCAL_STORAGE, data)
  }

  const getStorage = (): string | undefined => {
    return getItemLocalStorage(KEY_FILTER_LOCAL_STORAGE)
  }

  return {
    removeStorage,
    setStorage,
    getStorage,
  }
}

export const storageFilter = storage()
