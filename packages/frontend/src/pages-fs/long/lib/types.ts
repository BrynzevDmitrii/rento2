interface IStorageFilter {
  removeStorage: () => void
  setStorage: (data: string) => void
  getStorage: () => string | undefined
}

export type { IStorageFilter }
