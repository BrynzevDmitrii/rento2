import { useRouter } from 'next/router'
import { Dispatch, SetStateAction, useEffect, useState } from 'react'

import { IRequestFilterLong } from '@shared/api'

import { defaultFilters, getUrlQuery } from '@widgets/filter-long'

import { IStorageFilter } from '../lib/types'

interface IParams {
  filtersInit: IRequestFilterLong | null
  storage: IStorageFilter
}

// TODO: Может это заменить на контекст, т.к. просто вызываю хук на странице, а сами фильтры мне глубже нужны и в нескольких компонентах.
export const useFilter = ({
  filtersInit,
  storage,
}: IParams): {
  filters: IRequestFilterLong | null
  setFilters: Dispatch<SetStateAction<IRequestFilterLong | null>>
  onResetFilter: () => void
  onSubmitFilter: (filters: IRequestFilterLong) => void
} => {
  const [filters, setFilters] = useState<IRequestFilterLong | null>(filtersInit)
  const router = useRouter()

  // Из-за гидратации разметка, которая приходит от сервера и разметка, которая рисуется на начальном рендеринге на клиенте должна совпадать. Изначально на сервере может не быть данных о начальных фильтрах (например у пользователя есть сохраненные фильтры в локал сторож, которые потом восстанавливаются на клиенте). Из-за этого начальные фильтры для этой части кода могут измениться
  useEffect(() => {
    setFilters(filtersInit)
  }, [filtersInit])

  const onResetFilter = (): void => {
    storage.removeStorage()
    // https://github.com/vercel/next.js/blob/canary/docs/api-reference/next/router.md#potential-eslint-errors
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    router.replace({ query: '' }, undefined, {
      shallow: true,
    })

    setFilters(defaultFilters)
  }

  const onSubmitFilter = (filters: IRequestFilterLong): void => {
    storage.setStorage(getUrlQuery(filters))
    // https://github.com/vercel/next.js/blob/canary/docs/api-reference/next/router.md#potential-eslint-errors
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    router.replace({ query: getUrlQuery(filters) }, undefined, {
      shallow: true,
    })

    setFilters(filters)
  }

  return {
    filters,
    setFilters,
    onResetFilter,
    onSubmitFilter,
  }
}
