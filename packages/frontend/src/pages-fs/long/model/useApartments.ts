import { useEffect, useState } from 'react'

import {
  getApartmentsList,
  IApartmentItem,
  IApartmentsListParams,
  ICollections,
  IRequestFilterLong,
  TypeApartments,
} from '@shared/api'

interface IParams {
  filters: IRequestFilterLong | null
  apartmentsInit: IApartmentItem[]
  collectionsInit: ICollections
}

// TODO: Может вынести в контекст?
export const useApartments = ({
  filters,
  apartmentsInit,
  collectionsInit,
}: IParams): {
  apartments: IApartmentItem[]
  collections: ICollections
  isLoading: boolean
  error: string
} => {
  const [apartments, setApartments] = useState(apartmentsInit)
  const [collections, setCollections] = useState<ICollections>(collectionsInit)
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState('')

  useEffect(() => {
    let ignore = false

    if (filters !== null) {
      setIsLoading(true)
      const params: IApartmentsListParams = {
        apartmentRentType: TypeApartments.long,
        fields: filters,
      }

      getApartmentsList(params)
        .then(({ data }) => {
          if (!ignore) {
            setApartments(data.data)
            setCollections(data.collections)
          }
        })
        .catch((e) => {
          setError(e)
        })
        .finally(() => {
          setIsLoading(false)
        })
    }
    return () => {
      ignore = true
    }
  }, [filters, setApartments, setCollections])

  return {
    apartments,
    collections,
    isLoading,
    error,
  }
}
