import { useRouter } from 'next/router'
import { extract, parse } from 'query-string'
import { useEffect, useState } from 'react'

import { IRequestFilterLong } from '@shared/api'

import { defaultFilters, parseUrlQuery } from '@widgets/filter-long'

import { IStorageFilter } from '../lib/types'

export const useInitFilters = ({
  storage,
}: {
  storage: IStorageFilter
}): {
  filtersInit: IRequestFilterLong | null
} => {
  const [filtersInit, setFiltersInit] = useState<IRequestFilterLong | null>(
    null
  )
  const router = useRouter()

  useEffect(() => {
    const queryStorage = storage.getStorage()
    const queryString = extract(router.asPath)

    if (queryString === '' && queryStorage === undefined) {
      setFiltersInit(defaultFilters)
    }

    if (queryString === '' && queryStorage !== undefined) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      router.replace({ query: queryStorage }, undefined, {
        shallow: true,
      })

      setFiltersInit(parseUrlQuery(parse(queryStorage)))
    }

    if (queryString !== '') {
      storage.setStorage(queryString)
      setFiltersInit(parseUrlQuery(parse(queryString)))
    }

    // Здесь логика должна быть выполнена 1 раз при монтировании компонента
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return { filtersInit }
}
