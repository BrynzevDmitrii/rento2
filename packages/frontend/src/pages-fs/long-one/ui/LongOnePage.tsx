import { useRef, useState } from 'react'
import classNames from 'classnames'

import { Breadcrumbs, Layout, ModalBase } from '@shared/ui'
import { IApartmentItem, IApartmentsOneResponse } from '@shared/api'
import { roundInteger, useToggle } from '@shared/lib'
import { messengers, pathPages } from '@shared/config'

import { CallbackForm } from '@features/callback-form'
import { ChatButtons } from '@features/messengers/chat-buttons'
import { MapOne } from '@features/maps/map-one'
import { LongBookingModal } from '@features/modals/long-booking-modal'

import { Header } from '@widgets/header'
import { Footer } from '@widgets/footer'
import { LongInfo } from '@widgets/long-info'
import { PersonalSearchBanner } from '@widgets/personal-search-banner'
import { PersonalSearchModal } from '@widgets/modals/info/personal-search-modal'
import { PersonalSearchForm } from '@widgets/personal-search-form'
import { ErrorCallbackPopup } from '@widgets/modals/errors/callback-status-error'
import { SuccessCallbackPopup } from '@widgets/modals/success/callback-status-success'
import { GallerySlider } from '@widgets/gallery-slider'
import { LongDescriptionApartment } from '@widgets/long-description-apartment'
import { LongServices } from '@widgets/long-services'
import {
  CollectionApartments,
  CollectionCardLong,
} from '@widgets/collection-apartments'
import { GalleryModal } from '@widgets/modals/info/gallery-modal'
import { CallApartmentOneModal } from '@widgets/modals/info/call-apartment-one'
import { ShareLinkModal } from '@widgets/modals/info/share-link-modal'

import { getBreadcrumbs } from '../lib/helpers'
import { imagesTest } from '../model/mock'

import styles from './LongOnePage.module.scss'

const titlePage = 'Снять хорошую квартиру от 6 месяцев &#128156;'

interface IProps {
  apartment: IApartmentItem
  collections: IApartmentsOneResponse['collections']
}

export const LongOnePage = ({
  apartment,
  collections,
}: IProps): JSX.Element => {
  const [isModalShown, setIsModalShown] = useState(false)
  const [isFormShown, setIsFormShown] = useState(false)
  const [statusCallback, setStatusCallback] = useState<
    'success' | 'error' | 'initial'
  >('initial')

  const [isGalleryOpen, setIsGalleryOpen] = useState(false)
  const [isBookingModalOpen, setIsBookingModalOpen] = useState(false)
  const [isOpenCallMeApartmentOne, setIsOpenCallMeApartmentOne] =
    useState(false)
  const [isOpenShare, setIsOpenShare] = useToggle()

  const mapRef = useRef<HTMLDivElement>(null)

  const longInfo = {
    address: {
      street: apartment.name,
      metro: apartment.metroStations,
      timeFoot: apartment.metroAvailabilityByFoot,
      timeVehicle: apartment.metroAvailabilityByVehicle,
    },
    parameters: {
      roomsNum: apartment.roomsNum,
      area: roundInteger(Number(apartment.area)),
      storey: apartment.storey,
      totalStoreys: apartment.totalStoreys,
      pricePerMonth: apartment.pricePerMonth,
    },
    details: {
      communal: '',
      commission: roundInteger(Number(apartment.commission)),
      deposit: roundInteger(Number(apartment.securityDepositLong)),
    },
  }

  // TODO: Заменить imageSrc
  const apartmentBooking = {
    pricePerMonth: apartment.pricePerMonth,
    imageSrc: '/long-one/GallerySlider1.jpg',
    roomNum: apartment.roomsNum,
    area: roundInteger(Number(apartment.area)),
    storey: apartment.storey,
    totalStoreys: apartment.totalStoreys,
    street: apartment.name,
    metro: apartment.metroStations,
    metroAvailabilityByFoot: apartment.metroAvailabilityByFoot,
    metroAvailabilityByVehicle: apartment.metroAvailabilityByVehicle,
  }

  const messengerPath = {
    telegram: messengers.long.telegram,
    whatsapp: messengers.long.whatsApp,
  }

  const scrollToMap = (): void => {
    return mapRef.current?.scrollIntoView({
      behavior: 'smooth',
      block: 'center',
    })
  }

  const getPath = (latinName: string): string => {
    return `/${pathPages.long}/${latinName}`
  }

  return (
    <Layout footer={<Footer />} header={<Header />} titlePage={titlePage}>
      <main>
        <Breadcrumbs
          {...getBreadcrumbs(apartment.name)}
          className={classNames(styles.breadcrumbs, 'container')}
        />

        <div className={classNames(styles.wrapper, 'container')}>
          <section className={styles.gallery}>
            <h2 className="visually-hidden">Галерея</h2>
            <GallerySlider
              images={imagesTest}
              onCloseGalleryModal={() => setIsGalleryOpen(!isGalleryOpen)}
            />
          </section>

          <LongInfo
            address={longInfo.address}
            className={styles.info}
            details={longInfo.details}
            parameters={longInfo.parameters}
            onClickBooking={() => setIsBookingModalOpen(!isBookingModalOpen)}
            onClickCallMe={() =>
              setIsOpenCallMeApartmentOne(!isOpenCallMeApartmentOne)
            }
            onClickMap={scrollToMap}
            onClickShareLink={setIsOpenShare}
          />

          <section className={styles['description-wrapper']}>
            <h2 className="visually-hidden">Инфо</h2>

            <LongDescriptionApartment
              className={styles.description}
              text={apartment.description}
            />

            <ChatButtons
              className={styles.messenger}
              paths={messengerPath}
              text="Задать вопрос"
            />
          </section>

          <section className={styles.services}>
            <h2 className="visually-hidden">Услуги</h2>
            <LongServices />
          </section>

          <section ref={mapRef} className={styles.map}>
            <h2 className={styles.map__title}>Расположение квартиры</h2>
            <p className={styles.map__text}>{apartment.name}</p>
            <MapOne />
          </section>
        </div>
        <section className="container">
          <PersonalSearchBanner
            classProps={classNames(styles.banner)}
            grade="primary"
            onClick={() => setIsModalShown(true)}
          />
          <PersonalSearchModal
            isOpen={isModalShown}
            showForm={() => {
              setIsModalShown(false)
              setIsFormShown(true)
            }}
            onClose={() => setIsModalShown(false)}
          />
          <ModalBase isOpen={isFormShown} onClose={() => setIsFormShown(false)}>
            <CallbackForm
              onSubmit={() => {
                setIsFormShown(false)
                setStatusCallback('success')
              }}
            />
          </ModalBase>
        </section>

        <section>
          <CollectionApartments
            cards={collections.rentoChoose}
            className={styles.collections}
            titleCollection="rentoChoose"
          >
            {(apartment) => (
              <CollectionCardLong
                apartmentData={apartment}
                path={getPath(apartment.latinName)}
              />
            )}
          </CollectionApartments>
        </section>

        <section className={classNames(styles['search-form'], 'container')}>
          <PersonalSearchForm onSubmit={() => setStatusCallback('success')} />
        </section>
      </main>

      <SuccessCallbackPopup
        isOpen={statusCallback === 'success'}
        paths={messengerPath}
        onClose={() => {
          setStatusCallback('initial')
        }}
      />
      <ErrorCallbackPopup
        isOpen={statusCallback === 'error'}
        paths={messengerPath}
        onClose={() => {
          setStatusCallback('initial')
        }}
      />

      <LongBookingModal
        apartmentInfo={apartmentBooking}
        isOpen={isBookingModalOpen}
        onClose={() => setIsBookingModalOpen(!isBookingModalOpen)}
      />

      <GalleryModal
        apartmentInfo={longInfo.parameters}
        images={imagesTest}
        isOpen={isGalleryOpen}
        onClickBooking={() => {
          setIsGalleryOpen(!isGalleryOpen)
          setIsBookingModalOpen(!isBookingModalOpen)
        }}
        onClose={() => setIsGalleryOpen(!isGalleryOpen)}
      />

      <CallApartmentOneModal
        apartmentInfo={apartmentBooking}
        isOpen={isOpenCallMeApartmentOne}
        phone={messengers.long.phone}
        onClose={() => setIsOpenCallMeApartmentOne(!isOpenCallMeApartmentOne)}
      />

      <ShareLinkModal isOpen={isOpenShare} onClose={setIsOpenShare} />
    </Layout>
  )
}
