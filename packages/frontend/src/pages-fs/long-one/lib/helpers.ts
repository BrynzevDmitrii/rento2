import { IBreadcrumbs } from './types'

const getBreadcrumbs = (currentPathLabel: string): IBreadcrumbs => ({
  backPath: {
    path: '/long',
    label: 'Каталог',
  },
  currentPathLabel,
})

export { getBreadcrumbs }
