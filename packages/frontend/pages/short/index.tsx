import { GetServerSideProps } from 'next'

import {
  getApartmentsList,
  getMetroStationsList,
  IApartmentsListResponse,
  IMetroStationsListResponse,
  TypeApartments,
} from '@shared/api'

import { ShortPage } from '@pages-fs/short'

interface IProps {
  apartments: IApartmentsListResponse
  metroList: IMetroStationsListResponse
}

export default function Short({ apartments, metroList }: IProps): JSX.Element {
  return <ShortPage data={apartments} metroList={metroList.data} />
}

export const getServerSideProps: GetServerSideProps = async () => {
  const { data: apartments } = await getApartmentsList({
    apartmentRentType: TypeApartments.short,
    limit: 30,
  })
  const { data: metroList } = await getMetroStationsList()

  return { props: { apartments, metroList } }
}
