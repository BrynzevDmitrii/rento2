import { GetServerSideProps, GetStaticPropsContext } from 'next'
import { ParsedUrlQuery } from 'querystring'

import { getApartmentsOne, IApartmentsOneResponse } from '@shared/api'

import { ShortOnePage } from '@pages-fs/short-one'

interface IProps {
  apartment: IApartmentsOneResponse
}

export default function ShortOne({ apartment }: IProps): JSX.Element {
  return (
    <ShortOnePage
      apartment={apartment.data}
      collections={apartment.collections}
    />
  )
}

export const getServerSideProps: GetServerSideProps<IProps> = async ({
  params,
}: GetStaticPropsContext<ParsedUrlQuery>) => {
  if (params == null || typeof params['latinName'] !== 'string') {
    return {
      notFound: true,
    }
  }

  const { data: apartment } = await getApartmentsOne(params['latinName'])

  apartment.data.roomsNum =
    apartment.data.roomsNum === 'studio' ? 'студия' : apartment.data.roomsNum

  return { props: { apartment } }
}
