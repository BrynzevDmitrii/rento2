import { GetStaticProps } from 'next'

import {
  getApartmentsList,
  getMetroStationsList,
  IApartmentsListResponse,
  IMetroStationsListResponse,
  TypeApartments,
} from '@shared/api'

import { defaultFilters } from '@widgets/filter-long'

import { LongPage } from '@pages-fs/long'

interface IProps {
  apartments: IApartmentsListResponse
  metroList: IMetroStationsListResponse
}

// TODO: Можно использовать middleware для фильтрации урл https://github.com/vercel/examples/tree/main/edge-functions/query-params-filter
export default function Long({ apartments, metroList }: IProps): JSX.Element {
  return <LongPage data={apartments} metroList={metroList.data} />
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const { data: apartments } = await getApartmentsList({
      apartmentRentType: TypeApartments.long,
      fields: defaultFilters,
    })
    const { data: metroList } = await getMetroStationsList()

    return {
      props: {
        apartments,
        metroList,
      },
    }
  } catch {
    // TODO: обработать ошибку если бэк упал. Нужно бы показать страницу, но с модалкой, что что-то пошло не так
    return {
      notFound: true,
    }
  }
}
