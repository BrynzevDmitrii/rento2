import { Admin, Resource } from 'react-admin'
import restProvider from './common/dataProvider'
import PostIcon from '@material-ui/icons/Book'
import BedIcon from '@material-ui/icons/KingBed'
import AccommodationsIcon from '@material-ui/icons/Apartment'
import RateReview from '@material-ui/icons/RateReview'
import SubwayIcon from '@material-ui/icons/Subway'
import BuildingTypeIcon from '@material-ui/icons/HomeWork'

import { ApartmentList, ApartmentCreate, ApartmentEdit } from './components/Apartment'
import { authProvider } from 'common'
import LoginPage from 'components/LoginPage/LoginPage'
import { httpClient } from 'common/httpClient'
import { SleepingPlacesCreate, SleepingPlacesEdit, SleepingPlacesList } from 'components/SleepingPlaces'
import polyglotI18nProvider from 'ra-i18n-polyglot'
import russianMessages from 'ra-language-russian'
import { ReviewCreate, ReviewEdit, ReviewList } from 'components/Review'
import { MetroLineCreate, MetroLineEdit, MetroLineList } from 'components/MetroLine'
import { MetroStationCreate, MetroStationEdit, MetroStationList } from 'components/MetroStation'
import { AccommodationsCreate, AccommodationsEdit, AccommodationsList } from 'components/Accommodation'
import { BuildingTypeCreate, BuildingTypeEdit, BuildingTypeList } from 'components/BuildingType'

const i18nProvider = polyglotI18nProvider(() => russianMessages, 'ru')
const dataProvider = restProvider(String(process.env['REACT_APP_SERVER_URL']), httpClient)

const App = (): JSX.Element => (
  <Admin authProvider={ authProvider }
    dataProvider={ dataProvider }
    i18nProvider={i18nProvider}
    loginPage={ LoginPage }
  >
    <Resource
      create={ApartmentCreate}
      edit={ApartmentEdit}
      icon={PostIcon}
      list={ApartmentList}
      name='apartments'
    />

    <Resource
      create={BuildingTypeCreate}
      edit={BuildingTypeEdit}
      icon={BuildingTypeIcon}
      list={BuildingTypeList}
      name='building-types'
    />

    <Resource
      create={MetroLineCreate}
      edit={MetroLineEdit}
      icon={SubwayIcon}
      list={MetroLineList}
      name='metro-lines'
    />

    <Resource
      create={MetroStationCreate}
      edit={MetroStationEdit}
      icon={SubwayIcon}
      list={MetroStationList}
      name='metro-stations'
    />

    <Resource
      create={ReviewCreate}
      edit={ReviewEdit}
      icon={RateReview}
      list={ReviewList}
      name='reviews'
    />

    <Resource
      create={AccommodationsCreate}
      edit={AccommodationsEdit}
      icon={AccommodationsIcon}
      list={AccommodationsList}
      name='accommodations'
    />

    <Resource
      create={SleepingPlacesCreate}
      edit={SleepingPlacesEdit}
      icon={BedIcon}
      list={SleepingPlacesList}
      name='sleeping-places'
    />

    <Resource
      name='photos'
    />
  </Admin>
)

export default App
