import { FC } from 'react'
import {
  Edit,
  SimpleForm,
  TextInput,
  ListProps,
  NumberInput,
  BooleanInput,
  required
} from 'react-admin'

const ReviewEdit: FC<ListProps> = props => (
  <Edit { ...props }>
    <SimpleForm>
      <BooleanInput
        label='Одобрено'
        source='isApproved'
        validate={[required()]}
      />

      <TextInput label='Автор'
        source='author'
        validate={[required()]}
      />

      <TextInput multiline
        label='Комментарий'
        source='comment'
        validate={[required()]}
      />

      <NumberInput label='Ремонт'
        max={5}
        min={0}
        source='repairs'
        validate={[required()]}
      />

      <NumberInput label='Чистота'
        max={5}
        min={0}
        source='purity'
        validate={[required()]}
      />

      <NumberInput label='Локация'
        max={5}
        min={0}
        source='location'
        validate={[required()]}
      />
      <NumberInput label='Цена-качество'
        max={5}
        min={0}
        source='priceQuality'
        validate={[required()]}
      />
    </SimpleForm>
  </Edit>
)

export default ReviewEdit
