import { FC, useEffect, useState } from 'react'
import {
  Record,
  Create,
  TextInput,
  ListProps,
  NumberInput,
  SimpleForm,
  BooleanInput,
  useDataProvider,
  SelectInput,
  required
} from 'react-admin'

const ReviewCreate: FC<ListProps> = props => {
  const dataProvider = useDataProvider()
  const [apartments, setApartments] = useState<Record[]>([])

  useEffect(() => {
    dataProvider.getList('apartments', {
      pagination: { page: 1, perPage: 20 },
      sort: { field: 'name', order: 'asc' },
      filter: {}
    }).then(({ data }) => setApartments(data))
  }, [dataProvider])

  return (
    <Create { ...props }>
      <SimpleForm>
        <SelectInput
          choices={apartments.map(item => ({ id: item, name: item['name'] }))}
          label="Аппартаменты"
          source='apartments'
          validate={[required()]}
        />
        <BooleanInput
          label='Одобрено'
          source='isApproved'
          validate={[required()]}
        />

        <TextInput label='Автор'
          source='author'
          validate={[required()]}
        />

        <TextInput multiline
          label='Комментарий'
          source='comment'
          validate={[required()]}
        />

        <NumberInput label='Ремонт'
          max={5}
          min={0}
          source='repairs'
          validate={[required()]}
        />

        <NumberInput label='Чистота'
          max={5}
          min={0}
          source='purity'
          validate={[required()]}
        />

        <NumberInput label='Локация'
          max={5}
          min={0}
          source='location'
          validate={[required()]}
        />
        <NumberInput label='Цена-качество'
          max={5}
          min={0}
          source='priceQuality'
          validate={[required()]}
        />
      </SimpleForm>
    </Create>
  )
}

export default ReviewCreate
