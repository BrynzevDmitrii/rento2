import { FC } from 'react'
import { List, Datagrid, TextField, ListProps, EditButton, DateField, CreateButton, TopToolbar, DeleteButton, FunctionField } from 'react-admin'

const Toolbar = (): JSX.Element => (
  <TopToolbar>
    <CreateButton />
    <DeleteButton />
  </TopToolbar>
)

const MetroLineList: FC<ListProps> = (props) => {
  return (
    <List actions={ <Toolbar /> }
      { ...props }
    >
      <Datagrid>
        <TextField source='id' />
        <TextField label='Название ветки'
          source='name'
        />

        <FunctionField
          label="Цвет"
          render={(record: any) =>
            (<div style={{
              width: '32px',
              height: '32px',
              background: record.color,
              border: '#000'
            }}
            />) }
        />
        <DateField showTime
          label='Дата'
          source='createdAt'
        />
        +
        {' '}
        <EditButton />
      </Datagrid>
    </List>
  )
}

export default MetroLineList
