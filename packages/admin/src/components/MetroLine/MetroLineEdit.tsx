import { FC, useState } from 'react'
import {
  Edit,
  SimpleForm,
  TextInput,
  ListProps,
  required
} from 'react-admin'

const MetroLineEdit: FC<ListProps> = props => {
  const [color, setColor] = useState('#000000')

  return (
    <Edit { ...props }>
      <SimpleForm>
        <TextInput label='Название ветки'
          source='name'
          validate={[required()]}
        />
        <TextInput label='Цвет'
          placeholder='#000000'
          source='color'
          validate={[required()]}
          onChange={(e) => setColor(e.target.value)}
        />
        <div style={{
          width: '32px',
          height: '32px',
          background: color,
          border: '#000'
        }}
        />
      </SimpleForm>
    </Edit>
  )
}

export default MetroLineEdit
