import Grid from '@mui/material/Grid'
import { FC, useEffect, useState } from 'react'
import './style/filepond-styling.sass'
import {
  Edit,
  SimpleForm,
  TextInput,
  ListProps,
  NumberInput,
  BooleanInput,
  SelectInput,
  DateTimeInput,
  useDataProvider,
  SelectArrayInput,
  FormDataConsumer,
  required
} from 'react-admin'

import { FilePond } from 'react-filepond'
import { FilePondFile } from 'filepond'
import { AdminDistrictsOfMoscow } from 'common/enums/AdminDistrictsOfMoscow'
import Term from 'common/enums/Term'

const ApartmentEdit: FC<ListProps> = props => {
  const dataProvider = useDataProvider()

  const [metroStations, setMetroStations] = useState<any[]>([])
  const [accommodations, setAccommodations] = useState<any[]>([])
  const [sleepingPlaces, setSleepingPlaces] = useState<any[]>([])
  const [buildingTypes, setBuildingTypes] = useState<any[]>([])
  const [files, setFiles] = useState<any[]>([])

  useEffect(() => {
    dataProvider.getList('metro-stations', {
      pagination: { page: 1, perPage: 20 },
      sort: { field: 'name', order: 'asc' },
      filter: {}
    }).then(({ data }) => setMetroStations(data))

    dataProvider.getList('accommodations', {
      pagination: { page: 1, perPage: 20 },
      sort: { field: 'name', order: 'asc' },
      filter: {}
    }).then(({ data }) => setAccommodations(data))

    dataProvider.getList('sleeping-places', {
      pagination: { page: 1, perPage: 20 },
      sort: { field: 'name', order: 'asc' },
      filter: {}
    }).then(({ data }) => setSleepingPlaces(data))

    dataProvider.getList('building-types', {
      pagination: { page: 1, perPage: 20 },
      sort: { field: 'name', order: 'asc' },
      filter: {}
    }).then(({ data }) => setBuildingTypes(data))
  }, [dataProvider])

  const handleUploadFile = async (file: FilePondFile, apartmentId: number): Promise<void> => {
    const result = (dataProvider as any).uploadPhoto({
      file: file.source,
      apartmentId
    })

    file.setMetadata('id', result.id)
    setFiles([...files, file])
  }

  const handleDeleteFile = async (file: FilePondFile): Promise<void> => {
    dataProvider.delete('photos', {
      id: file.getMetadata()?.id
    })
  }

  return (
    <Edit { ...props }>
      <SimpleForm>

        <Grid container
          gap={5}
          justifyContent="center"
        >
          <Grid item
            md={4}
          >
            <BooleanInput label="Показывать объявление"
              source="isActive"
              validate={[required()]}
            />
            <BooleanInput label="Можно с детьми"
              source="childrenAllowed"
              validate={[required()]}
            />
            <BooleanInput label="Можно с животными"
              source="petsAllowed"
              validate={[required()]}
            />

            <DateTimeInput fullWidth
              label='Время начала заселения'
              source='checkInStart'
              validate={[required()]}
            />
            <DateTimeInput fullWidth
              label='Время конца заселения'
              source='checkInEnd'
              validate={[required()]}
            />
            <DateTimeInput fullWidth
              label='Время конца выезда'
              source='checkOutEnd'
              validate={[required()]}
            />
            <SelectInput fullWidth
              choices={Object.entries(Term).map(([id, name]) => ({ id, name }))}
              label='Тип'
              source='type'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Не больше Н взрослых'
              source='maxAdults'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Не больше Н детей'
              source='maxChildren'
              validate={[required()]}
            />

            <hr />
            <NumberInput fullWidth
              label='Цена'
              source='price'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Цена в месяц'
              source='pricePerMonth'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Скидка'
              source='discount'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Комиссия'
              source='commission'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Размер коммунальных платежей'
              source='utilityBills'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Размер депозита на краткосрок'
              source='securityDepositShort'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Размер депозита на долгосрок'
              source='securityDepositLong'
              validate={[required()]}
            />

            <hr />

            <NumberInput fullWidth
              label='Bnovo id'
              source='bnovoId'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='X координата'
              source='geoCoordinateX'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Y координата'
              source='geoCoordinateY'
              validate={[required()]}
            />
            <hr />

            <SelectArrayInput
              fullWidth
              choices={metroStations.map(item => ({ id: item, name: item.name }))}
              label='Метро'
              source='metroStations'
              validate={[required()]}
            />
            <SelectArrayInput
              fullWidth
              choices={accommodations.map(item => ({ id: item, name: item.name }))}
              label='Удобства'
              source='accomodations'
              validate={[required()]}
            />
            <SelectArrayInput
              fullWidth
              choices={sleepingPlaces.map(item => ({ id: { ...item, number: 1 }, name: item.name }))}
              label='Спальные места'
              source='sleepingPlaces'
              validate={[required()]}
            />
            <SelectArrayInput
              fullWidth
              choices={buildingTypes.map(item => ({ id: { ...item, number: 1 }, name: item.name }))}
              label='Тип здания'
              source='buildingTypes'
              validate={[required()]}
            />
          </Grid>
          <Grid item
            md={4}
          >
            <BooleanInput label="Курение разрешено"
              source="smokingAllowed"
              validate={[required()]}
            />
            <BooleanInput label="Вечеринки разрешены"
              source="partyingAllowed"
              validate={[required()]}
            />
            <TextInput fullWidth
              label='Имя'
              source='name'
              validate={[required()]}
            />
            <TextInput fullWidth
              label='Имя на английском'
              source='latinName'
              validate={[required()]}
            />
            <TextInput fullWidth
              multiline
              label='Описание'
              source='description'
              validate={[required()]}
            />

            <hr />

            <NumberInput fullWidth
              label='Число комнат'
              source='roomsNum'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Этаж'
              source='storey'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Всего этажей в доме'
              source='totalStoreys'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Площадь'
              source='area'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Размер кухни'
              source='kitchenArea'
              validate={[required()]}
            />

            <hr />

            <NumberInput fullWidth
              label='Расстояние до центра'
              source='distanceFromCenter'
              validate={[required()]}
            />
            <SelectInput
              fullWidth
              choices={Object.entries(AdminDistrictsOfMoscow).map(([id, name]) => ({ id, name }))}
              label='Административная единица'
              source='admArea'
              validate={[required()]}
            />
            <TextInput fullWidth
              label='Район'
              source='district'
              validate={[required()]}
            />
            <TextInput fullWidth
              label='Место встречи'
              source='sellingPoint'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Время до метро пешком'
              source='metroAvailabilityByFoot'
              validate={[required()]}
            />
            <NumberInput fullWidth
              label='Время до метро на машине'
              source='metroAvailabilityByVehicle'
              validate={[required()]}
            />
          </Grid>
        </Grid>

        <Grid container
          justifyContent="center"
        >
          <Grid item
            md={8}
          >
            <FormDataConsumer>
              {({ formData }) => (
                <FilePond
                  allowMultiple={true}
                  files={[
                    ...formData.photos.map((v: { link: string, id: string }) => ({
                      source: v.link,
                      options: {
                        metadata: { id: v.id }
                      }
                    })),
                    ...files
                  ]}
                  imagePreviewHeight={180}
                  labelIdle='Бросьте или выберите фото'
                  maxFiles={20}
                  onaddfilestart={async (file) => handleUploadFile(file, formData.id)}
                  onremovefile={async (_, file) => handleDeleteFile(file)}
                  onupdatefiles={() => {}}
                />
              )}
            </FormDataConsumer>
          </Grid>
        </Grid>

      </SimpleForm>
    </Edit>
  )
}

export default ApartmentEdit
