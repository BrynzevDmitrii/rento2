import React from 'react'
import {
  Edit,
  SimpleForm,
  TextInput,
  ListProps,
  required
} from 'react-admin'

const AccommodationEdit: React.FC<ListProps> = props => (
  <Edit { ...props }>
    <SimpleForm>
      <TextInput label='Название'
        source='name'
        validate={[required()]}
      />
    </SimpleForm>
  </Edit>
)

export default AccommodationEdit
