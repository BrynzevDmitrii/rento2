import React from 'react'
import {
  Create,
  TextInput,
  ListProps,
  required,
  SimpleForm
} from 'react-admin'

const AccommodationCreate: React.FC<ListProps> = props => (
  <Create { ...props }>
    <SimpleForm>
      <TextInput label='Название'
        source='name'
        validate={[required()]}
      />
    </SimpleForm>
  </Create>
)

export default AccommodationCreate
