export { default as AccommodationsCreate } from './AccommodationCreate'
export { default as AccommodationsEdit } from './AccommodationEdit'
export { default as AccommodationsList } from './AccommodationList'
