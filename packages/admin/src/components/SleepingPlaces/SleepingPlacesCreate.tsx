import React from 'react'
import {
  Create,
  TextInput,
  ListProps,
  TabbedForm,
  FormTab,
  required
} from 'react-admin'

const SleepingPlacesCreate: React.FC<ListProps> = props => (
  <Create { ...props }>
    <TabbedForm>
      <FormTab label="Данные о спальном месте">
        <TextInput label='Имя'
          source='name'
          validate={[required()]}
        />
        <TextInput label='Тип'
          source='type'
          validate={[required()]}
        />
        <TextInput label='Размер'
          source='size'
          validate={[required()]}
        />
      </FormTab>
    </TabbedForm>
  </Create>
)

export default SleepingPlacesCreate
