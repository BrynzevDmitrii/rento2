import React from 'react'
import {
  Edit,
  SimpleForm,
  TextInput,
  ListProps,
  required
} from 'react-admin'

const SleepingPlacesEdit: React.FC<ListProps> = props => (
  <Edit { ...props }>
    <SimpleForm>
      <TextInput label='Имя'
        source='name'
        validate={[required()]}
      />
      <TextInput label='Тип'
        source='type'
        validate={[required()]}
      />
      <TextInput label='Размер'
        source='size'
        validate={[required()]}
      />
    </SimpleForm>
  </Edit>
)

export default SleepingPlacesEdit
