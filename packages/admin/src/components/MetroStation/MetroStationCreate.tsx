import { FC } from 'react'
import {
  Create,
  TextInput,
  ListProps,
  SimpleForm,
  BooleanInput,
  NumberInput,
  ReferenceInput,
  SelectInput,
  required
} from 'react-admin'

const MetroStationCreate: FC<ListProps> = props => (
  <Create { ...props }>
    <SimpleForm>
      <BooleanInput
        defaultValue={ false }
        label='Является центральной'
        source='isCentral'
        validate={[required()]}
      />
      <BooleanInput
        defaultValue={ false }
        label='Есть переход на кольцевую'
        source='isRingAdjacent'
        validate={[required()]}
      />
      <TextInput label='Название станции'
        source='name'
        validate={[required()]}
      />
      <ReferenceInput label="Ветка метро"
        reference="metro-lines"
        source="metroLineId"
        validate={[required()]}
      >
        <SelectInput optionText="name" />
      </ReferenceInput>

      <NumberInput label='Широта(lat)'
        source='lat'
        validate={[required()]}
      />

      <NumberInput label='Долгота(lon)'
        source='lon'
        validate={[required()]}
      />
    </SimpleForm>
  </Create>
)

export default MetroStationCreate
