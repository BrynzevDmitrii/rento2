import { FC } from 'react'
import {
  Edit,
  SimpleForm,
  TextInput,
  ListProps,
  BooleanInput,
  ReferenceInput,
  SelectInput,
  NumberInput,
  required
} from 'react-admin'

const MetroStationEdit: FC<ListProps> = props => (
  <Edit { ...props }>
    <SimpleForm>
      <BooleanInput label='Является центральной'
        source='isCentral'
      />
      <BooleanInput label='Есть переход на кольцевую'
        source='isRingAdjacent'
      />
      <TextInput label='Название станции'
        source='name'
        validate={[required()]}
      />
      <ReferenceInput label="Ветка метро"
        reference="metro-lines"
        source="metroLineId"
        validate={[required()]}
      >
        <SelectInput optionText="name" />
      </ReferenceInput>

      <NumberInput label='Широта(lat)'
        source='lat'
        validate={[required()]}
      />

      <NumberInput label='Долгота(lon)'
        source='lon'
        validate={[required()]}
      />
    </SimpleForm>
  </Edit>
)

export default MetroStationEdit
