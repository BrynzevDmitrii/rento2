import { FC } from 'react'
import {
  Edit,
  SimpleForm,
  TextInput,
  ListProps,
  required
} from 'react-admin'

const BannerEdit: FC<ListProps> = props => (
  <Edit { ...props }>
    <SimpleForm>
      <TextInput label='Имя'
        source='name'
        validate={[required()]}
      />
    </SimpleForm>
  </Edit>
)

export default BannerEdit
