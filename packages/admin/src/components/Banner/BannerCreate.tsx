import { FC } from 'react'
import {
  Create,
  TextInput,
  ListProps,
  TabbedForm,
  FormTab,
  required
} from 'react-admin'

const BannerCreate: FC<ListProps> = props => (
  <Create { ...props }>
    <TabbedForm>
      <FormTab label="Step 1">
        <TextInput label='Имя'
          source='name'
          validate={[required()]}
        />
      </FormTab>
    </TabbedForm>
  </Create>
)

export default BannerCreate
