import { string } from '@ioc:Adonis/Core/Helpers'
import Factory from '@ioc:Adonis/Lucid/Factory'

import { SleepingPlacesFactory } from './SleepingPlacesFactory'
import { ServicesFactory } from './ServicesFactory'
import { BannersFactory } from './BannersFactory'
import { OrdersFactory } from './OrdersFactory'
import { PhotoFactory } from './PhotoFactory'
import { Apartment } from 'App/Models'

import { AdminDistrictsOfMoscow } from 'Common/enums/AdminDistrictsOfMoscow'
import { Term } from 'Common/enums/Term'

export const ApartmentsFactory = Factory
  .define(Apartment, ({ faker }) => {
    return {
      type: faker.helpers.arrayElement(Object.values(Term)),
      isActive: faker.datatype.boolean(),
      name: faker.internet.userName(),
      latinName: string.dashCase(faker.name.findName(), { capitalize: true }),
      description: faker.lorem.paragraph(),
      bnovoId: faker.datatype.number(),
      inpars_id: faker.datatype.number(),
      price: faker.datatype.number(100_000),
      pricePerMonth: faker.datatype.number(100_000),
      discount: Math.floor(Math.random() * (200 - 100) + 100),
      commission: Math.floor(Math.random() * (200 - 100) + 100),
      utilityBills: Math.floor(Math.random() * (200 - 100) + 100),
      securityDepositShort: Math.floor(Math.random() * (200 - 100) + 100),
      securityDepositLong: Math.floor(Math.random() * (200 - 100) + 100),
      roomsNum: faker.helpers.arrayElement(['1', '2', '3', 'studio']),
      storey: faker.datatype.number({ min: 1, max: 19, precision: 1 }),
      totalStoreys: faker.datatype.number({ min: 1, max: 19, precision: 1 }),
      area: faker.datatype.number({ min: 20, max: 300, precision: 1 }),
      kitchenArea: faker.datatype.number({ min: 4, max: 25, precision: 1 }),
      distanceFromCenter: faker.datatype.number({ min: 0, max: 25, precision: 1 }),
      admArea: faker.helpers.arrayElement(Object.values(AdminDistrictsOfMoscow)),
      district: faker.address.city(),
      sellingPoint: faker.lorem.words(5),
      geoCoordinateX: Number(faker.address.latitude(56, 55, 4)),
      geoCoordinateY: Number(faker.address.longitude(38, 36, 4)),
      metroAvailabilityByFoot: faker.datatype.number({ min: 0, max: 60 }),
      metroAvailabilityByVehicle: faker.datatype.number({ min: 0, max: 60 }),

      check_in_start: faker.datatype.datetime(),
      check_in_end: faker.datatype.datetime(),
      check_out_end: faker.datatype.datetime(),

      smoking_allowed: faker.datatype.boolean(),
      partying_allowed: faker.datatype.boolean(),
      children_allowed: faker.datatype.boolean(),
      pets_allowed: faker.datatype.boolean(),

      max_adults: faker.datatype.number(10),
      max_children: faker.datatype.number(10),

      isPopular: faker.datatype.boolean(),
      isRentoChoose: faker.datatype.boolean(),
    }
  })
  // .relation('accommodations', () => AccommodationsFactory)
  .relation('services', () => ServicesFactory)
  .relation('banners', () => BannersFactory)
  .relation('orders', () => OrdersFactory)
  .relation('sleepingPlaces', () => SleepingPlacesFactory)
  .relation('photos', () => PhotoFactory)
  .build()
