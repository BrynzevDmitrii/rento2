import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class BuildingTypes extends BaseSchema {
  protected tableName = 'building_types'

  public async up (): Promise<void> {
    void this.schema.createTable(this.tableName, (table) => {
      table.bigIncrements('id').primary()
      table.string('name', 255).unique()

      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down (): Promise<void> {
    this.schema.dropTableIfExists(this.tableName)
  }
}
