import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class BuildingTypesToApartments extends BaseSchema {
  protected tableName = 'building_types_to_apartments'

  public async up (): Promise<void> {
    void this.schema.createTable(this.tableName, (table) => {
      table.bigInteger('apartment_id').unsigned()
      table.foreign('apartment_id').references('apartments.id').onUpdate('CASCADE')
        .onDelete('CASCADE')
      table.bigInteger('building_type_id').unsigned()
      table.foreign('building_type_id').references('building_types.id').onUpdate('CASCADE')
        .onDelete('CASCADE')

      table.timestamp('created_at')
      table.timestamp('updated_at')

      table.primary(['apartment_id', 'building_type_id'])
    })
  }

  public async down (): Promise<void> {
    void this.schema.dropTableIfExists(this.tableName)
  }
}
