import { faker } from '@faker-js/faker'

import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'

import { Apartment, Accommodation } from 'App/Models'

export default class AccommodationsToApartmentSeeder extends BaseSeeder {
  public async run (): Promise<void> {
    const apartments = await Apartment.all()
    const accommodations = await Accommodation.all()

    const accommodationsIds = accommodations.map((accommodation) => accommodation.id)
    const randomAccommodationsIds = (): number[] => { return faker.helpers.arrayElements(Object.values(accommodationsIds), 3) }

    apartments.forEach(async apartment => {
      apartment.related('accommodations').attach(randomAccommodationsIds())
      await apartment.save()
    })
  }
}
