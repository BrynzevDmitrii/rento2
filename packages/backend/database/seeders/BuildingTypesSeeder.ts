import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'

import BuildingType from 'App/Models/BuildingType'

import { BuildingTypes } from 'Common/enums/BuildingTypes'

export default class BuildingTypesSeeder extends BaseSeeder {
  public static developmentOnly = true
  public async run (): Promise<void> {
    for (const element of Object.values(BuildingTypes)) {
      const buildingType = new BuildingType()
      buildingType.name = element
      await buildingType.save()
    }
  }
}
