import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'

import { Accommodation } from 'App/Models'

import { Appliances } from 'Common/enums/Appliances'
import { Furniture } from 'Common/enums/Furniture'

const options = ['childrenAllowed', 'petsAllowed', 'parking', 'moreThanAMonth'].map((el) => { return { name: el, type: 'options' } })

export default class AccommodationsSeeder extends BaseSeeder {
  public static developmentOnly = true
  public async run (): Promise<void> {
    for (const element of Appliances.concat(Furniture, options)) {
      const accommodation = new Accommodation()
      accommodation.name = element.name
      accommodation.type = element.type
      await accommodation.save()
    }
  }
}
