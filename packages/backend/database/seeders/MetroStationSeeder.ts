import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'

import MetroStation from './../../app/Models/MetroStation'

import { SubwayStations } from './../../common/enums/SubwayStations'

export default class MetroStationSeeder extends BaseSeeder {
  public static developmentOnly = true

  public async run (): Promise<void> {
    for (const element of SubwayStations) {
      const metroStations = new MetroStation()

      const metroLineId = element.line.id || 1

      metroStations.name = element.title
      metroStations.isCentral = metroLineId === 5
      metroStations.metroLineId = metroLineId
      metroStations.isRingAdjacent = Math.random() < 0.05
      metroStations.lat = 55 + (element.id / 0.71 % 1)
      metroStations.lon = 37 + (element.id / 0.43 % 1)
      await metroStations.save()
    }
  }
}
