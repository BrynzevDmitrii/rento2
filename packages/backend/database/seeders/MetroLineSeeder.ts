import MetroLine from './../../app/Models/MetroLine'
import { SubwayLines } from './../../common/enums/SubwayLine'
import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'

export default class MetroLineSeeder extends BaseSeeder {
  public static developmentOnly = true

  public async run (): Promise<void> {
    for (const element of SubwayLines) {
      const metroLine = new MetroLine()
      metroLine.name = element.name
      metroLine.color = element.color
      metroLine.id = element.id
      await metroLine.save()
    }
  }
}
