import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import { faker } from '@faker-js/faker'

import MetroStation from 'App/Models/MetroStation'
import Apartment from 'App/Models/Apartment'

export default class SubwayStationsToApartmentsSeeder extends BaseSeeder {
  public async run (): Promise<void> {
    const apartments = await Apartment.all()
    const metroStations = await MetroStation.all()
    const metroStationIds = metroStations.map((metroStation) => metroStation.id)
    const randomMetroStationIds = (): number[] => { return faker.helpers.arrayElements(Object.values(metroStationIds), 2) }

    apartments.forEach(apartment => {
      apartment.related('metroStations').attach(randomMetroStationIds())
      apartment.save()
    })
  }
}
