import { faker } from '@faker-js/faker'

import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'

import { Apartment } from 'App/Models'
import BuildingType from 'App/Models/BuildingType'

export default class BuildingTypesToApartmentSeeder extends BaseSeeder {
  public async run (): Promise<void> {
    const apartments = await Apartment.all()

    const buildingTypes = await BuildingType.all()
    const buildingTypesIds = buildingTypes.map((buildingType) => buildingType.id)
    const randomBuildingTypesId = (): number[] => { return faker.helpers.arrayElements(Object.values(buildingTypesIds), 2) }
    apartments.forEach(apartment => {
      apartment.related('buildingTypes').attach(randomBuildingTypesId())
      apartment.save()
    })
  }
}
