import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Application from '@ioc:Adonis/Core/Application'

export default class IndexSeeder extends BaseSeeder {
  private async runSeeder (seeder: { default: typeof BaseSeeder }): Promise<void> {
    if (seeder.default.developmentOnly && !Application.inDev) {
      return
    }
    /* eslint new-cap: ["error", { "newIsCap": false }] */
    await new seeder.default(this.client).run()
  }

  public async run (): Promise<void> {
    await this.runSeeder(await import('../AccommodationsSeeder'))
    await this.runSeeder(await import('../ApartmentSeeder'))
    await this.runSeeder(await import('../BuildingTypesSeeder'))
    await this.runSeeder(await import('../MetroLineSeeder'))
    await this.runSeeder(await import('../MetroStationSeeder'))
    await this.runSeeder(await import('../ReviewSeeder'))
    await this.runSeeder(await import('../SubwayStationsToApartmentsSeeder'))
    await this.runSeeder(await import('../AccommodationsToApartmentSeeder'))
    await this.runSeeder(await import('../BuildingTypesToApartmentSeeder'))
    await this.runSeeder(await import('../UserSeeder'))
  }
}
