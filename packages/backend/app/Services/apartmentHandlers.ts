import murmurhash from 'murmurhash'

import { ModelQueryBuilderContract } from '@ioc:Adonis/Lucid/Orm'
import Redis from '@ioc:Adonis/Addons/Redis'

import { Apartment } from 'App/Models'

import { IWrappedQueriesList } from 'Common/interfaces/IWrappedQueriesList'

export async function getOne (fields: string | undefined, searchValue: string, isAdmin: boolean = false): Promise<Apartment | null> {
  let searchField

  isAdmin ? searchField = 'id' : searchField = 'latin_name'

  let apartment

  if (fields) {
    apartment = await Apartment.query()
      .preload('reviews')
      .select(['id', 'name', ...fields.split(',')])
      .where(searchField, searchValue).first()
  } else {
    apartment = await Apartment.query()
      .preload('reviews')
      .preload('buildingTypes')
      .preload('accommodations')
      .preload('sleepingPlaces')
      .preload('services')
      .preload('banners')
      .preload('photos')
      .preload('metroStations', (metroStationsQuery) => {
        metroStationsQuery.preload('metroLine', (metroLine) => { metroLine.select('name', 'color') })
      })
      .where(searchField, searchValue).first()
  }
  return apartment
}

async function getQueryCache (query: ModelQueryBuilderContract<typeof Apartment, Apartment>): Promise<Apartment[]> {
  const queryCacheKey = `rento:apartments-collections:${murmurhash.v3(query.toQuery())}`
  const cachedRawResult = await Redis.get(queryCacheKey)
  if (!cachedRawResult) {
    const fetchedRows = await query
    await Redis.set(queryCacheKey, JSON.stringify(fetchedRows))
    await Redis.expire(queryCacheKey, 30 * 60) // 30 minutes

    return fetchedRows
  }

  return JSON.parse(cachedRawResult)
}

export async function getCollections (isCached: boolean = false): Promise<IWrappedQueriesList> {
  let wrappedQueriesList
  const queriesList = [
    Apartment.query().preload('reviews').where('isPopular', true).orderBy('createdAt', 'desc').limit(10),
    Apartment.query().preload('reviews').where('isRentoChoose', true).orderBy('createdAt', 'desc').limit(10),
    Apartment.query().preload('reviews').orderBy('createdAt', 'desc').limit(10), // todo put the correct metro stations here
    Apartment.query().preload('reviews').orderBy('createdAt', 'desc').limit(10),
    Apartment.query().preload('reviews').orderByRaw('random()').limit(10),
  ]

  !isCached ? wrappedQueriesList = queriesList : wrappedQueriesList = queriesList.map(async query => await getQueryCache(query))

  const [popular, rentoChoose, inCityCenter, newlyAdded, nearTheSubway] = await Promise.all(wrappedQueriesList)

  return { popular, rentoChoose, inCityCenter, newlyAdded, nearTheSubway }
}
