import { buildUrl, IQueryParams } from 'build-url-ts'
import axios from 'axios'
import { Exception } from '@adonisjs/core/build/standalone'
import Order from 'App/Models/Order'
import crypto from 'crypto'
import tinkoffPaymentsConfig from 'Config/tinkoffPayments'

export enum RequestMethod {
  GET = 'get',
  POST = 'post',
  DELETE = 'delete',
}

export default class TinkoffPaymentsGateway {
  protected readonly config: typeof tinkoffPaymentsConfig
  private readonly apiUrl = 'https://securepay.tinkoff.ru/v2'

  constructor () {
    this.config = tinkoffPaymentsConfig
  }

  protected request = async (
    method: RequestMethod,
    path: string,
    body: Record<string, any> = {},
    queryParams: IQueryParams = {},
  ): Promise<any> => {
    const url = buildUrl(this.apiUrl, {
      path,
      queryParams,
    })

    const output = (await axios[method](url, body, {
      headers: {
        Accept: 'application/json'
      }
    }))
    if (output.status !== 200) {
      throw new Exception(`Tinkoff api exception, ${output.status}`)
    }

    return output.data
  }

  public initializePayment = async (order: Order): Promise<{
    paymentUrl: string
    qrCodeUrl: string | null
  }> => {
    const { Success, ErrorCode, PaymentURL, PaymentId } = await this.request(RequestMethod.POST, '/Init', {
      TerminalKey: this.config.terminalKey,
      Amount: Math.floor(order.fixedTotalPrice * 100),
      OrderId: order.id,
      Description: `Rento, оплата счета номер ${order.id}`,
      DATA: {
        Phone: order.phone,
        Email: order.email,
        PayType: 'O',
      }
    })

    if (!Success) throw new Exception(`Failed to create payment: ${String(ErrorCode)}`)
    const qrCode = await this.generateSbpQR(PaymentId)

    return {
      paymentUrl: PaymentURL,
      qrCodeUrl: qrCode,
    }
  }

  protected generateSbpQR = async (paymentId: number): Promise<string | null> => {
    const payload = {
      TerminalKey: this.config.terminalKey,
      PaymentId: paymentId,
      Password: this.config.password,
    }

    const sortedPayload = Object.keys(payload)
      .sort()
      .reduce((acc, key) => ({
        ...acc, [key]: payload[key as keyof typeof payload]
      }), {})

    const rawHash = Object.values(sortedPayload).join('')
    const sha256RepresentedHash = crypto.createHash('sha256').update(rawHash).digest('hex')

    const { Data } = await this.request(RequestMethod.POST, '/GetQr', {
      TerminalKey: this.config.terminalKey,
      PaymentId: paymentId,
      Token: sha256RepresentedHash,
    })

    return Data ?? null
  }

  public checksum = async (order: Order): Promise<string> => {
    const payload = {
      TerminalKey: this.config.terminalKey,
      Amount: Math.floor(order.fixedTotalPrice * 100),
      OrderId: order.id,
      Description: `Rento, оплата счета номер ${order.id}`,
      Password: this.config.password,
    }

    const sortedPayload = Object.keys(payload)
      .sort()
      .reduce((acc, key) => ({
        ...acc, [key]: payload[key as keyof typeof payload]
      }), {})

    const rawHash = Object.values(sortedPayload).join('')
    const sha256RepresentedHash = crypto.createHash('sha256').update(rawHash).digest('hex')

    return sha256RepresentedHash
  }
}
