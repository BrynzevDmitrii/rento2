import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { rules, schema } from '@ioc:Adonis/Core/Validator'

import { ApartmentsFilterTypes } from 'Common/enums/ApartmentsFilterTypes'
import { SortDirectionTypes } from 'Common/enums/SortDirectionTypes'
import ValidatorMessages from 'App/Validators/ValidatorMessages'
import { RoomsNumberTypes } from 'Common/enums/RoomsNumberTypes'
import { AvailablityTypes } from 'Common/enums/AvailablityTypes'
import { BuildingTypes } from 'Common/enums/BuildingTypes'
import { ShortOptions } from 'Common/enums/ShortOptions'
import { SortByTypes } from 'Common/enums/SortByTypes'
import { Appliances } from 'Common/enums/Appliances'
import { FlorTypes } from 'Common/enums/FloorTypes'
import { Furniture } from 'Common/enums/Furniture'

export default class ApartmentPayloadValidator extends ValidatorMessages {
  constructor (protected ctx: HttpContextContract) {
    super()
  }

  public schema = schema.create({
    fields: schema.object.optional().members({
      roomsNumber: schema.array.optional().members(schema.enum(Object.values(RoomsNumberTypes))),
      areChildrenAllowed: schema.boolean.optional([rules.trim()]),
      arePetsAllowed: schema.boolean.optional([rules.trim()]),
      priceFrom: schema.number.optional([rules.unsigned()]),
      priceTo: schema.number.optional([rules.unsigned()]),
      areaFrom: schema.number.optional([rules.unsigned()]),
      areaTo: schema.number.optional([rules.unsigned()]),
      floorFrom: schema.number.optional([rules.unsigned()]),
      floorTo: schema.number.optional([rules.unsigned()]),
      floorType: schema.array.optional().members(schema.enum(Object.values(FlorTypes))),
      sortBy: schema.enum(Object.values(SortByTypes)),
      sortDirection: schema.enum(Object.values(SortDirectionTypes)),
      appliances: schema.array.optional().members(schema.enum(Appliances.map(appliance => appliance.name))),
      availablityBy: schema.enum.optional(Object.values(AvailablityTypes)),
      availabilityTimeFrom: schema.number.optional([rules.unsigned()]),
      availabilityTimeTo: schema.number.optional([rules.unsigned()]),
      apartmentFurniture: schema.array.optional().members(schema.enum(Furniture.map(part => part.name))),
      buildingType: schema.array.optional().members(schema.enum(Object.values(BuildingTypes))),
      metroStationIds: schema.array.optional().members(schema.number([rules.unsigned()])),

      pricePerNightFrom: schema.number.optional([rules.unsigned()]),
      pricePerNightTo: schema.number.optional([rules.unsigned()]),
      dateIn: schema.date.optional(),
      dateOut: schema.date.optional(),
      options: schema.array.optional().members(schema.enum(Object.values(ShortOptions))),
      OfAdults: schema.number.optional([rules.unsigned()]),
      OfChilds: schema.number.optional([rules.unsigned()]),
      OfInfants: schema.number.optional([rules.unsigned()]),
    }),
    offset: schema.number.optional(),
    limit: schema.number.optional(),
    apartmentRentType: schema.enum(Object.values(ApartmentsFilterTypes))
  })
}
