import { schema, rules } from '@ioc:Adonis/Core/Validator'
import ValidatorMessages from 'App/Validators/ValidatorMessages'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class AccommodationValidator extends ValidatorMessages {
  constructor (protected ctx: HttpContextContract) {
    super()
  }

  public schema = schema.create({
    name: schema.string({ trim: true }, [
      rules.minLength(3),
      rules.maxLength(255)
    ]),
    type: schema.enum(['furniture', 'appliances', 'options'] as const)
  })
}
