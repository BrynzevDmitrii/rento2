import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import { rules, schema } from '@ioc:Adonis/Core/Validator'

import ApartmentPayloadValidator from 'App/Validators/ApartmentPayloadValidator'
import ApartmentValidator from 'App/Validators/ApartmentValidator'
import * as apartmentHandlers from 'App/Services/apartmentHandlers'
import { Apartment } from 'App/Models'

import { ApartmentsFilterTypes } from 'Common/enums/ApartmentsFilterTypes'
import { SortDirectionTypes } from 'Common/enums/SortDirectionTypes'
import { PAGINATION_PAGE_SIZE } from 'Common/constants/Controller'
import { HttpStatusCode } from 'Common/constants/HttpStatusCode'
import { RoomsNumberTypes } from 'Common/enums/RoomsNumberTypes'
import { AvailablityTypes } from 'Common/enums/AvailablityTypes'
import { ShortOptions } from 'Common/enums/ShortOptions'
import { SortByTypes } from 'Common/enums/SortByTypes'
import { FlorTypes } from 'Common/enums/FloorTypes'
import {
  creatingErrMsg,
  creatingOkMsg,
  creatingPaginatedList,
  creatingShowCollectionsOkMsg,
} from 'Common/helpers/creatingResponse'
import { ModelQueryBuilderContract } from '@ioc:Adonis/Lucid/Orm'

export default class ApartmentsController {
  public async listAdmin ({ response, request }: HttpContextContract): Promise<void> {
    const { sortDirection, perPage } = request.qs()
    const { search, fields } = await request.validate({
      schema: schema.create({
        search: schema.string.optional(),
        fields: schema.string.optional({}, [
          rules.fieldNames()
        ])
      }),
    })

    let apartments
    if (fields) {
      apartments = Apartment.query()
        .preload('reviews')
        .select(['id', 'name', ...fields.split(',')])
    } else {
      apartments = Apartment.query()
        .preload('reviews')
        .preload('accommodations')
        .preload('sleepingPlaces')
        .preload('services')
        .preload('banners')
        .preload('photos')
        .preload('buildingTypes')
        .preload('metroStations', (metroStationsQuery) => {
          metroStationsQuery.preload('metroLine', (metroLine) => { metroLine.select('name', 'color') })
        })
    }

    if (search) {
      apartments = apartments.where('name', 'ilike', `%${search}%`)
    }

    const { popular, rentoChoose, inCityCenter, newlyAdded, nearTheSubway } = await apartmentHandlers.getCollections()

    const fetchedApartments = await apartments
      .orderBy('createdAt', sortDirection === SortDirectionTypes.ASC ? SortDirectionTypes.ASC : SortDirectionTypes.DESC)
      .paginate(request.param('page', 1), perPage ?? PAGINATION_PAGE_SIZE)
    return response
      .status(HttpStatusCode.OK)
      .send(
        creatingPaginatedList(fetchedApartments, {
          collections: {
            popular, rentoChoose, inCityCenter, newlyAdded, nearTheSubway
          }
        })
      )
  }

  public async many ({ response, request }: HttpContextContract): Promise<void> {
    const { sortDirection } = request.qs()
    const { ids } = await request.validate({
      schema: schema.create({
        ids: schema.array().members(schema.number()),
      }),
    })

    const apartments = Apartment.query()
      .preload('reviews')
      .preload('buildingTypes')
      .preload('accommodations')
      .preload('sleepingPlaces')
      .preload('services')
      .preload('banners')
      .preload('photos')
      .preload('metroStations', (metroStationsQuery) => {
        metroStationsQuery.preload('metroLine', (metroLine) => { metroLine.select('name', 'color') })
      })
      .orderBy('createdAt', sortDirection === SortDirectionTypes.ASC ? SortDirectionTypes.ASC : SortDirectionTypes.DESC)
      .whereIn('id', ids)
      .paginate(1, 100)

    return response
      .status(HttpStatusCode.OK)
      .send(
        creatingPaginatedList(await apartments)
      )
  }

  public async listFront ({ request, response }: HttpContextContract): Promise<void> {
    const { apartmentRentType, fields, offset, limit } = await request.validate(ApartmentPayloadValidator)

    const requestApartmentsLongWithFilterDb = (fields: ApartmentPayloadValidator['schema']['props']['fields'], queryLong: ModelQueryBuilderContract<typeof Apartment, Apartment>): void => {
      /* eslint-disable  @typescript-eslint/no-non-null-assertion */
      queryLong
        .if(fields!.roomsNumber, (query) => {
          query.whereIn('rooms_num', fields!.roomsNumber!.includes(RoomsNumberTypes.ONE_ROOMED) ? fields!.roomsNumber!.concat(RoomsNumberTypes.STUDIO) : fields!.roomsNumber!)
        })
        .if(fields!.areChildrenAllowed, (query) => {
          query.where('children_allowed', fields!.areChildrenAllowed!)
        })
        .if(fields!.arePetsAllowed, (query) => {
          query.where('pets_allowed', fields!.arePetsAllowed!)
        })
        .match(
          [fields!.priceFrom && fields!.priceTo, (query) => query.whereBetween('price_per_month', [fields!.priceFrom!, fields!.priceTo!])],
          [fields!.priceFrom, (query) => query.where('price_per_month', '>=', fields!.priceFrom!)],
          [fields!.priceTo, (query) => query.where('price_per_month', '<=', fields!.priceTo!)],
        )
        .match(
          [fields!.areaFrom && fields!.areaTo, (query) => query.whereBetween('area', [fields!.areaFrom!, fields!.areaTo!])],
          [fields!.areaFrom, (query) => query.where('area', '>=', fields!.areaFrom!)],
          [fields!.areaTo, (query) => query.where('area', '<=', fields!.areaTo!)],
        )
        .match(
          [fields!.floorFrom && fields!.floorTo, (query) => query.whereBetween('storey', [fields!.floorFrom!, fields!.floorTo!])],
          [fields!.floorFrom, (query) => query.where('storey', '>=', fields!.floorFrom!)],
          [fields!.floorTo, (query) => query.where('storey', '<=', fields!.floorTo!)],
        )
        .if(fields!.floorType && fields!.floorType.includes(FlorTypes.NOT_FIRST), (query) => {
          query.where('storey', '<>', 1)
        })
        .if(fields!.floorType && fields!.floorType.includes(FlorTypes.NOT_LAST) && !fields!.floorType.includes(FlorTypes.LAST), (query) => {
          query.whereColumn('storey', '<>', 'total_storeys')
        })
        .if(fields!.floorType && fields!.floorType.includes(FlorTypes.LAST), (query) => {
          query.whereColumn('storey', '=', 'total_storeys')
        })
        .if(fields!.availablityBy === AvailablityTypes.VEHICLE, (query) => {
          query.match(
            [fields!.availabilityTimeFrom && fields!.availabilityTimeTo, (query) => query.whereBetween('metro_availability_by_vehicle', [fields!.availabilityTimeFrom!, fields!.availabilityTimeTo!])],
            [fields!.availabilityTimeFrom, (query) => query.where('metro_availability_by_vehicle', '>=', fields!.availabilityTimeFrom!)],
            [fields!.availabilityTimeTo, (query) => query.where('metro_availability_by_vehicle', '<=', fields!.availabilityTimeTo!)]
          )
        })
        .if(fields!.availablityBy === AvailablityTypes.FOOT, (query) => {
          query.match(
            [fields!.availabilityTimeFrom && fields!.availabilityTimeTo, (query) => query.whereBetween('metro_availability_by_vehicle', [fields!.availabilityTimeFrom!, fields!.availabilityTimeTo!])],
            [fields!.availabilityTimeFrom, (query) => query.where('metro_availability_by_foot', '>=', fields!.availabilityTimeFrom!)],
            [fields!.availabilityTimeTo, (query) => query.where('metro_availability_by_foot', '<=', fields!.availabilityTimeTo!)]
          )
        })
        .if(fields!.metroStationIds && fields!.metroStationIds.length > 0, (query) => {
          query.whereHas('metroStations', (query) => {
            query.whereIn('id', (fields!.metroStationIds!.map(e => e)))
          })
        })
        .if(fields!.apartmentFurniture, (query) => {
          query.whereHas('accommodations', (query) => {
            query
              .whereIn('accommodations.name', fields!.apartmentFurniture!)
              .andWhere('type', 'furniture')
          })
        })
        .if(fields!.appliances, (query) => {
          query.whereHas('accommodations', (query) => {
            query
              .whereIn('accommodations.name', fields!.appliances!)
              .andWhere('type', 'appliances')
          })
        })
        .if(fields!.buildingType, (query) => {
          query.whereHas('buildingTypes', (query) => {
            query.whereIn('name', fields!.buildingType!)
          })
        })
        .if(fields!.sortBy === SortByTypes.PRICE, (query) => {
          query.orderBy('price_per_month', fields!.sortDirection)
        })
        .if(fields!.sortBy === SortByTypes.AREA, (query) => {
          query.orderBy('area', fields!.sortDirection)
        })
        .if(fields!.sortBy === SortByTypes.CREATED_AT, (query) => {
          query.orderBy('created_at', fields!.sortDirection)
        })
    }

    const requestApartmentsShortWithFilterDb = (fields: ApartmentPayloadValidator['schema']['props']['fields'], queryShort: ModelQueryBuilderContract<typeof Apartment, Apartment>): void => {
      queryShort
        .if(fields!.roomsNumber, (query) => [
          query.whereIn('rooms_num', fields!.roomsNumber!.includes(RoomsNumberTypes.ONE_ROOMED) ? fields!.roomsNumber!.concat(RoomsNumberTypes.STUDIO) : fields!.roomsNumber!)
        ])
        .match(
          [fields!.pricePerNightFrom && fields!.pricePerNightTo, (query) => query.whereBetween('price', [fields!.pricePerNightFrom!, fields!.pricePerNightTo!])],
          [fields!.pricePerNightFrom, (query) => query.where('price', '>=', fields!.pricePerNightFrom!)],
          [fields!.pricePerNightTo, (query) => query.where('price', '<=', fields!.pricePerNightTo!)],
        )
        .if(fields!.dateIn, (query) => {
          query.where('check_in_end', '>=', fields!.dateIn!.toString())
        })
        .if(fields!.dateOut, (query) => {
          query.where('check_out_end', '<=', fields!.dateOut!.toString())
        })
        .if(fields!.OfAdults, (query) => {
          query.where('max_adults', '>=', fields!.OfAdults!)
        })
        .if(fields!.OfChilds, (query) => {
          query.where('max_children', '>=', fields!.OfChilds!)
        })

      // // .where('max_children', '>=', 'OfInfants')               // add OfInfants

        .match(
          [fields!.areaFrom && fields!.areaTo, (query) => query.whereBetween('area', [fields!.areaFrom!, fields!.areaTo!])],
          [fields!.areaFrom, (query) => query.where('area', '>=', fields!.areaFrom!)],
          [fields!.areaTo, (query) => query.where('area', '<=', fields!.pricePerNightTo!)],
        )
        .if(fields!.availablityBy === AvailablityTypes.VEHICLE, (query) => {
          query.match(
            [fields!.availabilityTimeFrom && fields!.availabilityTimeTo, (query) => query.whereBetween('metro_availability_by_vehicle', [fields!.availabilityTimeFrom!, fields!.availabilityTimeTo!])],
            [fields!.availabilityTimeFrom, (query) => query.where('metro_availability_by_vehicle', '>=', fields!.availabilityTimeFrom!)],
            [fields!.availabilityTimeTo, (query) => query.where('metro_availability_by_vehicle', '<=', fields!.availabilityTimeTo!)]
          )
        })
        .if(fields!.availablityBy === AvailablityTypes.FOOT, (query) => {
          query.match(
            [fields!.availabilityTimeFrom && fields!.availabilityTimeTo, (query) => query.whereBetween('metro_availability_by_vehicle', [fields!.availabilityTimeFrom!, fields!.availabilityTimeTo!])],
            [fields!.availabilityTimeFrom, (query) => query.where('metro_availability_by_foot', '>=', fields!.availabilityTimeFrom!)],
            [fields!.availabilityTimeTo, (query) => query.where('metro_availability_by_foot', '<=', fields!.availabilityTimeTo!)]
          )
        })
        .if(fields!.metroStationIds && fields!.metroStationIds.length > 0, (query) => {
          query.whereHas('metroStations', (query) => {
            query.whereIn('id', (fields!.metroStationIds!.map(e => e)))
          })
        })
        .if(fields!.options, (query) => {
          query.whereHas('accommodations', (query) => {
            query
              .whereIn('accommodations.name', fields!.options!)
              .andWhere('type', 'furniture')
          })
        })
        .if(fields!.options, (query) => {
          query.whereHas('accommodations', (query) => {
            query
              .whereIn('accommodations.name', fields!.options!)
          })
        })
        .if(fields!.options && fields!.options.includes(ShortOptions.CHILDREN_ALLOWED), (query) => {
          query.where('children_allowed', '=', true)
        })
        .if(fields!.options && fields!.options.includes(ShortOptions.PETS_ALLOWED), (query) => {
          query.where('pets_allowed', '=', true)
        })
        .if(fields!.appliances, (query) => {
          query.whereHas('accommodations', (query) => {
            query
              .whereIn('accommodations.name', fields!.appliances!)
              .andWhere('type', 'appliances')
          })
        })
        .if(fields!.sortBy === SortByTypes.PRICE, (query) => {
          query.orderBy('price', fields!.sortDirection)
        })
        .if(fields!.sortBy === SortByTypes.AREA, (query) => {
          query.orderBy('area', fields!.sortDirection)
        })
        .if(fields!.sortBy === SortByTypes.CREATED_AT, (query) => {
          query.orderBy('created_at', fields!.sortDirection)
        })
    }

    const apartments = Apartment.query()
      .preload('reviews')
      .preload('accommodations')
      .preload('sleepingPlaces')
      .preload('services')
      .preload('banners')
      .preload('photos')
      .preload('metroStations', (metroStationsQuery) => {
        metroStationsQuery.preload('metroLine', (metroLine) => { metroLine.select('name', 'color') })
      })
      .preload('buildingTypes')

      .match(
        [apartmentRentType === ApartmentsFilterTypes.LONG, (queryLong) =>
          queryLong.whereIn('type', [ApartmentsFilterTypes.LONG, ApartmentsFilterTypes.All])
        ],
        [apartmentRentType === ApartmentsFilterTypes.SHORT, (queryShort) =>
          queryShort.whereIn('type', [ApartmentsFilterTypes.SHORT, ApartmentsFilterTypes.All])
        ],
      )

      .if(fields, (query) => {
        query.match(
          [apartmentRentType === ApartmentsFilterTypes.LONG, (queryLong) => {
            requestApartmentsLongWithFilterDb(fields, queryLong)
          }],
          [apartmentRentType === ApartmentsFilterTypes.SHORT, (queryShort) => {
            requestApartmentsShortWithFilterDb(fields, queryShort)
          }]
        )
      })
      .offset(offset ?? 0)
      .limit(limit ?? 20)

    const { popular, rentoChoose, inCityCenter, newlyAdded, nearTheSubway } = await apartmentHandlers.getCollections(true)

    return response
      .status(HttpStatusCode.OK)
      .send(creatingShowCollectionsOkMsg(await apartments, {
        popular, rentoChoose, inCityCenter, newlyAdded, nearTheSubway
      }))
  }

  public async one ({ request, response }: HttpContextContract): Promise<void> {
    const { fields } = await request.validate({
      schema: schema.create({
        fields: schema.string.optional({}, [
          rules.fieldNames()
        ]),
      }),
    })

    let apartment
    if (fields) {
      apartment = await Apartment.query()
        .preload('reviews')
        .select(['id', 'name', ...fields.split(',')])
        .where('id', request.param('id')).first()
    } else {
      apartment = await Apartment.query()
        .preload('reviews')
        .preload('buildingTypes')
        .preload('accommodations')
        .preload('sleepingPlaces')
        .preload('services')
        .preload('banners')
        .preload('photos')
        .preload('metroStations', (metroStationsQuery) => {
          metroStationsQuery.preload('metroLine', (metroLine) => { metroLine.select('name', 'color') })
        })
        .where('id', request.param('id')).first()
    }
    if (!apartment) {
      return response.send(creatingErrMsg('error', 'Apartment not found'))
    } else {
      return response.status(HttpStatusCode.OK).send(creatingOkMsg(apartment))
    }
  }

  public async show ({ request, response }: HttpContextContract): Promise<void> {
    const { fields } = await request.validate({
      schema: schema.create({
        fields: schema.string.optional({}, [
          rules.fieldNames()
        ]),
      }),
    })
    const latinName: string = request.param('latinName')

    const apartment = await apartmentHandlers.getOne(fields, latinName)

    const wrappedQueriesList = [
      Apartment.query().preload('reviews').where('isPopular', true).limit(10),
      Apartment.query().preload('reviews').where('isRentoChoose', true).limit(10),
    ]
    const [popular, rentoChoose] = await Promise.all(wrappedQueriesList)

    if (!apartment) {
      return response.send(creatingErrMsg('error', 'Apartment not found'))
    } else {
      return response.status(HttpStatusCode.OK).send(creatingShowCollectionsOkMsg(apartment, {
        popular, rentoChoose
      }))
    }
  }

  public async create ({
    request,
    response,
  }: HttpContextContract): Promise<void> {
    const apartmentPayload = await request.validate(ApartmentValidator)
    const apartment = await Apartment.create(apartmentPayload)

    await Promise.all([
      apartment
        .related('accommodations')
        .attach(apartmentPayload.accommodations.map(({ id }) => id)),
      apartment
        .related('metroStations')
        .attach(apartmentPayload.metroStations.map(({ id }) => id)),
      apartment.related('sleepingPlaces').attach(
        apartmentPayload.sleepingPlaces.reduce(
          (prev, { id, number }) => ({
            ...prev,
            [id]: { number },
          }),
          {}
        )
      ),
    ])

    return response
      .status(HttpStatusCode.Created)
      .send(creatingOkMsg(apartment))
  }

  public async update ({
    response,
    request,
  }: HttpContextContract): Promise<void> {
    const apartment = await Apartment.query()
      .preload('reviews')
      .preload('buildingTypes')
      .preload('accommodations')
      .preload('sleepingPlaces').preload('metroStations')
      .where('id', request.param('id', null)).first()

    if (!apartment) {
      return response.status(HttpStatusCode.NotFound).send(creatingErrMsg('error', 'Apartments not found'))
    }

    const modifiedApartmentPayload = await request.validate(ApartmentValidator)
    await Promise.all([
      apartment.related('accommodations')
        .detach(apartment.accommodations.map(({ id }) => id)),
      apartment.related('metroStations')
        .detach(apartment.metroStations.map(({ id }) => id)),
      apartment.related('sleepingPlaces')
        .detach(apartment.sleepingPlaces.map(({ id }) => id)),
      apartment.related('buildingTypes')
        .detach(apartment.buildingTypes.map(({ id }) => id)),
    ])

    await Promise.all([
      apartment.related('accommodations')
        .attach(modifiedApartmentPayload.accommodations.map(({ id }) => id)),
      apartment.related('metroStations')
        .attach(modifiedApartmentPayload.metroStations.map(({ id }) => id)),
      apartment.related('sleepingPlaces')
        .attach(modifiedApartmentPayload.sleepingPlaces.map(({ id }) => id)),
      apartment.related('buildingTypes')
        .attach(modifiedApartmentPayload.buildingTypes.map(({ id }) => id)),
    ])

    const updatedApartment = await apartment
      .merge(modifiedApartmentPayload)
      .save()

    return response
      .status(HttpStatusCode.OK)
      .send(creatingOkMsg(updatedApartment))
  }

  public async delete ({
    response,
    request,
  }: HttpContextContract): Promise<any> {
    const apartment = await Apartment.findOrFail(request.param('id', null))

    await apartment.delete()
    return response.status(HttpStatusCode.OK).send(creatingOkMsg(apartment.id))
  }
}
