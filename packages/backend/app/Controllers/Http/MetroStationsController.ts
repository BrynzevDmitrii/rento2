import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { HttpStatusCode } from 'Common/constants/HttpStatusCode'
import { creatingErrMsg, creatingOkMsg, creatingPaginatedList } from 'Common/helpers/creatingResponse'
import { PAGINATION_PAGE_SIZE } from 'Common/constants/Controller'
import CreateMetroStationValidator from 'App/Validators/MetroStationValidator'
import MetroStation from 'App/Models/MetroStation'

export default class MetroStationsController {
  public async list ({ response, request }: HttpContextContract): Promise<void> {
    const { sortDirection, perPage } = request.qs()
    return response.status(HttpStatusCode.OK).send(
      creatingPaginatedList(
        await MetroStation.query()
          .preload('metroLine')
          .orderBy('createdAt', sortDirection === 'asc' ? 'asc' : 'desc')
          .paginate(request.param('page', 1), perPage ?? PAGINATION_PAGE_SIZE)
      )
    )
  }

  public async publicList ({ response }: HttpContextContract): Promise<void> {
    const stations = await MetroStation.query()
      .select('id', 'name', 'lat', 'lon', 'isCentral', 'isRingAdjacent', 'metroLineId')
      .preload('metroLine',
        (metroLineQuery) => {
          metroLineQuery
            .select('id', 'name', 'color')
        })

    if (!stations) {
      return response
        .status(HttpStatusCode.NotFound)
        .send(creatingErrMsg('error', 'db Metro not found'))
    }
    return response.status(HttpStatusCode.OK)
      .send(creatingOkMsg(stations))
  }

  public async one ({ response, request }: HttpContextContract): Promise<void> {
    const metroStation = await MetroStation.query()
      .preload('metroLine')
      .where('id', request.param('id')).first()
    return response.status(HttpStatusCode.OK).send(creatingOkMsg(metroStation))
  }

  public async update ({ response, request }: HttpContextContract): Promise<void> {
    const metroStation = await MetroStation.find(request.param('id', null))
    if (!metroStation) {
      return response.send(creatingErrMsg('error', 'MetroStation not found'))
    }

    const updatedMetroStation = await metroStation.merge(
      await request.validate(CreateMetroStationValidator)
    ).save()

    return response.status(HttpStatusCode.OK).send(creatingOkMsg(updatedMetroStation))
  }

  public async create ({ request, response }: HttpContextContract): Promise<void> {
    const metroStation = await MetroStation.create({
      ...(await request.validate(CreateMetroStationValidator)),
    })

    return response
      .status(HttpStatusCode.Created)
      .send(creatingOkMsg(metroStation))
  }

  public async delete ({ response, request }: HttpContextContract): Promise<any> {
    const metroStation = await MetroStation.find(request.param('id', null))
    if (!metroStation) {
      return response.send(creatingErrMsg('error', 'MetroStation not found'))
    }

    await metroStation.delete()
    return response.status(HttpStatusCode.OK).send(creatingOkMsg(metroStation.id))
  }
}
