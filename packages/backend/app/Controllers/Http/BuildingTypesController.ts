import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import BuildingTypeValidator from 'App/Validators/BuildingTypeValidator'
import BuildingType from 'App/Models/BuildingType'

import { creatingErrMsg, creatingOkMsg, creatingPaginatedList } from 'Common/helpers/creatingResponse'
import { PAGINATION_PAGE_SIZE } from 'Common/constants/Controller'
import { HttpStatusCode } from 'Common/constants/HttpStatusCode'

export default class BuildingTypesController {
  public async list ({ response, request }: HttpContextContract): Promise<void> {
    const { sortDirection, perPage } = request.qs()
    const items = await BuildingType.query()
      .orderBy('createdAt', sortDirection === 'asc' ? 'asc' : 'desc')
      .paginate(request.param('page', 1), perPage ?? PAGINATION_PAGE_SIZE)

    return response
      .status(HttpStatusCode.OK).send(
        creatingPaginatedList(items)
      )
  }

  public async one ({ response, request }: HttpContextContract): Promise<void> {
    const buildingType = await BuildingType.findOrFail(request.param('id', null))
    return response.status(HttpStatusCode.OK).send(creatingOkMsg(buildingType))
  }

  public async update ({ response, request }: HttpContextContract): Promise<void> {
    const buildingType = await BuildingType.findOrFail(request.param('id', null))

    const updatedBuildingType = await buildingType.merge(
      await request.validate(BuildingTypeValidator)
    ).save()

    return response.status(HttpStatusCode.OK).send(creatingOkMsg(updatedBuildingType))
  }

  public async create ({ request, response }: HttpContextContract): Promise<void> {
    const buildingType = await BuildingType.create(
      await request.validate(BuildingTypeValidator)
    )

    return response
      .status(HttpStatusCode.Created)
      .send(creatingOkMsg(buildingType))
  }

  public async delete ({ response, request }: HttpContextContract): Promise<any> {
    const buildingType = await BuildingType.find(request.param('id', null))

    if (!buildingType) {
      return response.send(creatingErrMsg('error', 'Building type not found'))
    }
    await buildingType.delete()
    return response.status(HttpStatusCode.OK).send(creatingOkMsg(buildingType.id))
  }
}
