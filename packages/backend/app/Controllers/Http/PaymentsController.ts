import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { HttpStatusCode } from 'Common/constants/HttpStatusCode'
import Order from 'App/Models/Order'
import { TransactionStatus } from 'Common/enums/TransactionStatus'
import TinkoffPaymentsGateway from 'App/Services/TinkoffPaymentsGateway'

export default class PaymentsController {
  public async success ({ response, request }: HttpContextContract): Promise<void> {
    const { Status, OrderId, Token } = request.body()

    const gateway = new TinkoffPaymentsGateway()
    const order = await Order.findOrFail(OrderId)

    const paymentHash = gateway.checksum(order)
    if (paymentHash !== Token) {
      return response
        .status(HttpStatusCode.OK).send('WRONG TOKEN')
    }

    if (Status === 'CONFIRMED') {
      order.paymentStatus = TransactionStatus.COMPLETED
      await order.save()

      return response
        .status(HttpStatusCode.OK).send('OK')
    }

    order.paymentStatus = TransactionStatus.FAILED
    order.paymentComment = `Ошибка при оплате: ${String(Status)}`
    await order.save()

    return response
      .status(HttpStatusCode.OK).send('OK')
  }
}
