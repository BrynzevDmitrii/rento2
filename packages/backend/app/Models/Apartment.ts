import { DateTime } from 'luxon'

import {
  HasMany,
  BaseModel,
  column,
  hasMany,
  ManyToMany,
  manyToMany,
  computed,
} from '@ioc:Adonis/Lucid/Orm'

import CamelCaseNamingStrategy from './NamingStrategy/CamelCaseNamingStrategy'
import Accommodation from 'App/Models/Accommodation'
import BuildingType from 'App/Models/BuildingType'
import SleepingPlace from './SleepingPlace'
import MetroStation from './MetroStation'
import Photo from 'App/Models/Photo'
import Service from './Service'
import Review from './Review'
import Banner from './Banner'
import Order from './Order'

import { AdminDistrictsOfMoscow } from 'Common/enums/AdminDistrictsOfMoscow'
import { Term } from 'Common/enums/Term'

export default class Apartment extends BaseModel {
  public static namingStrategy = new CamelCaseNamingStrategy()

  @column({ isPrimary: true, serialize: (v) => Number(v) })
  public id!: number

  @manyToMany(() => Service, {
    pivotTable: 'services_to_apartments',
    pivotTimestamps: true,
  })
  public services!: ManyToMany<typeof Service>

  @manyToMany(() => MetroStation, {
    pivotTable: 'metro_stations_to_apartments',
    pivotTimestamps: true,
  })
  public metroStations!: ManyToMany<typeof MetroStation>

  @manyToMany(() => Banner, {
    pivotTable: 'banners_to_apartments',
    pivotTimestamps: true,
  })
  public banners!: ManyToMany<typeof Banner>

  @hasMany(() => Order)
  public orders!: HasMany<typeof Order>

  @hasMany(() => Review)
  public reviews!: HasMany<typeof Review>

  @computed()
  public get reviewsStatistics (): {
    repairs: number
    purity: number
    location: number
    priceQuality: number
    total: number
  } {
    if (!this.$preloaded['reviews']) {
      throw new Error('You have to preload reviews first')
    }

    const repairs = this.reviews.map(r => r.repairs)
    const purity = this.reviews.map(r => r.purity)
    const location = this.reviews.map(r => r.location)
    const priceQuality = this.reviews.map(r => r.priceQuality)

    const sum = (l: number[]): number => l.reduce((a, b) => a + b, 0)

    const total = [
      sum(repairs) / repairs.length,
      sum(purity) / purity.length,
      sum(location) / location.length,
      sum(priceQuality) / priceQuality.length,
    ]

    return {
      repairs: +(sum(repairs) / repairs.length).toFixed(2),
      purity: +(sum(purity) / purity.length).toFixed(2),
      location: +(sum(location) / location.length).toFixed(2),
      priceQuality: +(sum(priceQuality) / priceQuality.length).toFixed(2),
      total: +(sum(total) / total.length).toFixed(2),
    }
  }

  @column()
  public type!: Term

  @column()
  public isActive!: boolean

  @column()
  public name!: string

  @column()
  public latinName!: string | null

  @column()
  public description!: string

  @column()
  public bnovoId!: number | null

  @column()
  public inparsId!: number | null

  @column()
  public price!: number | null

  @column()
  public pricePerMonth!: number | null

  @column()
  public discount!: number

  @column()
  public commission!: number

  @column()
  public utilityBills!: number

  @column()
  public securityDepositShort!: number

  @column()
  public securityDepositLong!: number

  @column()
  public roomsNum!: string

  @column()
  public storey!: number

  @column()
  public totalStoreys!: number

  @column()
  public area!: number

  @column()
  public kitchenArea!: number

  @column()
  public distanceFromCenter!: number

  @column()
  public admArea!: AdminDistrictsOfMoscow

  @column()
  public district!: string

  @column()
  public sellingPoint!: string

  @column()
  public geoCoordinateX!: number

  @column()
  public geoCoordinateY!: number

  @column()
  public metroAvailabilityByFoot!: number

  @column()
  public metroAvailabilityByVehicle!: number

  @column()
  public checkInStart!: DateTime | null

  @column()
  public checkInEnd!: DateTime | null

  @column()
  public checkOutEnd!: DateTime | null

  @column()
  public smokingAllowed!: boolean | null

  @column()
  public partyingAllowed!: boolean | null

  @column()
  public childrenAllowed!: boolean | null

  @column()
  public petsAllowed!: boolean | null

  @column()
  public isPopular!: boolean

  @column()
  public isRentoChoose!: boolean

  @column()
  public maxAdults!: number | null

  @column()
  public maxChildren!: number | null

  @column.dateTime({ autoCreate: true })
  public createdAt!: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt!: DateTime

  @manyToMany(() => Accommodation, {})
  public accommodations!: ManyToMany<typeof Accommodation>

  @hasMany(() => Photo, {})
  public photos!: HasMany<typeof Photo>

  @manyToMany(() => SleepingPlace, {
    pivotColumns: ['number'],
    pivotTable: 'sleeping_places_to_apartments',
    pivotTimestamps: true,
  })
  public sleepingPlaces!: ManyToMany<typeof SleepingPlace>

  @manyToMany(() => BuildingType, {
    pivotTable: 'building_types_to_apartments',
    pivotTimestamps: true,
  })
  public buildingTypes!: ManyToMany<typeof BuildingType>
}
