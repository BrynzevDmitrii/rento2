import { DateTime } from 'luxon'

import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

import CamelCaseNamingStrategy from './NamingStrategy/CamelCaseNamingStrategy'

export default class BuildingType extends BaseModel {
  public static namingStrategy = new CamelCaseNamingStrategy()

  @column({ isPrimary: true, serialize: (v) => Number(v) })
  public id!: number

  @column()
  public name!: string

  @column.dateTime({ autoCreate: true })
  public createdAt!: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt!: DateTime
}
