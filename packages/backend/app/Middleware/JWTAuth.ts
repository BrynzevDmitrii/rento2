import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { HttpStatusCode } from 'Common/constants/HttpStatusCode'
import { creatingErrMsg } from 'Common/helpers/creatingResponse'

export default class AuthMiddleware {
  public async handle ({ auth, response }: HttpContextContract, next: () => Promise<void>): Promise<void> {
    if (await auth.use('jwt').check()) {
      return await next()
    }

    return response
      .status(HttpStatusCode.Unauthorized)
      .send(creatingErrMsg('NOT_AUTHORIZED'))
  }
}
