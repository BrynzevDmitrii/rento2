import Env from '@ioc:Adonis/Core/Env'

export default {
  terminalKey: Env.get('TINKOFF_TERMINAL_KEY'),
  password: Env.get('TINKOFF_PASSWORD'),
}
