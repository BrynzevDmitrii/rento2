export enum BuildingTypes {
  STALIN_ERA = 'stalinEra',
  NEW = 'newlyBuilt',
  IN_THE_CENTER = 'inTheCenter',
}
