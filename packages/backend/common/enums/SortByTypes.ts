export enum SortByTypes {
  PRICE = 'price',
  AREA = 'area',
  CREATED_AT = 'createdAt',
}
