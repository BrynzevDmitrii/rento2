export enum RoomsNumberTypes {
  ONE_ROOMED = '1',
  TWO_ROOMED = '2',
  THREE_ROOMED = '3',
  STUDIO = 'studio',
}
