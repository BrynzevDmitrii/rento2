export enum ShortOptions {
  CHILDREN_ALLOWED = 'childrenAllowed',
  PETS_ALLOWED = 'petsAllowed',
  BALCONY = 'balcony',
  PARKING = 'parking',
  MORE_THAN_A_MONTH = 'moreThanAMonth',
}
