export const Furniture = [
  {
    name: 'withFurniture',
    type: 'furniture'
  },
  {
    name: 'withoutFurniture',
    type: 'furniture'
  },
  {
    name: 'goodRepairs',
    type: 'furniture'
  },
  {
    name: 'balcony',
    type: 'furniture'
  },
]
