export const Appliances = [
  {
    name: 'refrigerator',
    type: 'appliances'
  },
  {
    name: 'tv',
    type: 'appliances'
  },
  {
    name: 'washingMachine',
    type: 'appliances'
  },
  {
    name: 'dishWasher',
    type: 'appliances'
  },
  {
    name: 'airConditioner',
    type: 'appliances'
  },
]
