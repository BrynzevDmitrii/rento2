export enum ApartmentsFilterTypes {
  LONG = 'long',
  SHORT = 'short',
  All = 'all',
}
