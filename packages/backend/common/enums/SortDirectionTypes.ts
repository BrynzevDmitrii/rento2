export enum SortDirectionTypes {
  ASC = 'asc',
  DESC = 'desc',
}
