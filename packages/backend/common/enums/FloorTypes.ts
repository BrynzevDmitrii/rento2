export enum FlorTypes {
  NOT_FIRST = 'notFirst',
  NOT_LAST = 'notLast',
  LAST = 'last',
}
