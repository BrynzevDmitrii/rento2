import { Apartment } from 'App/Models'

export interface IWrappedQueriesList {
  popular?: Apartment[]
  rentoChoose?: Apartment[]
  inCityCenter?: Apartment[]
  newlyAdded?: Apartment[]
  nearTheSubway?: Apartment[]
}
